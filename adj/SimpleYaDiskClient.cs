using System;
using System.IO;
using System.Text;
using System.Collections.Generic;
using YandexDisk.Client.Http;
using YandexDisk.Client.Protocol;

namespace adj
{
   public class SimpleYaDiskClient 
   {
       private DiskHttpApi _api = null;
       private string _root_directory;
       Dictionary<string,Resource> _resources = null;

       private void _UpdateResources(string path)
       {
           path = path[path.Length - 1] == '/' ? path : (path + "/");

           if(!_resources.ContainsKey(path))
           {
               Resource resource = null;
               
               try {
                resource = _api.MetaInfo.GetInfoAsync(new ResourceRequest 
                    {
                      Path = path
                    }
               ).Result;
               }
               catch{ 
                   //Console.WriteLine("Не обнаружен путь '" + path + "'");
                   return;
               }

               _resources.Add(path, resource);
           }
           else
           {
               var resource = _api.MetaInfo.GetInfoAsync(new ResourceRequest 
                    {
                      Path = path
                    }
               ).Result;
               _resources[path] = resource;
           }
       }

       private Resource _GetResource(string path)
       {
           path = path[path.Length - 1] == '/' ? path : (path + "/");

           if(!_resources.ContainsKey(path))
           {
               _UpdateResources(path);
           }

           if(!_resources.ContainsKey(path))
           {
               throw new Exception("Не обнаружен путь '" + path + "'");
           }

           return _resources[path];
       }

       public int MaxNumberFile {set; get;} = 1000000;

       public SimpleYaDiskClient(string token, string root_directory = "/")
       {
           _api = new DiskHttpApi(token);
           _root_directory = root_directory[root_directory.Length - 1] == '/' ? root_directory : root_directory + "/";
           _resources = new Dictionary<string, Resource>();
           _UpdateResources(_root_directory);
       }

       public (string, string) GetParentDirectory(string path) 
       {
           string[] subdirs = path.Split('/', StringSplitOptions.RemoveEmptyEntries);
           int count = subdirs.Length - 1;

           StringBuilder parent = new StringBuilder(_root_directory);
           for(int i = 0; i < count; ++i)
           {
               parent.Append(subdirs[i] + "/");
           }

           return (parent.ToString(), subdirs[count]);
       }

       public bool DirectoryExists(string dirpath) 
       {
           var path = dirpath[0] == '/' ? dirpath : (_root_directory + dirpath);

           try
           {
               var resource = _GetResource(path);
               return true;
           }
           catch
           {
               return false;
           }
       }

       public bool FileExists(string filepath)
       {
           var path = GetParentDirectory(filepath);

           try
           {
               var resource = _GetResource(path.Item1);
               foreach(var item in resource.Embedded.Items)
               {
                   if(item.Name == path.Item2 && item.Type == ResourceType.File) return true;
               }
           }
           catch{}

           return false;
       }

       public void DirectoryCreate(string path)
       {
           path = path[0] == '/' ? path : (_root_directory + path);

           if(DirectoryExists(path)) return;

           var dirs = path.Split('/', StringSplitOptions.RemoveEmptyEntries);
           var count = dirs.Length;
           var i = 0;
           (string,string) parent_child;

           StringBuilder sb = new StringBuilder(_root_directory);
           var flag = false;

           for(; i < count; ++i) 
           {
               sb.Append(dirs[i] + "/");
               if(!flag && !DirectoryExists(sb.ToString())) flag = true;

               if(flag) {
                   _api.Commands.CreateDictionaryAsync(sb.ToString()).Wait();
                   parent_child = GetParentDirectory(sb.ToString());
                   _UpdateResources(parent_child.Item1);
               }
           }
       }

       public string UploadFile(string sourcepath, string destpath, bool overwrite = true)
       {
           if(!File.Exists(sourcepath)) throw new Exception("Не обнаружен источник '" + sourcepath + "'");
           
           var pc = GetParentDirectory(destpath);
           DirectoryCreate(pc.Item1);
           
           Link link = null;

           if(overwrite)
           {
               link = _api.Files.GetUploadLinkAsync(destpath, overwrite: true).Result;

               using(var fs = File.OpenRead(sourcepath))
               {
                   _api.Files.UploadAsync(link,fs).Wait();
               }

               _UpdateResources(pc.Item1);

               return destpath;
           }
           else
           {
               int k = 2;
               var extension = Path.GetExtension(pc.Item2);
               var fname_we = Path.GetFileNameWithoutExtension(pc.Item2);
               
               while(FileExists(destpath)) {
                   destpath = pc.Item1 + fname_we + "_(" + k + ")" + extension;
                   ++k;
                   if(k > MaxNumberFile) {
                       throw new Exception("Достигнуто максимальное количество номеров файлов " + MaxNumberFile);
                   }
               }

               link = _api.Files.GetUploadLinkAsync(destpath, overwrite: false).Result;

               using(var fs = File.OpenRead(sourcepath))
               {
                   _api.Files.UploadAsync(link,fs).Wait();
               }

               _UpdateResources(pc.Item1);

               return destpath;
           }
       }

   }
}
