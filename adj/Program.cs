﻿using System;
using System.Text;
using System.IO;
using System.IO.Compression;
using System.Threading; 
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.InteropServices; 
using System.Net;
using System.Net.Sockets;
using Tomlyn;

namespace adj
{
    class Program
    {
        static string lockfile = "";
        static string zipfile = "";
        static string apitoken = "";
        static string folder = "";  

        static string appid = "8df7ca2fe5e548a8b6100e7964e34e5d";

        static bool listen_flag = false;

        static void StartHttpListener()
        {
            TcpListener server=null;
            try
            {
                server = new TcpListener(IPAddress.Any, 64333);
                server.Start();
                listen_flag = true;

                TcpClient client = server.AcceptTcpClient();
                NetworkStream stream = client.GetStream();
 
                string response = @"HTTP/1.1 200 OK
                Content-Type: text/html

                <!DOCTYPE html>
                <html lang='ru'>
                <head>
                    <meta charset='utf-8'>
                    <title>Получение токена YandexDisk</title>
                    <style>
                       #token {
                           border: 1px dashed red; 
                       }
                       .result {
                           margin-left: auto;
                           margin-right: auto;
                           margin-top: 25%;
                           width: 35em;
                           text-align: center;
                           border: 1px solid blue;
                           padding-bottom: 20px;
                       }
                    </style>
                </head>
                <body>
                     <div class='result'>
                       <h2>SBOX.ADJ</h2>
                       <h3>Ваш токен от YandexDisk</h3>
                       <span id='token'></span>
                     </div>
                     <script type='text/javascript'>
                        var url = document.URL;
                        var splstr = url.split('=');
                        var token = splstr[1].split('&')[0];
                        var ptoken = document.getElementById('token');
                        ptoken.innerHTML = token;
                     </script>
                </body>
                </html>";

                byte[] data = Encoding.UTF8.GetBytes(response);
                stream.Write(data, 0, data.Length);
                
                stream.Close();
                client.Close();
                server.Stop();
                
            }
            catch(Exception exc) 
            {
                Console.WriteLine("Ошибка: " + exc.Message);
            }
            finally
            {
                if(server != null) server.Stop();
            }
        }

        static void GetYaDiskToken()
        {
           string url = $"https://oauth.yandex.ru/authorize?response_type=token&client_id={appid}";
           Thread thread = new Thread(StartHttpListener);
           thread.Start();

           while(!listen_flag) Thread.Sleep(2000);

           try
           {
                Process.Start(url);  
           }
           catch 
           {
            if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
           {
              url = url.Replace("&", "^&");
              Process.Start(new ProcessStartInfo("cmd", $"/c start {url}") { CreateNoWindow = true });
           }
           else if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux))
           {
              Process.Start("xdg-open", url);
           }
           else if (RuntimeInformation.IsOSPlatform(OSPlatform.OSX))
           {
               Process.Start("open", url);
           }
           else
           {
              throw new Exception("Не смог открыть url: '" + url +"'");
           }
           }
  
        }

        static void GetLockFiles(string dir, List<string> lockfiles) 
        {
            var dirs = Directory.GetDirectories(dir);

            foreach(var d in dirs) 
            {
                var lf = Path.Combine(d,lockfile);
                if(File.Exists(lf))
                {
                    lockfiles.Add(lf);
                    Console.WriteLine("Обнаружен запуск расчёта в '" + d +"'");
                }
                GetLockFiles(d, lockfiles);
            }
            
        }

        static bool CreateZip() 
        {
            try {
            var cd = Directory.GetCurrentDirectory();
            var filename = Path.Combine(cd,"h.txt");
            var dname = new DirectoryInfo(Path.GetDirectoryName(filename)).Name;

            zipfile = Path.Combine(Directory.GetParent(cd).FullName, dname + ".zip");
            if(File.Exists(zipfile)) File.Delete(zipfile);

            Console.WriteLine("Архивация директории '" + cd + "' в '" + zipfile + "'...");
            ZipFile.CreateFromDirectory(cd, zipfile);

            Console.WriteLine("Архивация завершена");
            Console.WriteLine();
            return true;

            }
            catch(Exception exc) {
                Console.WriteLine("Во время архивации возникла ошибка: " + exc.Message);
                Console.WriteLine("Нажмите любую кнопку...");
                Console.ReadKey();
                return false;
            }
        }

        static void UploadToYandexDisk() {
            try{

            if(!File.Exists(zipfile)) {
                Console.WriteLine("Не обнаружен архив для сохранения");
                return;
            }
            
            Console.WriteLine("Загрузка результатов на YandexDisk...");
            var yadisk = new SimpleYaDiskClient(apitoken);
            var result = yadisk.UploadFile(zipfile, folder + Path.GetFileName(zipfile), false);
            Console.WriteLine("YandexDisk: Файл '" + zipfile + "' загружен по пути '" + result + "'"); 

            }
            catch(Exception exc) {
                Console.WriteLine("Во время загрузки на YandexDisk возникла ошибка: " + exc.Message);
                Console.WriteLine("Нажмите любую кнопку...");
                Console.ReadKey();
            }
        }

        static bool LoadSettings(string fname)
        {
            if(!File.Exists(fname)) {
                Console.WriteLine("Ошибка: Не найден файл конфигурации '" + fname + "'");
                return false;
            }

            var config = File.ReadAllText(fname);
            var doc = Toml.Parse(config);
            var table = doc.ToModel();
            
            if(table.ContainsKey("api_token"))
               apitoken = (string)(table["api_token"]);

            if(table.ContainsKey("send_to"))
               folder = (string)(table["send_to"]);

            folder = folder[folder.Length - 1] == '/' ? folder : folder + "/";

            if(table.ContainsKey("locker"))
               lockfile = (string)(table["locker"]);

            if(lockfile == "") lockfile = ".lock";

            return true;
        }

        static void Main(string[] args)
        {
            //StartHttpListener();

            string settings_file = "adj.toml";

            if(args.Length != 0)
            {
                settings_file = args[0];
            }

            if(settings_file == "--get_token") {
                GetYaDiskToken();
                return;
            }

            if(!LoadSettings(settings_file)) return;

            List<string> lockfiles = new List<string>();

            Console.WriteLine("Поиск запущенных расчётов...");
            var curdir = Directory.GetCurrentDirectory();

            var lckf = Path.Combine(curdir,lockfile);
            if(File.Exists(lckf))
            {
                lockfiles.Add(lckf);
                Console.WriteLine("Обнаружен запуск расчёта в '" + curdir +"'");
            }

            GetLockFiles(Directory.GetCurrentDirectory(), lockfiles);
            
            if(lockfiles.Count > 0)
            {
               Console.WriteLine();
               Console.WriteLine("Ожидание завершения расчётов...");
               int count = lockfiles.Count;

                for(int i = 0; i < count; ++i) {
                    while(File.Exists(lockfiles[i])) Thread.Sleep(1000);
                }

                Console.WriteLine();
                Console.WriteLine("Все расчёты завершены");
                Console.WriteLine("Архивация результатов...");

                if(CreateZip()) UploadToYandexDisk();
            }
            else {
                Console.WriteLine("Запущенных расчётов не обнаружено");
            }

            Console.WriteLine("Программа завершена");
            
            
        }
    }
}
