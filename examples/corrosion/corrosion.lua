--листинг corrosion.lua
--Пример запуска моделирования коррозии

CE_dat_file = "alloy3_system.dat" -- CE файл с параметрами потенциала
config_file = "1.xyz" -- конфигурация атомов

config_load(config_file, false) -- загрузить конфиг
sys_name = get_system_name()

mol_masses_load(CE_dat_file) -- загрузить молярные массы

simple_geometry_init(7.55) -- инициализировать простую геометрию
simple_randomizer_init(0.2, -1) -- инициализировать простой генератор трансляций атомов

gupt_load(CE_dat_file) -- создать потенциал Гупта и загрузить для него параметры

-- параметры для проведения моделирования
params = {
    total_steps = 60000, -- полное число шагов Монте-Карло
    relaxing_steps = 50000, -- количество шагов для проведения релаксации
    dump_every = 1000, -- сбрасывать конфиги каждые 100 раз в dump-файл
    aver_every = 1000,  -- на этапе усреднения сбрасывать конфиги в aver-файл
    dump_file = sys_name.."_dump.xyz",
    aver_file = sys_name.."_aver.xyz"
}

MH_sampler_init(params) -- инициализировать сэмплер Метрополиса-Гастингса
print_info()

num_of_atoms = get_num_of_atoms()

while true do 
    print(string.format("%s> Launching for %d atoms", sys_name, num_of_atoms))
    run_temperature(300)
    config_dump(string.format("%d_", num_of_atoms)..".xyz")
    if num_of_atoms > 150 then
        remove_max_energy_atoms(5)
        num_of_atoms = get_num_of_atoms()
    else break end
end

free_all() -- освободить все объекты, если они не нужны

print("All done!") -- печатаем, что всё сделано
