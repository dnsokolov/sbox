local serie = require "serie"

options = {}

options.files = { 
  "alloy3_system.dat",
  "mc.lua"
}

configs = {template = ".xyz"}
list_files(configs, "")

options.configs = configs

serie.start(options)
