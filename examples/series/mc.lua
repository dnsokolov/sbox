--листинг mc.lua
--Пример запуска моделирования методом Монте-Карло

CE_dat_file = find_file(".dat") -- найти какой-нибудь файл CE файл с параметрами потенциала
config_file = find_file(".xyz") -- найти какой-нибудь файл xyz с конфигурацией атомов

config_load(config_file, false) -- загрузить конфиг
sys_name = get_system_name()

mol_masses_load(CE_dat_file) -- загрузить молярные массы

simple_geometry_init(7.55) -- инициализировать простую геометрию
simple_randomizer_init(0.2, -1) -- инициализировать простой генератор трансляций атомов

gupt_load(CE_dat_file) -- создать потенциал Гупта и загрузить для него параметры

-- параметры для проведения моделирования
params = {
    total_steps = 60000, -- полное число шагов Монте-Карло
    relaxing_steps = 50000, -- количество шагов для проведения релаксации
    dump_every = 1000, -- сбрасывать конфиги каждые 100 раз в dump-файл
    aver_every = 1000,  -- на этапе усреднения сбрасывать конфиги в aver-файл
    dump_file = sys_name.."_dump.xyz",
    aver_file = sys_name.."_aver.xyz"
}

MH_sampler_init(params) -- инициализировать сэмплер Метрополиса-Гастингса
print_info()

run_temperature(300.0) -- запуск симуляции для температуры 300 К
run(300, 1500, 10)     -- продолжение симуляции в диапазоне температур
get_caloric_dat(params.aver_file, sys_name.."_caloric.dat")

free_all() -- освободить все объекты, если они не нужны

print("All done!") -- печатаем, что всё сделано


