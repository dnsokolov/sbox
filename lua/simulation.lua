--Created by Sokolov Denis (C) 2021
--module for initializing of simulation
--for sbox version 0.14

local simulation = {}
local chemdata = require "chemdata"

local function integrator_init(integrator)
   local ok_flag = false

   if integrator.name == 'leapfrog' then
      leapfrog_integrator_init(integrator.dt)
      ok_flag = true
      print('leapfrog is initialized')
   end

   if integrator.name == 'verlet' then
      verlet_integrator_init(integrator.dt)
      ok_flag = true
      print('verlet is initialized')
   end

   if integrator.name == 'verlet_velocity' then
      verlet_velocity_integrator_init(integrator.dt)
      ok_flag = true
      print('verlet_velocity is initialized')
   end

   if integrator.name == 'nose_hoover' then
      nose_hoover_integrator_init(integrator.dt, integrator.Q, integrator.temperature)
      ok_flag = true
      print('nose_hoover is initialized')
   end

   if ok_flag then
      if integrator.init_temperature ~= nil then
         random_velocities_init(integrator.init_temperature)
      end
      integrator_start()
   else
      print("Error: unknown integrator name: '"..integrator.name.."'")
      io.read()
   end
end

function simulation.init(options)
  if options == nil then
     print("Error: options is nil in 'simulation.init(options)'")
     return
  end

  local continuation = false
  local sys_name = ""
  local ce_loaded_flag = false
  local integrator_flag = (options.integrator ~= nil) 

  if options.config_file == nil then
     print("Error: options.config_file is nil in 'simulation.init(options)'")
     return
  end

  if options.CE_dat_file == nil then
     ce_loaded_flag = false
  else
     ce_loaded_flag = file_exists(options.CE_dat_file)
  end

  if options.sampler == nil then
     print("Error: options.sampler is nil in 'simulation.init(options)'")
     return
  end

  if options.randomizer == nil then
     options.randomizer = {}
     options.randomizer.name  = "simple"
     options.randomizer.drmax = 0.2
     options.randomizer.seed = -1
  end

  if options.randomizer.name == nil then
     options.randomizer.name = "simple"
  end

  if options.randomizer.drmax == nil then
     options.randomizer.drmax = 0.2
  end

  if options.randomizer.time == nil then
     options.randomizer.time = 0.01
  end

  if options.randomizer.seed == nil then
     options.randomizer.seed = -1
  end

  if options.continuation ~= nil then
     continuation = options.continuation
  end

  if options.geometry == nil then
     options.geometry = {}
     options.geometry.name = "simple"
     options.geometry.rcut = 7.55
  end

  if options.geometry.name == nil then
     options.geometry.name = "simple"
  end

  if options.geometry.rcut == nil then
     options.geometry.rcut = 7.55
  end
  
  config_load(options.config_file, continuation)
  sys_name = get_system_name()
  
  if ce_loaded_flag then
     mol_masses_load(options.CE_dat_file)
  end

  if options.randomizer.name == "simple" then
     simple_randomizer_init(options.randomizer.drmax, options.randomizer.seed)
  elseif options.randomizer.name == "maxwell" then
     maxwell_randomizer_init(options.randomizer.time, options.randomizer.seed)
  else
     print(string.format("Error: Couldn't find '%s' randomizer", options.randomizer.name))
     return  
  end
  
  if options.geometry.name == "simple" then
     simple_geometry_init(options.geometry.rcut)
  elseif options.geometry.name == "box" then
     if options.geometry.params == nil then
        print("Error: options.geometry.params is nil in 'simulation.init(options)'")
        return
     end
     box_geometry_init(options.geometry.rcut, options.geometry.params)
  elseif options.geometry.name == "pbc" then
     if options.geometry.params == nil then
        print("Error: options.geometry.params is nil in 'simulation.init(options)'")
        return
     end
     pbc_geometry_init(options.geometry.rcut, options.geometry.params)
  else
     print(string.format("Error: Couldn't find '%s' geometry", options.geometry.name))
     return 
  end

  local num_of_threads = 1
  if options.num_of_threads ~= nil then
     num_of_threads = options.num_of_threads
  end
  
  if ce_loaded_flag then
     gupt_load(options.CE_dat_file, num_of_threads)
  elseif file_exists("atominfo.toml") then
     local symbols = {}
     
     gupt_init(num_of_threads)
     get_config_components(symbols)
     
     chemdata.init("atominfo.toml")
     
     for i,sym in ipairs(symbols) do
        local potline = chemdata.get_potential_line("gupt",sym,sym)
        set_potential_line(potline)
        local mol_mass = chemdata.get_molar_mass(sym)
        set_molar_mass(sym, mol_mass)
     end
     
     for i = 1, #symbols do
        for j = i + 1, #symbols do
           local sym1, sym2 = symbols[1], symbols[2]
           local potline = chemdata.get_potential_line("gupt", sym1, sym2)
           set_potential_line(potline)
        end
     end
     
     chemdata.free()
  else
     gupt_init(num_of_threads)
     
     if options.potential_params ~= nil then
        for i,s in ipairs(options.potential_params) do
           set_potential_line(s)
        end
     else
        print("Couldn't load potential_params (is nil)")
        return   
     end
     
     if options.elements ~= nil then
        for i,t in ipairs(options.elements) do
           set_molar_mass(t.name, t.molar_mass)
        end
     end
  end
  
  if options.total_steps == nil then
     options.total_steps = 100000
  end

  if options.relaxing_steps == nil then
     options.relaxing_steps = 90000
  end

  if options.integrator_steps == nil then
     options.integrator_steps = 10
  end

  if options.dump_every == nil then
     options.dump_every = 1000
  end

  if options.aver_every == nil then
     options.aver_every = 1000
  end
 
  if options.dump_file == nil then
     options.dump_file = sys_name.."_dump.xyz"
  end

  if options.aver_file == nil then
     options.aver_file = sys_name.."_aver.xyz"
  end
  
  options.sys_name = sys_name

  if options.inter_atomic_dist == nil then
     options.inter_atomic_dist = get_inter_atomic_dist()
  end

  if get_md_flag(options.sampler) then
     if integrator_flag then
        integrator_init(options.integrator)
     else
        print("Error: Integrator is not defined")
        io.read()
     end
  end

  sampler_init(options.sampler, options)
end

-----------------------------------------------------------------------
--@function simulation.run_corrosion
--@double temperature - temperature for simulation
--@int stop_num - stop simulation if num_of_atoms <= stop_num
--@int remove_num - num of removing atom for each iteration
function simulation.run_corrosion(temperature, stop_num, remove_num)
   local num_of_atoms = get_num_of_atoms()
   print("Start corrosion simulation...")
   while true do
      local sys_name = get_system_name()
      print(string.format("%s> Launching for %d atoms", sys_name, num_of_atoms))
      run_temperature(temperature)
      config_dump(sys_name.."("..string.format("%d_", num_of_atoms)..").xyz")
      if num_of_atoms > stop_num then
         remove_max_energy_atoms(remove_num)
         num_of_atoms = get_num_of_atoms()
      else break end
   end
end

return simulation
