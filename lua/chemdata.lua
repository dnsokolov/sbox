local chemdata = {}

local toml = require "toml"
local data = nil

function chemdata.init(tomlfile)
   if data then 
      print("Error: chemdata is used already")
      return false
   end
    
   if tomlfile == nil then
      data = toml.parse_file(get_sbox_directory().."/chemdata.toml")
      return true
   end
   
   data = toml.parse_file(tomlfile)
   return true
end

function chemdata.get_potential_line(potname, ename1, ename2)
   if data == nil then return "" end
   local potentials = data["potentials"]
   for i, pline in ipairs(potentials) do
      local tokens = {} 
      local i = 1
      
      for token in pline:gmatch("[^%s]+") do
         tokens[i] = token
         i = i + 1
      end
 
      local pname, en1, en2 = tokens[1], tokens[2], tokens[3]
      
      if pname == potname and ((en1 == ename1 and en2 == ename2) or (en1 == ename2 and en2 == ename1)) then
         return pline
      end
   end
end

function chemdata.get_potential_params(potname, ename1, ename2)
   local pline = chemdata.get_potential_line(potname, ename1, ename2)
   
   local params = {
      potname = potname,
      ename1 = ename1,
      ename2 = ename2,
   }

   local i = 1
      
   for token in pline:gmatch("[^%s]+") do
      if i>3 then
         table.insert(params, tonumber(token))
      end
      i = i + 1
   end
   
   return params
end

function chemdata.get_molar_mass(ename)
   return data[ename].molmass
end

function chemdata.get_a(ename)
   return data[ename].a
end

function chemdata.get_b(ename)
   return data[ename].b
end

function chemdata.get_lattype(ename)
   return data[ename].lattype
end

function chemdata.get_element_data(ename)
   return data[ename]
end

function chemdata.fill_table(tbl)
   local i = 1
   for k,v in pairs(tbl) do
      if k:find("ename") then
         if data[v].a then tbl["a"..tostring(i)] = data[v].a end
         if data[v].b then tbl["b"..tostring(i)] = data[v].b end
         if data[v].lattype then tbl["lattype"..tostring(i)] = data[v].lattype end
      end
   end
end

function chemdata.free()
   data = nil
end

function chemdata.params_to_line(prms)
   local result = fmt({prms.potname, prms.ename1, prms.ename2,}," ")
   for _,v in ipairs(prms) do
      result = result..tostring(v).." "
   end
   return result
end

function chemdata.mix(potname, ename1, ename2, mix_fn)
   local prms1 = chemdata.get_potential_params(potname, ename1, ename1)
   local prms2 = chemdata.get_potential_params(potname, ename2, ename2)
   local prms12 = {
      potname = potname,
      ename1 = ename1,
      ename2 = ename2,
   }
   
   if mix_fn == nil or mix_fn == "marithm" then
      local i = 1
      for _, v in ipairs(prms1) do
         table.insert(prms12, 0.5*(v + prms2[i]))
         i = i + 1
      end
      return prms12
   end
   
   if mix_fn == "mgeom" then
      local i = 1
      for _, v in ipairs(prms1) do
         table.insert(prms12, math.sqrt(v*prms2[i]))
         i = i + 1
      end
      return prms12   
   end
   
   if mix_fn == "mgupt" then
      table.insert(prms12, (prms1[1] + prms2[1]) * 0.5)
      table.insert(prms12, math.sqrt(prms1[2] * prms2[2]))
      table.insert(prms12, (prms1[3] + prms2[3]) * 0.5)
      table.insert(prms12, math.sqrt(prms1[4] * prms2[4]))
      table.insert(prms12, (prms1[5] + prms2[5]) * 0.5)
      table.insert(prms12, (prms1[6] + prms2[6]) * 0.5)
      return prms12
   end
   
   if type(mix_fn) == "function" then
      mix_fn(prms1,prms2,prms12)
      return prms12
   end
   
   return prms2
end

return chemdata
