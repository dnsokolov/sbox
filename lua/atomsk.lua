local atomsk = {
   module_version = "0.1",
   author  = "Sokolov Denis",
   version = "0.10.6",
}

local toml = require "toml"
local chemdata = nil

local function is_need_c(lattype)
   local ltypes = {"hcp", "st", "bct", "fct", "L1_0", "wurtzite", "wz", "graphite", "C14", "C36"}
   for i,t in ipairs(ltypes) do
      if lattype == t then return true end
   end
   return false 
end

local function get_lattype_info(lattype)
   if (lattype == "sc") or (lattype == "st") then 
      local info = {
         max_elements = 1,
         min_elements = 1,
      }
      return info
   end

   local only_two_elements_ltype = {"L12", "L1_2", "fluorite", "rocksalt", "C15", "wurtzite", "wz", "C14", "C36"}
   for i,t in ipairs(only_two_elements_ltype) do
      if lattype == t then 
         local info = {
            max_elements = 2,
            min_elements = 2,
         }
         return info
      end
   end

   if lattype == "perovskite" or lattype == "pr" then
      local info = {
         max_elements = 3,
         min_elements = 3,
      }
      return info
   end

   local info = {
      max_elements = 2,
      min_elements = 1,
   }
   return info
end

local function is_need_radius(shape)
   local shapes = {"sphere", "semisphere", "cone", "cyl"}
   for i,s in ipairs(shapes) do
      if shape == s then return true end
   end
   return false
end

local function is_need_height(shape)
   local shapes = {"cone", "cyl"}
   for i,s in ipairs(shapes) do
      if shape == s then return true end
   end
   return false
end

local function load_chem_data()
   if chemdata == nil then chemdata = toml.parse_file(get_sbox_directory().."/chemdata.toml") end
end

local function validate(options)
   local info = {ok = true, message = "", enames = ""}
   load_chem_data()
   
   if options == nil then
      info.message = "Error: options is nil"
      info.ok = false
      return info
   end
   
   if options.enames == nil then
      info.message = "Error: enames is nil"
      info.ok = false
      return info
   end
   
   local ename = nil
   local enames = options.enames

   if type(enames) == "string" then 
      ename = enames
      info.enames = ename.." "
   end

   if type(enames) == "table" then
      if #enames == 1 then ename = enames[1] end
      info.enames = fmt(enames," ")
   end

   local ename_is_nil = (ename == nil)
   local element = nil 

   if ename ~= nil then element = chemdata[ename] end
   local element_is_nil = (element == nil)
   
   if element_is_nil or ename_is_nil then
      if options.lattype == nil then
         info.message = "Error: lattype is nil"
         info.ok = false
         return info
      end
      
      if options.lattype == "NT" or options.lattype == "nanotube" then
         info.message = "Nanotubes is not supported yet"
         info.ok = false
         return info
      end
      
      if options.a == nil then
         info.message = "Error: a is nil"
         info.ok = false
         return info
      end
      
      if is_need_c(options.lattype) and options.c == nil then
         info.message = "Error: c is nil"
         info.ok = false
         return info
      end
   end

   if not element_is_nil then
      if options.lattype == nil then
         options.lattype = element["lattype"]
         if options.lattype == nil then
            info.ok = false
            info.message = "Error: lattype is nil"
            return info
         end
      end
      
      if options.a == nil then
         options.a = element["a"]
         if options.a == nil then
            info.ok = false
            info.message = "Error: a is nil"
            return info
         end
      end
      
      if is_need_c(options.lattype) and options.c == nil then
         options.c = element["c"]
         if options.c == nil then
            info.ok = false
            info.message = "Error: c is nil"
            return info
         end
      end
   end

   local lt_info = get_lattype_info(options.lattype)

   if #enames < lt_info.min_elements then
      info.message = fmt("Error: for this type of lattice need [lt_info.min_elements] type of atoms at least")
      info.ok = false
      return info
   end

   local outfile = ""
   if options.outfile == nil then
      if ename then outfile = ename..".xyz" else
         local num = #enames
         for i,e in ipairs(enames) do
            if i < num then outfile = outfile..e.."@" end
            if i == num then outfile = outfile..e..".xyz" end 
         end
      end
   end
   options.outfile = outfile

   return info
end

local function get_shape_line(shape)
   if shape == nil then return nil end
   if shape.type == nil then return nil end

   if is_need_radius(shape.type) and shape.type ~= "cone" then
      if shape.radius == nil then
         print("Error: shape.radius is nil")
         return nil
      end
   end

   if shape.type == "sphere" then
      local line = "-select out sphere"
      if shape.center == nil then
         line = fmt({line," 0.5*BOX 0.5*BOX 0.5*BOX ",shape.radius, " -rmatom select"})
         return line
      else
         local center = fmt(shape.center, " ")
         line = fmt({line, " ", center, " -rmatom select"})
         return line
      end
   end

   if shape.type == "semisphere" then
      local line = "-select out sphere"
      local axis = "z"
      if shape.axis ~= nil then axis = shape.axis end

      if axis == "z" then
         line = fmt({line," 0.5*BOX 0.5*BOX 0 ",shape.radius, " -rmatom select"})
         return line
      end

      if axis == "x" then
         line = fmt({line," 0 0.5*BOX 0.5*BOX ",shape.radius, " -rmatom select"})
         return line
      end

      if axis == "y" then
         line = fmt({line," 0.5*BOX 0 0.5*BOX ",shape.radius, " -rmatom select"})
         return line
      end
   end

   if shape.type == "cyl" then
      local line = "-select out cylinder"
      local axis = "z"
      if shape.axis ~= nil then axis = shape.axis end
      line = fmt({line," ",axis," 0.5*BOX 0.5*BOX ",shape.radius, " -rmatom select"})
      return line
   end

   if shape.type == "cone" then
      if shape.alpha == nil then
         print("Error: shape.alpha is nil")
         return nil
      end

      local line = "-select out cone"
      local axis = "z"

      if shape.axis ~= nil then axis = shape.axis end
      line = fmt({line, " ", axis, " 0.5*BOX 0.5*BOX 0.5*BOX ", shape.alpha, " -rmatom select"})
      return line
   end

   if shape.type == "box" then
      if shape.L == nil then 
         print("Error: shape.L is nil")
         return nil
      end

      if #shape.L ~= 3 then
         print("Error: shape.L must contains 3 components {Lx,Ly,Lz}")
         return nil
      end

      local line = "-select out box 0 0 0 "
      local L = fmt(shape.L, " ")

      line = line..L.." -rmatom select"
      return line
   end

   if shape.type == "torus" then
      if shape.Radius == nil then
         print("Error: Radius is nil")
         return nil
      end

      if shape.radius == nil then
         print("Error: radius is nil")
         return nil
      end

      local axis = "z"

      if shape.axis ~= nil then axis = shape.axis end

      local line = "-select out torus "..axis.." 0.5*BOX 0.5*BOX 0.5*BOX "
      line = fmt({line,shape.Radius," ",shape.radius," -rmatom select"})

      return line
   end
end

--[[local function call_atomsk(options)
   if options.shape == "sphere" then
      
   end
end]]--

function atomsk.execute(cmdline)
  if cmdline == nil or cmdline == "" then
     process_start("atomsk")
     return 
  end
  
  process_start("atomsk "..cmdline)
end

function atomsk.execute_script(script)
   if file_exists(script) then
      atomsk.execute(" < '"..script.."'")
      return true
   else
      print("Script '"..script.."' is not found")
      return false
   end
end

function atomsk.create(options)
   local info = validate(options)

   if not info.ok then 
      print(info.message)
      return
   end

   local cmdline = fmt({"--create ", options.lattype, " ", options.a})
   if is_need_c(options.lattype) then cmdline = fmt({cmdline," ", options.c}) end
   
   cmdline = fmt({cmdline," ", info.enames})
   
   if options.orient ~= nil then
      if #options.orient == 3 then
         local orient = fmt(options.orient," ")
         cmdline = cmdline.." orient "..orient
      else
         print("Warning: orient must contains 3 elements")
      end
   end 

   if options.ortho ~= nil then
      if options.ortho == true then
         cmdline = cmdline.. " -orthogonal-cell "
      end
   end

   if options.duplicate ~= nil then
      if #options.duplicate == 3 then
         local dup = fmt(options.duplicate," ")
         cmdline = cmdline.." -duplicate "..dup
      else
         print("Warning: orient must contains 3 elements")
      end
   end

   local shape_line = get_shape_line(options.shape)

   if shape_line ~= nil then
      cmdline = cmdline.." "..shape_line
   end

   cmdline = cmdline.." "..options.outfile

   if file_exists(options.outfile) then file_remove(options.outfile) end

   print(fmt("Launching atomsk with command line -> '[cmdline]'"))
   atomsk.execute(cmdline)
    
   return options.outfile
end



return atomsk
