local bootstrap = {}

local chemdata

local function Set(list)
  local set = {}
  for _, l in ipairs(list) do set[l] = true end
  return set
end

function bootstrap.init(chemdt)
    chemdata = chemdt
end

function bootstrap.generate_atominfo(params)
   --chemdata.init()
   
   local symbols = {}
   local molar_masses = {}
   local atominfo = io.open("atominfo.toml", "w")
   atominfo:write("configs = [\n")
   
   for _,v in ipairs(params.configs) do
      if file_exists(v) then
         atominfo:write("   \""..v.."\",\n")
         
         config_load(v, false)
         local species = {}
         get_config_components(species)
         local items = Set(symbols) 
         
         for _, sp in ipairs(species) do
            if not items[sp] then
               table.insert(symbols,sp)
               molar_masses[sp] = chemdata.get_molar_mass(sp)
            end
         end
         
         free_config()
      end
   end
   
   atominfo:write("]\n");
   atominfo:write("\n");
   
   atominfo:write("#Gupta Potential Parameters (ro A p B q rcut)\n")
   atominfo:write("potentials = [\n")
   
   for _,sym in ipairs(symbols) do
         local line = chemdata.get_potential_line("gupt", sym, sym)
         if params.rcut then
            local prms = chemdata.get_potential_params("gupt", sym, sym)
            prms[6] = params.rcut
            line = chemdata.params_to_line(prms)
         end
         atominfo:write("   \""..line.."\",\n")
   end
   
   for i = 1, #symbols do
      for j = i+1, #symbols do
         local sym1, sym2 = symbols[i], symbols[j]
         local line = chemdata.get_potential_line("gupt", sym1, sym2)
         if params.mix_fn then
             local prms12 = chemdata.mix("gupt", sym1, sym2, params.mix_fn)     
             if params.rcut then prms12[6] = params.rcut end
             line = chemdata.params_to_line(prms12)
         else
            if params.rcut then
               local prms12 = chemdata.get_potential_params("gupt", sym1, sym2)
               prms12[6] = params.rcut
               line = chemdata.params_to_line(prms12)
            end
         end
         atominfo:write("   \""..line.."\",\n")
      end
   end
   
   atominfo:write("]\n")
   atominfo:write("\n")
   
   for k,v in pairs(molar_masses) do
      atominfo:write(fmt({"[",k,"]\n"}))
      atominfo:write(fmt({"molmass = ", v,"\n"}))
      atominfo:write("\n")
   end
   
   atominfo:close()
   --chemdata.free()
end

return bootstrap