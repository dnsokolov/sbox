--Created by Sokolov Denis (C) 2021
--module for series launching
--for sbox version 0.13

local serie = {}

function serie.start(options)  
   if options == nil then options = {} end

   local main_dir = get_working_directory()
   print(main_dir)
   local serie_template_name = "serie_"
   local max_number_of_series = 1000
   local configs = {template = ".xyz"}
   local serie_directory_name = ""
   local ok_flag = false
   local start_with = 1
   local rdirs = {}

   if options.serie_template_name ~= nil then serie_template_name = options.serie_template_name end
   if options.max_number_of_series ~= nil then max_number_of_series = options.max_number_of_series end

   if options.configs == nil then
      list_files(configs,"")
   else
      configs = options.configs
   end

   if options.start_with ~= nil then
      start_with = options.start_with
   end
   
   for i = start_with, max_number_of_series do
      serie_directory_name = serie_template_name..tostring(i)
      if directory_exists(serie_directory_name) == false then
         directory_create(serie_directory_name)
         print("Directory '"..serie_directory_name.."' is created for serie #"..tostring(i))
         ok_flag = true
         break
      end
   end

   if ok_flag == false then
      print("Couldn't create directory for the serie. Too many directories were created. Try to increase 'options.max_number_of_series' or to delete directories")
      return rdirs
   end

   for _,config in ipairs(configs) do
      local d = get_file_name_without_extension(config)
      local dirname = serie_directory_name.."/"..d
      local filename = get_file_name(config)

      if directory_exists(dirname) then
         print("Deleting directory '"..dirname.."'") 
         directory_remove(dirname) 
      end

      print("Creating directory '"..dirname.."'")
      directory_create(dirname)
      
      rdirs[#rdirs + 1] = dirname

      print("Copying file '"..config.."' to '"..dirname.."/"..filename.."'")
      file_copy(config, dirname.."/"..filename)

      if options.files ~= nil then
         for _,file in ipairs(options.files) do
            filename = get_file_name(file)
            print("Copying file '"..file.."' to '"..dirname.."/"..filename.."'")
            file_copy(file, dirname.."/"..filename)
         end
      end

      directory_change(dirname)
      local lua_script = find_file(".lua")
      
      if options.start_point ~= nil then
         lua_script = options.start_point
      end
      
      if lua_script == "" or file_exists(lua_script) ~= true then 
         print("Script file for launching is not found. Add it in 'options.file'")
         return
      end

      print("Launching sbox in '"..dirname.."'")
      local wd = get_working_directory()
      print("wd="..wd)
      print("lua_script="..lua_script)
      sbox_execute(lua_script)
      sleep(1000)
      directory_change(main_dir)
      print("")
   end
   
   return rdirs
end

function serie.restart(script_name)
   local dirs = {}
   list_directories(dirs,"")
   local main_dir = get_working_directory()
   local script_path = main_dir.."/"..script_name
   local rdirs = {}

   if file_exists(script_path) == false then
      print("Couldn't found script '"..script_name.."' in current directory")
      return rdirs
   end
   
   for _,dir in ipairs(dirs) do
      print("In directory '"..dir.."'")
      local subdirs = {}
      list_directories(subdirs,dir)
      
      for _,sdir in ipairs(subdirs) do
         if file_exists(script_path) then
            directory_change(sdir)
            if file_exists('.lock')==false then goto continue end
            rdirs[#rdirs + 1] = sdir
            os.remove(script_name)
            file_copy(script_path, script_name)
            print("   In directory '"..sdir.."'")
            print("   Executing '"..script_name.."'...")
            directory_unlock()
            sbox_execute(script_name)
            sleep(1000)
            ::continue::
            directory_change(main_dir)
         end
      end
      print("")
   end
   
   return rdirs
end

function serie.start_group(num, options)
   local i
   local dirs = {}
   local dir
   
   for i = 1, num do
      local drs = serie.start(options)
      for _,dir in ipairs(drs) do
         dirs[#dirs + 1] = dir
      end
   end
   
   return dirs
end

function serie.wait(dirs, sleep_time)
   if dirs == nil then return end
   if sleep_time == nil then sleep_time = 1000 end
   
   local counter = 0
   local dir
   
   while true do
      counter = 0
      
      for _,dir in ipairs(dirs) do
         if file_exists(dir..'/.lock') then counter = counter + 1 end
      end 
      
      if counter == 0 then break end   
      sleep(sleep_time)
   end   
     
end

function serie.execute(options, sleep_time)
   local num_of_parallel_series = options.num_of_parallel_series
   local num_of_total_series = options.num_of_total_series
   
   if num_of_parallel_series == nil then num_of_parallel_series = 1 end
   if num_of_total_series == nil then num_of_total_series = 1 end
   
   local restart_dirs = serie.restart(options.start_point)
   serie.wait(restart_dirs)
   
   if #restart_dirs > 0 then
      num_of_total_series = num_of_total_series - (#restart_dirs/#options.configs) - num_of_parallel_series
   end
   
   local remainder = num_of_total_series % num_of_parallel_series
   local num_of_groups = math.floor(num_of_total_series / num_of_parallel_series) 
   local dirs
   
   if sleep_time == nil then sleep_time = 1000 end
   
   for i = 1, num_of_groups do
      dirs = serie.start_group(num_of_parallel_series, options)
      serie.wait(dirs, sleep_time)
   end
   
   if remainder ~= 0 then
      dirs = serie.start_group(remainder, options)
      serie.wait(dirs, sleep_time)
   end     
end  

return serie
