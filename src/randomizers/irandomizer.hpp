#pragma once

#include "dSFMT/dSFMT.h"
#include "core/utils.hpp"
#include "core/atom.hpp"
#include "core/configuration.hpp"

#include <cstdlib>

namespace sbox {

    class IRandomizer : public Configuration::IRemoveAtomHandler {
        protected:
        Configuration *_config = nullptr;
        std::vector<Atom*> _atoms, _no_frozen_atoms;
        size_t _size, _size_no_frozen;
        double _temperature, _sqrt_temperature, _E_temperature;
        Atom *_atom;
        uint32_t _seed;
        double _zero_temperature = 0.001;
        vector<double> _mol_masses;
        
        public:
        IRandomizer(Configuration *config, uint32_t seed) {
            seed = seed == -1 ? get_time() : seed;
            srand(seed);
            _config = config;
            _atoms = config->GetAtoms();
            _no_frozen_atoms = config->GetNoFrozenAtoms();
            _size = _atoms.size();
            _size_no_frozen = _no_frozen_atoms.size();
            _mol_masses = config->GetMolMasses();
            init_gen_rand(seed);
            _seed = seed;
        }

        void GetRandomAtom(Atom **atom) {
            *atom = _atoms[rand()%_size];
            _atom = *atom;
        } 

        void GetRandomNoFrozenAtom(Atom **atom) {
            *atom = _no_frozen_atoms[rand()%_size_no_frozen];
            _atom = *atom;
        }

        uint32_t GetSeed() const {
            return _seed;
        }

        Atom *GetRandomAtom(std::vector<Atom*> &vatoms) {
            return vatoms[rand()%(vatoms.size())];
        }

        void SetCurrentAtom(Atom *a) {
            _atom = a;
        }

        void SetCurrentTemperature(double temperature) {
            if(temperature <= 0) temperature = _zero_temperature;
            _temperature = temperature;
            _sqrt_temperature = sqrt(temperature);
            _E_temperature = k_boltzmann_3_2 * _size * temperature;
        }

        void SetZeroTemperature(double value) {
            _zero_temperature = value;
        }

        void CallRemoveAtomHandler(Configuration &config, Atom *atom) override {
            _atoms = config.GetAtoms();
            _no_frozen_atoms = config.GetNoFrozenAtoms();
            _size = _atoms.size();
            _size_no_frozen = _no_frozen_atoms.size();
        }

        void SetRandomVelocities() {
            size_t j;
            Atom *a, *b;
            Vec3D rji, K, Vci(0,0,0);
            double mi, mj, mi_mj, d, total_mass = _config->GetTotalMolMass(), Ekin = 0;

            for(size_t i = 0; i < _size; i += 2) {
                j = i + 1;
                a = _atoms[i];
                a->velocity[2] = 0;

                if(j >= _size) {  
                    a->velocity[0] = 0;
                    a->velocity[1] = 0;
                    break;
                }
                  
                b = _atoms[j];
                rji = blitz::cross(b->position, a->position);
                mi = _mol_masses[a->symbol_index];
                mj = _mol_masses[b->symbol_index];
                mi_mj = -mi/mj;

                a->velocity[0] = genrand_open_close();
                a->velocity[1] = genrand_open_close();
                a->velocity[2] = -blitz::dot(a->velocity, rji)/rji[2];

                K = -blitz::cross(a->position,a->velocity) * mi_mj;
                d = sqr_vec(a->position);
                
                if(d == 0) {
                    b->velocity = b->position;
                }
                else {
                    b->velocity = b->position - blitz::cross(b->position, K)/d;
                }

                Vci += (a->velocity * mi + b->velocity * mj)/total_mass;
            }

            for(size_t i = 0; i < _size; ++i) {
                a = _atoms[i];
                a->velocity -= Vci;
                mi = _mol_masses[a->symbol_index] * g_per_mol_to_eV_ps2_A2 * 0.5;
                Ekin += mi * sqr_vec(a->velocity);
            }

            double f = std::sqrt(_E_temperature/Ekin);

            for(size_t i = 0; i < _size; ++i) {
                _atoms[i]->velocity *= f;
              //  std::cout << _atoms[i]->velocity << std::endl;              
            }

        }

        virtual void GetRandomTranslation(Vec3D &translation) = 0;
        virtual void PrintInfo() = 0;
        virtual std::string GetName() = 0;

    };
}