#pragma once

#include "irandomizer.hpp"

namespace sbox {
    class SimpleRandomizer : public IRandomizer {
        private:
        double _drmax;
        double _d;

        public:
        SimpleRandomizer(double drmax, uint32_t seed, Configuration *config) :
           IRandomizer(config, seed), _drmax(drmax), _d(2*drmax) {}

        void GetRandomTranslation(Vec3D &translation) override {
            translation = genrand_close_open() * _d - _drmax, 
                          genrand_close_open() * _d - _drmax,
                          genrand_close_open() * _d - _drmax;
        }

        std::string GetName() override {
            return "simple";
        }

        void PrintInfo() override {
            std::cout << "Simple Randomizer"<<std::endl;
            std::cout << "seed:  " << _seed << std::endl;
            std::cout << "drmax: " << _drmax << std::endl;
        }
    };
}