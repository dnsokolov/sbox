#pragma once

#include "randomizers/irandomizer.hpp"

namespace sbox {

    class MaxwellRandomizer : public IRandomizer {
        private:
        std::vector<double> _sqrt_mol_masses, _mol_masses;
        double _f0, _f;
        double _time;

        public:
        MaxwellRandomizer(double time, uint32_t seed, Configuration *config) :
           IRandomizer(config, seed), _f0(sqrt_R * time) {   
               _mol_masses = config->GetMolMasses();
               auto end = _mol_masses.end();

               for(auto i = _mol_masses.begin(); i != end; ++i) {
                   _sqrt_mol_masses.push_back(sqrt(*i));
               }

               _time = time;
           }

        void GetRandomTranslation(Vec3D &translation) override {
            _f = _f0 * _sqrt_temperature/_sqrt_mol_masses[_atom->symbol_index];

            translation = 
               _f * sqrt(-2.0 * log(genrand_open_close())) * cos(two_pi * genrand_close_open()),
               _f * sqrt(-2.0 * log(genrand_open_close())) * cos(two_pi * genrand_close_open()),
               _f * sqrt(-2.0 * log(genrand_open_close())) * cos(two_pi * genrand_close_open());
        }

        std::string GetName() override {
            return "maxwell";
        }

        void PrintInfo() override {
            std::cout << "Maxwell Randomizer" << std::endl;
            std::cout << "seed: " << _seed << std::endl;
            std::cout << "time: " << _time << std::endl;
        }
    };
}