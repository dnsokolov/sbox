#pragma once

#include "iintegrator.hpp"

namespace sbox {
    class VerletVelocity : public IIntegrator {
        private:
        std::vector<Vec3D> _vel_dt_2;
        size_t _indx;

        public:
        VerletVelocity(Configuration *config, IPotential *potential, double dt) : IIntegrator(config, potential, dt) {  
            _vel_dt_2.resize(config->GetNumOfAtoms()); 
        }

        void Init() override {
            _potential->GetForces();
        }

        void Renew() override {
            
        }

        void Update() override {
            _indx = 0;

            for(auto i = _atoms.begin(); i != _end_atoms; ++i) {
                _vel_dt_2[_indx] = (*i)->velocity + ((*i)->force * _dt_per_mass[_indx] * 0.5);
                (*i)->position += (_vel_dt_2[_indx] * _dt); 
                _geometry->Correction((*i)->position);
                ++_indx; 
            }

            _potential->GetForces();
            _indx = 0;

            for(auto i = _atoms.begin(); i != _end_atoms; ++i) {
                (*i)->velocity = _vel_dt_2[_indx] + ((*i)->force * _dt_per_mass[_indx] * 0.5);
                ++_indx;
            }
        }
    };
}