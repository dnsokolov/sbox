#pragma once

#include "iintegrator.hpp"

namespace sbox {
    class LeapFrog: public IIntegrator {
        private:
        size_t _indx;
        std::vector<Vec3D> _vels_half;

        public:
        LeapFrog(Configuration *config, IPotential *potential, double dt) : IIntegrator(config, potential, dt) {   
        }

        void Init() override {
            _potential->GetForces();
            _vels_half.clear();
            _indx = 0;

            for(auto i = _atoms.begin(); i != _end_atoms; ++i) {
                _vels_half.push_back((*i)->velocity + (*i)->force * _dt_per_mass[_indx] * 0.5);
                (*i)->position += (*i)->velocity * _dt;
                ++_indx; 
            }
        }

        void Renew() override {
            _vels_half.clear();
            _indx = 0;

            for(auto i = _atoms.begin(); i != _end_atoms; ++i) {
                _vels_half.push_back((*i)->velocity + (*i)->force * _dt_per_mass[_indx] * 0.5);
                (*i)->position += (*i)->velocity * _dt;
                _geometry->Correction((*i)->position);
                ++_indx; 
            }
        }

        void Update() override {
            _indx = 0;
            _potential->GetForces();

            for(auto i = _atoms.begin(); i != _end_atoms; ++i) {
                _vels_half[_indx] += (*i)->force * _dt_per_mass[_indx];
                (*i)->position += _vels_half[_indx] * _dt;
                _geometry->Correction((*i)->position);
                ++_indx; 
            }
        }
    };
}