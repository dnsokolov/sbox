#pragma once 

#include "iintegrator.hpp"

namespace sbox {
    class NoseHooverIntegrator : public IIntegrator {
        private:
        double _one_per_Q, _friction, _Ekin2, _Ekin;
        uint32_t _num_of_atoms;
        std::vector<Vec3D> _vel_dt_2;
        size_t _indx;

        public:
        NoseHooverIntegrator(Configuration *config, IPotential *potential, double dt, double temp, double Q) :
           IIntegrator(config, potential, dt), _one_per_Q(1.0/Q), _num_of_atoms(config->GetNumOfAtoms()) {
               _vel_dt_2.resize(_num_of_atoms);
               temperature = temp;
           }

        void Init() override {
            _potential->GetForces();
            _friction = _one_per_Q * (_config->GetKineticalEnergy() - 1.5 * (_num_of_atoms + 0.5) * k_boltzmann * temperature) * _dt;
        }

        void Renew() override {
            
        }

        void Update() override {
            _indx = 0;
            _Ekin2 = 0.0;
            _Ekin = 0.0;

            for(auto i = _atoms.begin(); i != _end_atoms; ++i) {
                (*i)->position += ((*i)->velocity * _dt) + ((*i)->force * _dt_per_mass[_indx] - _friction * (*i)->velocity * _dt) * _half_per_dt;
                _geometry->Correction((*i)->position);
                _vel_dt_2[_indx] = (*i)->velocity + ((*i)->force * _dt_per_mass[_indx] - _friction * (*i)->velocity * _dt) * 0.5;
                _Ekin2 += 0.5 * _mol_masses[(*i)->symbol_index] * g_per_mol_to_eV_ps2_A2 * blitz::dot(_vel_dt_2[_indx], _vel_dt_2[_indx]);
                _Ekin += 0.5 * _mol_masses[(*i)->symbol_index] * g_per_mol_to_eV_ps2_A2 * blitz::dot((*i)->velocity, (*i)->velocity);
                ++_indx; 
            }

            _friction += 0.5 * _dt * _one_per_Q * (_Ekin - 1.5 * (_num_of_atoms + 0.5) * k_boltzmann * temperature);
            _friction += 0.5 * _dt * _one_per_Q * (_Ekin2 - 1.5 * (_num_of_atoms + 0.5) * k_boltzmann * temperature);
            _indx = 0;

            _potential->GetForces();

            for(auto i = _atoms.begin(); i != _end_atoms; ++i) {
                (*i)->velocity = (_vel_dt_2[_indx] + (*i)->force * 0.5 * _dt_per_mass[_indx]) / (1.0 + _friction * _half_dt);
            }

        }
    };
}