#pragma once

#include "core/configuration.hpp"

namespace sbox {
    class IIntegrator : public Configuration::IRemoveAtomHandler {
        protected:
        Configuration *_config;
        std::vector<Atom*> _atoms;
        std::vector<Atom*>::iterator _end_atoms;
        double _dt, _half_per_dt, _half_dt;
        std::vector<double> _mol_masses;
        IGeometry *_geometry;
        IPotential *_potential;
        std::vector<double> _dt_per_mass;

        public:
        IIntegrator(Configuration *config, IPotential *potential, double dt) {
            _config = config;
            _atoms = config->GetAtoms();
            _dt = dt;
            _half_per_dt = 0.5/dt;
            _half_dt = 0.5 * dt;
            _mol_masses = config->GetMolMasses();
            _end_atoms = _atoms.end();
            _geometry = config->GetGeometry();
            _potential = potential;

            for(auto i = _atoms.begin(); i != _end_atoms; ++i) {
                _dt_per_mass.push_back(_dt / (_mol_masses[(*i)->symbol_index] * g_per_mol_to_eV_ps2_A2));
            }
        }

        double Get_dt() {
            return _dt;
        }

        double temperature;

        virtual void Init() = 0;
        virtual void Update() = 0;
        virtual void Renew() = 0;

        void CallRemoveAtomHandler(Configuration &config, Atom *atom) override {
            _atoms = config.GetAtoms();
            _end_atoms = _atoms.end();
        }
    };
}