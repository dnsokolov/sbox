#pragma once

#include "iintegrator.hpp"

namespace sbox {
    class Verlet : public IIntegrator {
        private:
        size_t _indx;
        
        public:
        Verlet(Configuration *config, IPotential *potential, double dt) : IIntegrator(config, potential, dt) {   
        }

        void Init() override {
            for(auto i = _atoms.begin(); i != _end_atoms; ++i) {
                (*i)->old_position = (*i)->position;
                (*i)->old_velocity = (*i)->velocity;
                (*i)->position += ((*i)->velocity * _dt);
            }
        }

        void Renew() override {
            
        }

        void Update() override {
            _indx = 0;
            _potential->GetForces();
            for(auto i = _atoms.begin(); i != _end_atoms; ++i) { 
                (*i)->position = (2.0 * (*i)->position) - ((*i)->old_position) + ((*i)->force * _dt_per_mass[_indx] * _dt);
                _geometry->Correction((*i)->position);
                (*i)->velocity = ((*i)->position - (*i)->old_position) * _half_per_dt;
                (*i)->old_position = (*i)->position;
                ++_indx;
            }
        }
    };
}