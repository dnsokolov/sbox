#pragma once

#include "ipotential.hpp"
#include "core/ceparamsreader.hpp"
#include <mutex>

namespace sbox {
    class Gupt: public IPotential {
        private:
        Array<double, 3> _params;
        Array<bool, 2> _bond_matrix, _interaction_matrix;
        Array<double, 2> _C_matrix, _D_matrix;
        Array<Vec3D,2> _attractive_matrix, _repulsive_matrix;
        std::vector<Atom*> _atoms;
        Array<double,1> _r_sqrt_rho, _repulsive_values, _attractive_values;
        double _repulsive, _attractive, _repulsive_t, _attractive_t, _attractive_piece, _repulsive_piece, _dist, _d;
        double _attractive_piece_old, _repulsive_piece_old;
        double *_mthr_repulsive = nullptr, *_mthr_attractive = nullptr;
        double *_mthr_repulsive_piece = nullptr, *_mthr_attractive_piece = nullptr;
        double *_mthr_repulsive_piece_old = nullptr, *_mthr_attractive_piece_old = nullptr;
        double *_mthr_Efull = nullptr;
        double _p3, _p3_2;
        int _a_indx, _a_indx_t, _i_indx, _j_indx;
        std::vector<Atom*>::iterator _begin_atoms, _end_atoms;
        size_t _num_of_atoms;
        int _counter_i, _counter_j;
        Vec3D _dr, _eij, _attractive_v, _repulsive_v;
        size_t _num_of_threads{0};
        std::mutex mutex;

        public:
        Gupt(Configuration *config, size_t num_of_threads) :
           IPotential(config, num_of_threads)
        {   
            int num_of_components = static_cast<int>(config->GetNumOfTypes());
            _num_of_atoms = config->GetNumOfAtoms();
            _params.resize(5, num_of_components, num_of_components);
            _bond_matrix.resize(num_of_components, num_of_components);
            _bond_matrix = false;
            _C_matrix.resize(num_of_components, num_of_components);
            _D_matrix.resize(num_of_components, num_of_components);
            _C_matrix = 0; _D_matrix = 0;
            _atoms = _config->GetAtoms();
            _geometry = config->GetGeometry();
            _rcut = _geometry->GetRcut();
            _begin_atoms = _atoms.begin();
            _end_atoms = _atoms.end();
            _r_sqrt_rho.resize(_num_of_atoms);
            _repulsive_values.resize(_num_of_atoms);
            _attractive_values.resize(_num_of_atoms);
            _repulsive_values = 0;
            _attractive_values = 0;
            _attractive_matrix.resize(_num_of_atoms, _num_of_atoms);
            _repulsive_matrix.resize(_num_of_atoms, _num_of_atoms);
            _interaction_matrix.resize(_num_of_atoms, _num_of_atoms);
            _interaction_matrix = false;

            if(_multithreads_flag) {
               _num_of_threads = num_of_threads;
               _mthr_attractive = new double[num_of_threads];
               _mthr_repulsive = new double[num_of_threads];
               _mthr_attractive_piece = new double[num_of_threads];
               _mthr_repulsive_piece = new double[num_of_threads];
               _mthr_repulsive_piece_old = new double[num_of_threads];
               _mthr_attractive_piece_old = new double[num_of_threads];
               _mthr_Efull = new double[num_of_threads];
            }
            else {
                _num_of_threads = 1;
                _mthr_attractive = new double[_num_of_threads];
                _mthr_repulsive = new double[_num_of_threads];
                _mthr_attractive_piece = new double[_num_of_threads];
                _mthr_repulsive_piece = new double[_num_of_threads];
                _mthr_repulsive_piece_old = new double[_num_of_threads];
                _mthr_attractive_piece_old = new double[_num_of_threads];
                _mthr_Efull = new double[_num_of_threads];
            }
        }

        ~Gupt() {
            if(_mthr_repulsive) delete [] _mthr_repulsive;
            if(_mthr_attractive) delete [] _mthr_attractive;
            if(_mthr_attractive_piece) delete [] _mthr_attractive_piece;
            if(_mthr_repulsive_piece) delete [] _mthr_repulsive_piece;
            if(_mthr_attractive_piece_old) delete [] _mthr_attractive_piece_old;
            if(_mthr_repulsive_piece_old) delete [] _mthr_repulsive_piece_old;
            if(_mthr_Efull) delete [] _mthr_Efull;
        }

        void DoEnergyJob(Atom *a, const Range<size_t> &r, std::vector<Atom*> &neighbours) override {
            _mthr_attractive[r.id] = 0.0;
            _mthr_repulsive[r.id] = 0.0;
            _mthr_Efull[r.id] = 0.0;

            int _a_indx = a->symbol_index, _i_indx;
            double _d, _p3, _dist;
            size_t thread_id = r.id, finish = r.finish;
            Atom **i;
            
            for(auto j = r.start; j < finish ; ++j) {
                i = &_atoms[j];
                if((*i) == a) continue;
                _i_indx = (*i)->symbol_index;
                if(!_bond_matrix(_i_indx, _a_indx)) continue;
                _dist = _geometry->GetDistanceThreadSafe(a, *i);

                if((_rcut > 0.0 && _dist < _rcut) || _rcut <= 0) {
                   _d = 1.0 - _dist/_params(0,_a_indx, _i_indx);
                   _mthr_repulsive_piece[thread_id] = _params(1,_a_indx, _i_indx) * exp(_params(2,_a_indx, _i_indx) * _d);
                   _mthr_repulsive[thread_id] += _mthr_repulsive_piece[thread_id];
                   _p3 = _params(3,_a_indx, _i_indx);
                   _mthr_attractive_piece[thread_id] = _p3 * _p3 * exp(2.0 * _params(4,_a_indx, _i_indx) * _d);
                   _mthr_attractive[thread_id] += _mthr_attractive_piece[thread_id];

                   (*i)->SetOldEnergy();
                   _dist = _geometry->GetOldDistanceThreadSafe(a, *i);

                   _mthr_repulsive_piece_old[thread_id] = 0.0;
                   _mthr_attractive_piece_old[thread_id] = 0.0;

                   if((_rcut > 0.0 && _dist < _rcut) || (_rcut <= 0)) {
                        _d = 1.0 - _dist/_params(0,_a_indx, _i_indx);
                        _mthr_repulsive_piece_old[thread_id] = _params(1,_a_indx, _i_indx) * exp(_params(2,_a_indx, _i_indx) * _d );
                        _mthr_attractive_piece_old[thread_id] = _p3 * _p3 * exp(2.0 * _params(4,_a_indx, _i_indx) * _d);
                    }

                    _repulsive_values((*i)->index) += (_mthr_repulsive_piece[thread_id] - _mthr_repulsive_piece_old[thread_id]);
                    _attractive_values((*i)->index) += (_mthr_attractive_piece[thread_id] - _mthr_attractive_piece_old[thread_id]);
                   
                    (*i)->energy = _repulsive_values((*i)->index) - sqrt(_attractive_values((*i)->index));

                    if(!(*i)->neighbour_flag) {
                        mutex.lock();
                        neighbours.push_back(*i);
                        mutex.unlock();
                    }

                   (*i)->neighbour_flag = false;
                }
                else {
                    if((*i)->neighbour_flag) {
                        (*i)->neighbour_flag = false;  
                        _dist = _geometry->GetOldDistanceThreadSafe(a, *i);

                        if((_rcut > 0.0 && _dist < _rcut) || (_rcut <= 0)) {
                            (*i)->SetOldEnergy();
                            _d = 1.0 - _dist/_params(0,_a_indx, _i_indx);
                            _p3 = _params(3,_a_indx, _i_indx);
                            _mthr_repulsive_piece_old[thread_id] = _params(1,_a_indx, _i_indx) * exp(_params(2,_a_indx, _i_indx) * _d );
                            _mthr_attractive_piece_old[thread_id] = _p3 * _p3 * exp(2.0 * _params(4,_a_indx, _i_indx) * _d);

                            _repulsive_values((*i)->index) -= _mthr_repulsive_piece_old[thread_id];
                            _attractive_values((*i)->index) -= _mthr_attractive_piece_old[thread_id];

                            (*i)->energy = _repulsive_values((*i)->index) - sqrt(_attractive_values((*i)->index));            
                        }
                        else {
                            std::cout << "bad" << std::endl;
                        }
                    } 
                }

                _mthr_Efull[thread_id] += (*i)->energy;
            }
        }

        void DoEnergyJob(Atom *atom, const Range<size_t> &r) override {
            size_t thread_id = r.id;

            _mthr_repulsive[thread_id] = 0.0;
            _mthr_attractive[thread_id] = 0.0;
            _mthr_Efull[thread_id] = 0.0;

            int _a_indx_t = atom->symbol_index, _i_indx;
            double _dist, _d, _p3;
            Atom **i;
            size_t finish = r.finish;
            
            for(auto j = r.start; j < finish ; ++j) {
                i = &_atoms[j];
                if((*i) == atom) continue;
                _i_indx = (*i)->symbol_index;
                if(!_bond_matrix(_i_indx, _a_indx_t)) continue;
                _dist = _geometry->GetDistanceThreadSafe(atom, *i);

                if((_rcut > 0.0 && _dist < _rcut) || (_rcut <= 0)) {
                   _d = 1.0 - _dist/_params(0,_a_indx_t, _i_indx);
                   _mthr_repulsive[thread_id] += _params(1,_a_indx_t, _i_indx) * exp(_params(2,_a_indx_t, _i_indx) * _d);
                   _p3 = _params(3,_a_indx_t, _i_indx);
                   _mthr_attractive[thread_id] += _p3 * _p3 * exp(2.0 * _params(4,_a_indx_t, _i_indx) * _d);
                }
            }
        }

        double GetEnergyJob(Atom *atom) override {
            _repulsive = 0.0;
            _attractive = 0.0;
            _Efull = 0.0;

            for(size_t i = 0; i < _num_of_threads; ++i) {
                _repulsive += _mthr_repulsive[i];
                _attractive += _mthr_attractive[i];
                _Efull += _mthr_Efull[i];
            }

            atom->energy = _repulsive - sqrt(_attractive);
            _Efull += atom->energy;

            _repulsive_values(atom->index) = _repulsive;
            _attractive_values(atom->index) = _attractive;

            return atom->energy;
        }

        void LoadParamsFromCEFile(const std::string &cefile) {
            CEParamsReader reader(cefile);
            std::vector<std::string> components = _config->GetSymbols();

            std::string params;
            auto end = components.end();
            auto begin = components.begin();

            for(auto i = begin; i != end; ++i) {
                params = reader.ReadTBPotential(*i, *i);
                AddParams(params);
                for(auto j = (begin + 1); j != end; ++j) {
                    params = reader.ReadTBPotential(*i, *j);
                    AddParams(params);
                }
            }
        }

        void AddParams(const std::string &params) override {
            std::stringstream strstr(params);
            std::string pot_name, comp1, comp2;

            strstr >> pot_name >> comp1 >> comp2;
            if(pot_name != "gupt") {
                throw "Wrong potential name in 'void Gupt::AddParams(const std::string &params)'";
            }

            std::vector<std::string> components = _config->GetSymbols();
            int i_comp1 = static_cast<int>(index_of(components, comp1));
            int i_comp2 = static_cast<int>(index_of(components, comp2));

            if(i_comp1 == n_pos || i_comp2 == n_pos) {
                throw "Wrong components in 'void Gupt::AddParams(const std::string &params)'";
            }

            double p0, p1, p2, p3, p4;
            strstr >> p0 >> p1 >> p2 >> p3 >> p4;

            _params(0, i_comp1, i_comp2) = p0;
            _params(1, i_comp1, i_comp2) = p1;
            _params(2, i_comp1, i_comp2) = p2;
            _params(3, i_comp1, i_comp2) = p3;
            _params(4, i_comp1, i_comp2) = p4;
            _bond_matrix(i_comp1, i_comp2) = true;
            _C_matrix(i_comp1, i_comp2) =  2.0 * p1 * p2 / p0;
            _D_matrix(i_comp1, i_comp2) = p3 * p3 * p4/ p0;

            if(i_comp1 != i_comp2) {
               _params(0, i_comp2, i_comp1) = p0;
               _params(1, i_comp2, i_comp1) = p1;
               _params(2, i_comp2, i_comp1) = p2;
               _params(3, i_comp2, i_comp1) = p3;
               _params(4, i_comp2, i_comp1) = p4;
               _bond_matrix(i_comp2, i_comp1) = true;
               _C_matrix(i_comp2, i_comp1) =  2.0 * p1 * p2 / p0;
               _D_matrix(i_comp2, i_comp1) = p3 * p3 * p4/ p0;
            }   
        }

        std::string GetParams(const std::string &comp1, const std::string &comp2,std::vector<double> &params) override {
            std::vector<std::string> components = _config->GetSymbols();
            int i_comp1 = static_cast<int>(index_of(components, comp1));
            int i_comp2 = static_cast<int>(index_of(components, comp2));

            if(i_comp1 == n_pos || i_comp2 == n_pos) {
                throw "Wrong components in 'std::string Gupt::GetParams(const std::string &comp1, const std::string &comp2,std::vector<double> &params)'";
            }

            params.push_back(_params(0, i_comp1, i_comp2));
            params.push_back(_params(1, i_comp1, i_comp2));
            params.push_back(_params(2, i_comp1, i_comp2));
            params.push_back(_params(3, i_comp1, i_comp2));
            params.push_back(_params(4, i_comp1, i_comp2));

            return "gupt (ro A p B q)";
        }

        double GetAtomEnergy(Atom *atom) override {
            _repulsive_t = 0.0;
            _attractive_t = 0.0;
            _a_indx_t = atom->symbol_index;
            
            for(auto i = _begin_atoms; i != _end_atoms; ++i) {
                if((*i) == atom) continue;
                _i_indx = (*i)->symbol_index;
                if(!_bond_matrix(_i_indx, _a_indx_t)) continue;
                _dist = _geometry->GetDistance(atom, *i);

                if((_rcut > 0.0 && _dist < _rcut) || (_rcut <= 0)) {
                   _d = 1.0 - _dist/_params(0,_a_indx_t, _i_indx);
                   _repulsive_t += _params(1,_a_indx_t, _i_indx) * exp(_params(2,_a_indx_t, _i_indx) * _d);
                   _p3 = _params(3,_a_indx_t, _i_indx);
                   _attractive_t += _p3 * _p3 * exp(2.0 * _params(4,_a_indx_t, _i_indx) * _d);
                }
            }

            _repulsive_values(atom->index) = _repulsive_t;
            _attractive_values(atom->index) = _attractive_t;

            atom->energy = _repulsive_t - sqrt(_attractive_t);
            return atom->energy;
        }

        void RejectInteraction(Atom *a1, Atom *a2) override {
            _dist = _geometry->GetDistance(a1, a2);

            _a_indx = a1->symbol_index;
            _i_indx = a2->symbol_index;
            _p3 = _params(3,_a_indx, _i_indx);

            if((_rcut > 0.0 && _dist < _rcut) || (_rcut <= 0)){
                _d = 1.0 - _dist/_params(0,_a_indx, _i_indx);
                _repulsive_piece = _params(1,_a_indx, _i_indx) * exp(_params(2,_a_indx, _i_indx) * _d);     
                _attractive_piece = _p3 * _p3 * exp(2.0 * _params(4,_a_indx, _i_indx) * _d);
                _attractive_values(a1->index) -= _attractive_piece;
                _repulsive_values(a1->index) -= _repulsive_piece;
                _attractive_values(a2->index) -= _attractive_piece;
                _repulsive_values(a2->index) -= _repulsive_piece;
            }

            _dist = _geometry->GetOldDistance(a1, a2);

            if((_rcut > 0.0 && _dist < _rcut) || (_rcut <= 0)){
                _d = 1.0 - _dist/_params(0,_a_indx, _i_indx);
                _repulsive_piece = _params(1,_a_indx, _i_indx) * exp(_params(2,_a_indx, _i_indx) * _d);
                _attractive_piece = _p3 * _p3 * exp(2.0 * _params(4,_a_indx, _i_indx) * _d);
                _attractive_values(a1->index) += _attractive_piece;
                _repulsive_values(a1->index) += _repulsive_piece;
                _attractive_values(a2->index) += _attractive_piece;
                _repulsive_values(a2->index) += _repulsive_piece;
            }       
        }

        double GetAtomEnergy(Atom *atom, std::vector<Atom*> &neighbours) override {
            _repulsive = 0.0;
            _attractive = 0.0;
            _a_indx = atom->symbol_index;
            _Efull = 0.0;
            
            for(auto i = _begin_atoms; i != _end_atoms; ++i) {
                if((*i) == atom) continue;
                _i_indx = (*i)->symbol_index;
                if(!_bond_matrix(_i_indx, _a_indx)) continue;
                _dist = _geometry->GetDistance(atom, *i);

                if((_rcut > 0.0 && _dist < _rcut) || is_zero_eps(_rcut)) {
                   _d = 1.0 - _dist/_params(0,_a_indx, _i_indx);
                   _repulsive_piece = _params(1,_a_indx, _i_indx) * exp(_params(2,_a_indx, _i_indx) * _d );
                   _repulsive += _repulsive_piece;
                   _p3 = _params(3,_a_indx, _i_indx);
                   _attractive_piece = _p3 * _p3 * exp(2.0 * _params(4,_a_indx, _i_indx) * _d);
                   _attractive += _attractive_piece;

                   (*i)->SetOldEnergy();
                   _dist = _geometry->GetOldDistance(atom, *i);

                   _repulsive_piece_old = 0.0;
                   _attractive_piece_old = 0.0;

                   if((_rcut > 0.0 && _dist < _rcut) || (_rcut <= 0)) {
                        _d = 1.0 - _dist/_params(0,_a_indx, _i_indx);
                        _repulsive_piece_old = _params(1,_a_indx, _i_indx) * exp(_params(2,_a_indx, _i_indx) * _d );
                        _attractive_piece_old = _p3 * _p3 * exp(2.0 * _params(4,_a_indx, _i_indx) * _d);
                    }
                   
                   _repulsive_values((*i)->index) += (_repulsive_piece - _repulsive_piece_old);
                   _attractive_values((*i)->index) += (_attractive_piece - _attractive_piece_old);
                   
                   (*i)->energy = _repulsive_values((*i)->index) - sqrt(_attractive_values((*i)->index));

                   if(!(*i)->neighbour_flag) {
                      neighbours.push_back(*i);
                   }

                   _Efull += (*i)->energy;   
                       
                   (*i)->neighbour_flag = false;
                }
                else {
                    if((*i)->neighbour_flag) {
                        (*i)->neighbour_flag = false;  
                        _dist = _geometry->GetOldDistance(atom, *i);

                        if((_rcut > 0.0 && _dist < _rcut) || (_rcut <= 0)) {
                            (*i)->SetOldEnergy();
                            _d = 1.0 - _dist/_params(0,_a_indx, _i_indx);
                            _p3 = _params(3,_a_indx, _i_indx);
                            _repulsive_piece_old = _params(1,_a_indx, _i_indx) * exp(_params(2,_a_indx, _i_indx) * _d );
                            _attractive_piece_old = _p3 * _p3 * exp(2.0 * _params(4,_a_indx, _i_indx) * _d);

                            _repulsive_values((*i)->index) -= _repulsive_piece_old;
                            _attractive_values((*i)->index) -= _attractive_piece_old;

                            (*i)->energy = _repulsive_values((*i)->index) - sqrt(_attractive_values((*i)->index));            
                        }
                        else {
                            std::cout << "bad" << std::endl;
                        }
                    } 

                    _Efull += (*i)->energy;
                }
            }

            _repulsive_values(atom->index) = _repulsive;
            _attractive_values(atom->index) = _attractive;

            atom->energy = _repulsive - sqrt(_attractive);
            _Efull += atom->energy;

            return atom->energy;
        }

        std::string GetName() override {
            return "gupt";
        }

        std::string GetParamsString(const std::string &comp1, const std::string &comp2) {
            std::vector<std::string> symbols = _config->GetSymbols();
            size_t cmp1 = index_of(symbols, comp1);

            if(cmp1 == n_pos) {
                throw "Invalid comp1 in 'Gupt::GetParamsString(const std::string &comp1, const std::string &comp2)'";
            }

            size_t cmp2 = index_of(symbols, comp2);

            if(cmp2 == n_pos) {
                throw "Invalid comp2 in 'Gupt::GetParamsString(const std::string &comp1, const std::string &comp2)'";
            }

            int i_cmp1 = static_cast<int>(cmp1);
            int i_cmp2 = static_cast<int>(cmp2);

            std::string result = "gupt " + comp1 + " " + comp2 + " ";
            for(int i = 0; i < 5; ++i) {
                result += as_string(_params(i, i_cmp1, i_cmp2)) + " ";
            }
            
            return result.substr(0, result.size()-1);
        }

        size_t n = 0;

        double getLenth(const Vec3D &v) {
            return sqrt(blitz::dot(v,v));
        }

        void GetForces() override {
            for(_counter_i = 0;_counter_i < _num_of_atoms; ++_counter_i) {
                _i_indx = _atoms[_counter_i]->symbol_index;
                _attractive = 0.0;

                for(_counter_j = 0;_counter_j < _num_of_atoms; ++_counter_j) {
                    if(_counter_j == _counter_i) continue;

                    _j_indx = _atoms[_counter_j]->symbol_index;

                    if(!_bond_matrix(_i_indx, _j_indx)) {
                        _repulsive_matrix(_counter_i, _counter_j) = 0.0;
                        _repulsive_matrix(_counter_j, _counter_i) = 0.0;
                        _attractive_matrix(_counter_i, _counter_j) = 0.0;
                        _attractive_matrix(_counter_j, _counter_i) = 0.0;
                        continue;
                    }

                    _dr = _atoms[_counter_j]->position - _atoms[_counter_i]->position;
                    _dist = _geometry->GetDistance(_dr);
                    _p3 = _params(3, _i_indx,_j_indx);
                    _d = 1.0 - _dist/_params(0, _i_indx, _j_indx);

                    if((_rcut > 0.0 && _dist <= _rcut) || is_zero_eps(_rcut)) {
                       if(_counter_i < _counter_j) {
                         _eij = _dr/_dist;         
                         _repulsive_matrix(_counter_i, _counter_j) = (_C_matrix(_i_indx, _j_indx) * exp(_params(2,_i_indx, _j_indx) * _d ) * _eij);
                         _attractive_matrix(_counter_i, _counter_j) = (_D_matrix(_i_indx, _j_indx) * exp(2.0 * _params(4,_i_indx, _j_indx) * _d) * _eij);
                         _repulsive_matrix(_counter_j, _counter_i) = -_repulsive_matrix(_counter_i, _counter_j);
                         _attractive_matrix(_counter_j, _counter_i) = -_attractive_matrix(_counter_i, _counter_j);
                         _interaction_matrix(_counter_i,_counter_j) = true;
                         _interaction_matrix(_counter_j, _counter_i) = true;              
                        }
                    }  

                    if(_interaction_matrix(_counter_i, _counter_j)) {
                        _attractive += _p3 * _p3 * exp(2.0 * _params(4, _i_indx, _j_indx) * _d);
                    }  
                }
                
                _r_sqrt_rho(_counter_i) = 1.0/sqrt(_attractive);            
            } 

            for(_counter_i = 0; _counter_i < _num_of_atoms; ++_counter_i) {
                _attractive_v = 0.0;
                _repulsive_v = 0.0;

                for(_counter_j = 0; _counter_j < _num_of_atoms; ++_counter_j) {
                    if(_interaction_matrix(_counter_i, _counter_j)) {
                      _repulsive_v += _repulsive_matrix(_counter_i, _counter_j);
                      _attractive_v += ((_r_sqrt_rho(_counter_i) + _r_sqrt_rho(_counter_j)) * _attractive_matrix(_counter_i,_counter_j));
                      _interaction_matrix(_counter_i, _counter_j) = false;
                    }
                }

                _atoms[_counter_i]->force = _attractive_v - _repulsive_v;
            }   

        } 

        void MultithreadingInit(size_t num_of_threads) {

        }

        void PrintInfo() override {
            std::cout << "Potential type: gupt" << std::endl;
            std::cout << "Parameters: (ro A p B q)" << std::endl; 
            std::vector<std::string> symbols = _config->GetSymbols();
            size_t size = symbols.size();
            
            for(int i = 0; i < size; ++i) {
                std::cout << "   ---> " << GetParamsString(symbols[i], symbols[i]) << std::endl;
                for(int j = i + 1; j < size; ++j) {
                    std::cout << "   ---> " << GetParamsString(symbols[i], symbols[j]) << std::endl;
                }
            }
            std::cout << "Calculation Strategy: " << _strategy->GetName() << std::endl;
        }
    };
}