#pragma once

#include "core/configuration.hpp"
#include "core/range.hpp"
#include "core/threadpool.hpp"

namespace sbox {
    class IPotential : public Configuration::IRemoveAtomHandler {
        public:
        class ICalculationStrategy : public Configuration::IRemoveAtomHandler {
            protected:
            IPotential *_potential = nullptr;
            Configuration *_config = nullptr;
            std::vector<Atom*> _atoms;
            std::vector<double> _masses;
            size_t _num_of_atoms;

            private:
            void SetPotential(IPotential *potential) {
                _potential = potential;
            }

            void SetConfig(Configuration *config) {
                _config = config;
                _atoms = config->GetAtoms();
                _num_of_atoms = _atoms.size();
            }

            void SetMasses(const std::vector<double> &masses) {
                _masses = masses;
            }

            public:
            ICalculationStrategy() {}
            virtual double GetFullEnergy() = 0;
            virtual double GetFullEnergy(Atom *atom, std::vector<Atom*> &neighbours) = 0;
            virtual double GetFullEnergy(double &Epot, double &Ekin) = 0;
            virtual std::string GetName() = 0;

            virtual void CallRemoveAtomHandler(Configuration &config, Atom *removed_atom) {
                _atoms = config.GetAtoms();
                _num_of_atoms = _atoms.size();
            }

            friend class IPotential;
        };

        class OneThreadStrategy : public ICalculationStrategy {
            private:
            double _sum, _dE;

            public:
            double GetFullEnergy() override {
               auto end = _atoms.end();
               _sum = 0.0;

               for(auto i = _atoms.begin(); i != end; ++i) {
                 _sum += _potential->GetAtomEnergy(*i);
               }

               return _sum;
            }

            double GetFullEnergy(Atom *atom, std::vector<Atom*> &neighbours) override {
                _potential->GetAtomEnergy(atom, neighbours);
                return _potential->_Efull;
            }
 
            double GetFullEnergy(double &Epot, double &Ekin) override {
                auto end = _atoms.end();
                Epot = 0.0;
                Ekin = 0.0;

                for(auto i = _atoms.begin(); i != end; ++i) {
                    Epot += _potential->GetAtomEnergy(*i);
                    Ekin += (0.5 * _masses[(*i)->symbol_index] * blitz::dot((*i)->velocity, (*i)->velocity));
                }

                return Epot + Ekin;
            }

            std::string GetName() {
                return "onethread";
            }

         };

         class MultiThreadsStrategy : public ICalculationStrategy {
             private:
             Range<size_t> *_ranges = nullptr;
             bool _multithread_flag = false;
             Atom *_current_atom;
             std::vector<Atom*> *_current_list;
             std::vector<std::function<void(size_t)>> _jobs;
             ThreadPool *_thread_pool = nullptr;
             size_t _num_of_threads;
             double _sum;

             double GetAtomEnergy(Atom *a) {
                _current_atom = a;
                _thread_pool->DoJob(1);
                _potential->DoEnergyJob(a, _ranges[_num_of_threads - 1]);
                _thread_pool->Wait();
                return _potential->GetEnergyJob(a);
             }

             public:
             bool Init(size_t num_of_threads) {
                 if(_config == nullptr) {
                     std::cerr << "_config is null" << std::endl;
                     _multithread_flag = false;
                     return false;
                 }

                 if(num_of_threads <= 1) {
                     std::cerr << "_num_of_threads (<= 1) = " << num_of_threads << std::endl;
                     _multithread_flag = false;
                     return false;
                 }

                 _num_of_threads = num_of_threads;
                 _ranges = generate_uint_distr_ranges(_num_of_atoms, _num_of_threads);

                  _jobs.push_back([&](size_t job_id) {
                      _potential->DoEnergyJob(_current_atom, _ranges[job_id], *_current_list);
                  });

                  _jobs.push_back([&](size_t job_id) {
                      _potential->DoEnergyJob(_current_atom, _ranges[job_id]);
                  });

                  std::cout << "Try to create a thread pool " << _num_of_threads << " threads" << std::endl;
                  _thread_pool = new ThreadPool(_num_of_threads - 1, _jobs);

                  if((_multithread_flag = _thread_pool->IsInitialized())) {
                    std::cout << "Multithreading is initialized" << std::endl;
                    std::cout << "Num of threads: " << _num_of_threads << std::endl;
                    std::cout << "Work distribution: " << std::endl;

                    for(size_t i = 0; i < num_of_threads - 1; ++i) {
                        std::cout << "Thread '" << i << "' -> [" << _ranges[i].start << ", " << _ranges[i].finish << ")" << std::endl;
                    }

                    std::cout << "Main Thread -> [" << _ranges[_num_of_threads-1].start << ", " << _ranges[_num_of_threads-1].finish << ")" << std::endl << std::endl;
                  }
                  else {
                    std::cout << "Creating thread pool is failed" << std::endl;
                    delete _thread_pool;
                    _thread_pool = nullptr;
                    delete [] _ranges;
                    _ranges = nullptr;
                    _jobs.clear();
                  }

                  return _multithread_flag;
             }

            double GetFullEnergy(Atom *atom, std::vector<Atom*> &neighbours) override {
                _current_atom = atom;
                _current_list = &neighbours;   
                _thread_pool->DoJob(0);
                _potential->DoEnergyJob(atom, _ranges[_num_of_threads-1], *_current_list);
                _thread_pool->Wait();
                _potential->GetEnergyJob(atom);
                return _potential->_Efull;
            }

            double GetFullEnergy() override {
                auto end = _atoms.end();
                double Epot = 0;

                for(auto i = _atoms.begin(); i != end; ++i) {
                    Epot += GetAtomEnergy(*i);
                }

                return Epot;
            }

            double GetFullEnergy(double &Ekin, double &Epot) override {
                auto end = _atoms.end();
                Epot = 0.0;
                Ekin = 0.0;

                for(auto i = _atoms.begin(); i != end; ++i) {
                    Epot += GetAtomEnergy(*i);
                    Ekin += (0.5 * _masses[(*i)->symbol_index] * blitz::dot((*i)->velocity, (*i)->velocity));
                }

                return Epot + Ekin;
            }

            std::string GetName() {
                return "multithreads";
            }

            void CallRemoveAtomHandler(Configuration &config, Atom *removed_atom) override {
                _atoms = config.GetAtoms();
                _num_of_atoms = _atoms.size();
                change_uint_distr_ranges(_ranges, _num_of_atoms, _num_of_threads);
            }

            ~MultiThreadsStrategy() {
                if(_thread_pool) delete _thread_pool; 
                if(_ranges) delete [] _ranges;
            }

         };

        protected:
        Configuration *_config;
        std::vector<Atom*> _atoms;
        std::vector<double> _masses;
        size_t _num_of_atoms;
        ICalculationStrategy *_strategy = nullptr;
        bool _multithreads_flag = false;
        IGeometry *_geometry;
        double _Efull, _rcut;

        public:
        IPotential(Configuration *config, size_t num_of_threads) {
            _config = config;
            _atoms = config->GetAtoms();
            _num_of_atoms = _atoms.size();

            std::vector<double> mol_masses = config->GetMolMasses();
            auto end = mol_masses.end();

            for(auto i = mol_masses.begin(); i != end; ++i) {
                _masses.push_back((*i) * g_per_mol_to_eV_ps2_A2);
            }

            if(num_of_threads <= 1) _strategy = new OneThreadStrategy();
            else _strategy = new MultiThreadsStrategy();

            _strategy->SetConfig(_config);
            _strategy->SetMasses(_masses);
            _strategy->SetPotential(this);

            if(_strategy->GetName() == "multithreads") {
                MultiThreadsStrategy *mth = static_cast<MultiThreadsStrategy*>(_strategy);
                _multithreads_flag = mth->Init(num_of_threads);
            }
        }

        ~IPotential() {
            if(_strategy != nullptr) {
                delete _strategy;
                _strategy = nullptr; 
            }
        }

        ICalculationStrategy *GetStrategy() {
            return _strategy;
        }

        double GetFullEnergy() {
            return _strategy->GetFullEnergy();
        }

        double GetFullEnergy(Atom *atom, std::vector<Atom*> &neighbours) {
            return _strategy->GetFullEnergy(atom, neighbours);
        }

        double GetFullEnergy(double &Epot, double &Ekin) {
            return _strategy->GetFullEnergy(Epot, Ekin);
        }

        double GetEnergyOfAtom(size_t indx) {
            return GetAtomEnergy(_atoms[indx]);
        }

        void GetNeighbours(Atom *a, std::vector<Atom*> &neighbours) {
            auto end = _atoms.end();

            for(auto i = _atoms.begin(); i != end; ++i) {
                if((*i) == a) continue;
                (*i)->old_position = (*i)->position;
                if(_geometry->GetDistance(*i, a) < _rcut) {
                    (*i)->neighbour_flag = true;
                    neighbours.push_back(*i);
                }
            }
        }

        void RejectConfig(Atom *a, std::vector<Atom*> &neighbours) {
            auto end = neighbours.end();

            for(auto i = neighbours.begin(); i != end; ++i) {
                RejectInteraction(a,*i);
                (*i)->BackToOldEnergy();
            }

            a->BackToOldConfig();
        }

        virtual void CallRemoveAtomHandler(Configuration &config, Atom *atom) {
            _atoms = config.GetAtoms();
            _num_of_atoms = _atoms.size();
        }

        virtual double GetAtomEnergy(Atom *atom) = 0;
        virtual double GetAtomEnergy(Atom *atom, std::vector<Atom*> &neighbours) = 0;
        virtual void DoEnergyJob(Atom *a, const Range<size_t> &r, std::vector<Atom*> &neighbours) = 0;
        virtual void DoEnergyJob(Atom *a, const Range<size_t> &r) = 0;
        virtual double GetEnergyJob(Atom *a) = 0;
        virtual void RejectInteraction(Atom *a1, Atom *a2) = 0;

        virtual void GetForces() = 0;
        virtual std::string GetName() = 0;
        virtual void PrintInfo() = 0;
        virtual void AddParams(const std::string &params) = 0;
        virtual std::string GetParams(const std::string &comp1, const std::string &comp2, std::vector<double> &params) = 0;
    };
}