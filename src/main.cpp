#include "core/luaengine.hpp"
#include "commands/commands.hpp"

int main(int argc, char **argv) {
    std::vector<std::string> args;
    sbox::LuaEngine lua;
    sbox::Commands commands(&lua);

#ifdef _WIN32
    setlocale(LC_ALL, "");
#endif // _WIN32

    sbox::print_logo();

    if(argc == 1) {
        std::cout << "Enter --help for commands helping" << std::endl << std::endl;
    }
    
    if(argc>1) {

        if(argc>2) {
            for(size_t i = 2; i < argc; ++i) {
                args.push_back(argv[i]);
            }
        }

        std::string filename = argv[1];
        int cmd_code = -1;

        if(filename == "--help") {
            commands.GetHelp();
            return EXIT_SUCCESS;
        }

        if((cmd_code = commands.Execute(filename, args)) == 0) {
            return EXIT_SUCCESS;
        }

        if(std::filesystem::exists(filename)) {
            lua.Execute(filename);
        }
        else if (cmd_code == -1) {
            std::cerr << "Error: Script '" << filename << "' is not founded" << std::endl;
        }
        else {
            std::cerr << "Error: Command '" << filename << "' return code " << cmd_code << std::endl;
        }
    }

    return EXIT_SUCCESS;
}