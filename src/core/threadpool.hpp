#pragma once

#include "threadworker.hpp"
#include <exception>

namespace sbox {
    class ThreadPool {
        private:
        ThreadWorker **_workers = nullptr;
        size_t _num_of_threads, i;
        bool _is_initialized{false};
        std::atomic_bool *_shared_start, *_run_flag;
        std::atomic_int _shared_counter;
        std::atomic_int _task_type;

        public:
        ThreadPool(size_t num_of_threads, const std::vector<std::function<void(size_t)>> &jobs) {
            _num_of_threads = num_of_threads;
            _shared_start = new std::atomic_bool[_num_of_threads];
            _run_flag = new std::atomic_bool[_num_of_threads];
            _workers = new ThreadWorker*[_num_of_threads];

            for(size_t i = 0; i < num_of_threads; ++i) {
                _run_flag[i].store(true);
                _shared_start[i].store(false);
                _workers[i] = new ThreadWorker(jobs, i, &_shared_start[i], &_shared_counter, &_task_type, _num_of_threads);
                _workers[i]->Execute();
            }

            _is_initialized = true;
        }

        bool IsInitialized() {
            return _is_initialized;
        }

        size_t GetNumOfThreads() const {
            return _num_of_threads;
        }

        void DoJob(size_t t) {
            _shared_counter.store(0);
            _task_type.store(t);
            std::memcpy(_shared_start, _run_flag, sizeof(std::atomic_bool) * _num_of_threads);
        }

        void Wait() {
            if(_shared_counter.load() == _num_of_threads) return;
            if(_shared_counter.load() == _num_of_threads) return;
            if(_shared_counter.load() == _num_of_threads) return;
            if(_shared_counter.load() == _num_of_threads) return;
            if(_shared_counter.load() == _num_of_threads) return;
            if(_shared_counter.load() == _num_of_threads) return;

            while(true) {
                if(_shared_counter.load() == _num_of_threads) return;
                if(_shared_counter.load() == _num_of_threads) return;
                if(_shared_counter.load() == _num_of_threads) return;
                if(_shared_counter.load() == _num_of_threads) return;
                if(_shared_counter.load() == _num_of_threads) return;
                if(_shared_counter.load() == _num_of_threads) return;
                if(_shared_counter.load() == _num_of_threads) return;
            }
        }
    };
}