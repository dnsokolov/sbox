#pragma once

#include <chrono>
#include <sstream>

namespace sbox {
    class StopWatch {
        private:
        #ifdef _WIN32
        std::chrono::steady_clock::time_point _start;
        std::chrono::steady_clock::time_point _stop;
        #else
        std::chrono::system_clock::time_point _start;
        std::chrono::system_clock::time_point _stop;
        #endif

        public:
        void Start() {
            _start = std::chrono::high_resolution_clock::now();
        }

        std::string Stop(const std::string &units, int64_t &res) {
            _stop = std::chrono::high_resolution_clock::now();
            std::stringstream ss;

            if(units == "s") {
                res = std::chrono::duration_cast<std::chrono::seconds>(_stop - _start).count();
                ss << res << "s";
                return ss.str();
            }
            
            if(units == "ms") {
                res = std::chrono::duration_cast<std::chrono::milliseconds>(_stop - _start).count();
                ss << res << "ms";
                return ss.str();
            }

            if(units == "mcs") {
                res = std::chrono::duration_cast<std::chrono::microseconds>(_stop - _start).count();
                ss << res << "mcs";
                return ss.str();
            }

            if(units == "ns") {
                res = std::chrono::duration_cast<std::chrono::nanoseconds>(_stop - _start).count();
                ss << res << "ns";
                return ss.str();
            }

            res = std::chrono::duration_cast<std::chrono::nanoseconds>(_stop - _start).count();
            ss << res << "ns";
            return ss.str();
        }
        
    };
}