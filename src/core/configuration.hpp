#pragma once

#include <vector>
#include <unordered_map>
#include <string>
#include <algorithm>
#include <float.h>

#include "utils.hpp"
#include "formats/xyzformat.hpp"
#include "geometries/igeometry.hpp"
#include "atom.hpp"
#include "ceparamsreader.hpp"

namespace sbox {
    
class Configuration {
    public:
    class IRemoveAtomHandler {
        public:
        virtual void CallRemoveAtomHandler(Configuration &config, Atom *removed_atom) = 0;
    };

    struct Group {
        size_t start{0};
        size_t finish{0};
    };

    class GroupSelector {
        private:
        Configuration *_config = nullptr;

        public:
        GroupSelector(Configuration &config) {
           _config = &config;
        } 

        std::vector<Atom*> Select(size_t group, bool freeze_selected = false) {
            auto groups = _config->GetMergedGroups();
            std::vector<Atom*> sel_atoms;
            if(group < groups.size()) {
                Group gr = groups[group];
                auto _atoms = _config->GetAtoms();
                
                for(size_t i = gr.start; i < gr.finish; ++i) {
                    if(freeze_selected) _atoms[i]->frozen = 1;
                    sel_atoms.emplace_back(_atoms[i]);
                }
            }
            return sel_atoms;
        }
    };

    class SpeciesSelector {
        private:
        std::string _sp;
        bool _freeze_selected;

        public:
        SpeciesSelector(const std::string &sp, bool freeze_selected) : _sp(sp),
                                                 _freeze_selected(freeze_selected) {}

        bool Check(Atom *a) {
            auto result = a->symbol == _sp;
            if(result && _freeze_selected) a->frozen = 1; 
            return result;
        }

        bool Stop() {
            return false;
        }
    };

    private:
    std::vector<std::string> _symbols;
    std::vector<double> _molmasses;
    bool _molmasses_loaded = false;
    bool _recal_molmass = false;
    std::vector<Atom*> _atoms,_state;
    std::vector<IRemoveAtomHandler*> _remove_handlers;
    uint32_t _index_counter = 0;
    double _total_mol_mass = 0;

    double _xmax = -DBL_MAX, _xmin = DBL_MAX, 
           _ymax = -DBL_MAX, _ymin = DBL_MAX, 
           _zmax = -DBL_MAX, _zmin = DBL_MAX,
           _xmax_state = -DBL_MAX, _xmin_state = DBL_MAX, 
           _ymax_state = -DBL_MAX, _ymin_state = DBL_MAX, 
           _zmax_state = -DBL_MAX, _zmin_state = DBL_MAX;

    IGeometry *_geometry = nullptr;
    
    public:
    std::string comment;
    std::unordered_map<std::string, std::string> properties;
    std::unordered_map<std::string, std::vector<std::string>> columns;

    Configuration(const std::string &filename = "", int frame = 0);

    ~Configuration() {
       // std::cout << "Call destructor delete_atoms = " << delete_atoms << std::endl;
        Clear();
    }

    void RegAtomRemoveHandler(IRemoveAtomHandler *handler) {
        _remove_handlers.push_back(handler);
    }
    
    void AddAtom(double x, double y, double z, double energy, const std::string &type, uint32_t frozen) {
        Atom *atom = new Atom;
        atom->position = x, y, z;
        atom->energy = energy;
        atom->symbol = type;
        atom->frozen = frozen;
        atom->index = _index_counter;
        push_back_unique(_symbols, type);
        atom->symbol_index = index_of(_symbols, type);
        _atoms.push_back(atom);
        ++_index_counter;

        if(x > _xmax) _xmax = x;
        if(x < _xmin) _xmin = x;

        if(y > _ymax) _ymax = y;
        if(y < _ymin) _ymin = y;

        if(z > _zmax) _zmax = z;
        if(z < _zmin) _zmin = z;

        _recal_molmass = true;
    }

    void AddAtom(Atom *a) {
        AddAtom(a->position[0], a->position[1], a->position[2], a->energy, a->symbol, a->frozen);
        size_t size = _atoms.size();
        _atoms[size-1]->velocity = a->velocity;
        _atoms[size-1]->neighbour_flag = a->neighbour_flag;
        _atoms[size-1]->old_energy = a->old_energy;
        _atoms[size-1]->old_position = a->old_position;
        _atoms[size-1]->old_velocity = a->old_velocity;
        _atoms[size-1]->symbol_index = a->symbol_index;
        _atoms[size-1]->column_values = a->column_values;
        _recal_molmass = true;
    }

    void RemoveAtom(Atom *atom) {
        auto end = _atoms.end();
        auto it = std::find(_atoms.begin(), end, atom);

        if(it != end) {
            if(_molmasses_loaded) _total_mol_mass -= _molmasses[atom->symbol_index];

            _atoms.erase(it);

            auto end_remove_h = _remove_handlers.end();

            for(auto i = _remove_handlers.begin(); i != end_remove_h; ++i) {
                (*i)->CallRemoveAtomHandler(*this, atom);
            }

            if(_molmasses_loaded) _recal_molmass = true;
        }
    }

    void RemoveAtom(size_t num) {
        RemoveAtom(_atoms[num]);
    }

    void SetMolMasses(const std::vector<double> &molmasses) {
        _molmasses = molmasses;
        auto end = _atoms.end();
        for(auto i = _atoms.begin(); i != end; ++i) {
            _total_mol_mass +=_molmasses[(*i)->symbol_index];
        }
        _recal_molmass = false;
    }

    double GetTotalMolMass() {
 
        if(_recal_molmass) {
            _total_mol_mass = 0;
            auto end = _atoms.end();
            for(auto i = _atoms.begin(); i != end; ++i) {
                _total_mol_mass += _molmasses[(*i)->symbol_index];
            }
            _recal_molmass = false;
        }

        return _total_mol_mass;
    }

    void GetPieceOfAtoms(size_t start, size_t finish, std::vector<Atom*> &piece){
        for(size_t i = start; i < finish; ++i) {
            piece.push_back(_atoms[i]);
        }
    }

    void GetBox(Vec3D &min, Vec3D &max) const {
        min[0] = _xmin;
        min[1] = _ymin;
        min[2] = _zmin;
        max[0] = _xmax;
        max[1] = _ymax;
        max[2] = _zmax;
    }

    void SetCMS() {
        Vec3D r_cms = 0.0;
        double mass = 0.0;
        auto end = _atoms.end();
        auto begin = _atoms.begin();
        
        for(auto i = begin; i != end; ++i) {
            r_cms += ((*i)->position * _molmasses[(*i)->symbol_index]);
            mass += _molmasses[(*i)->symbol_index];
        }

        r_cms /= mass;

        for(auto i = begin; i != end; ++i) {
            (*i)->position -= r_cms;
        }   
    }

    double GetKineticalEnergy() const {
        double sum = 0.0;
        auto end = _atoms.end();

        for(auto i = _atoms.begin(); i != end; ++i) {
            sum += 0.5 * _molmasses[(*i)->symbol_index] * g_per_mol_to_eV_ps2_A2 * blitz::dot((*i)->velocity, (*i)->velocity); 
        }

        return sum;
    }

    size_t FindMinEnergyAtom(Atom **atom, bool exclude_frozen) {
        auto end = _atoms.end();
        double energy = DBL_MAX;
        size_t num = -1, finded;

        for(auto i = _atoms.begin(); i != end; ++i) {
            ++num;
            if(exclude_frozen && (*i)->frozen) continue;
            if((*i)->energy < energy) {
                atom = &(*i);
                energy = (*i)->energy;
                finded = num;
            }
        }

        return finded;
    }

    size_t FindMaxEnergyAtom(Atom **atom, bool exclude_frozen) {
        auto end = _atoms.end();
        double energy = -DBL_MAX;
        size_t num = -1, finded;

        for(auto i = _atoms.begin(); i != end; ++i) {
            ++num;
            if(exclude_frozen && (*i)->frozen) continue;
            if((*i)->energy > energy) {
                atom = &(*i);
                energy = (*i)->energy;
                finded = num;
            }
        }

        return finded;
    }

    bool delete_atoms = true;

    void Clear() {	
        comment = "";
        _index_counter = 0;

        if(delete_atoms) {
           auto end = _atoms.end();
           for(auto i = _atoms.begin(); i != end; ++i) {
              delete *i;
           }
        }

        auto end = _state.end();
        for(auto i = _state.begin(); i != end; ++i) {
            delete *i;
        }

        _atoms.clear();
        _xmax = -DBL_MAX, _xmin = DBL_MAX, 
        _ymax = -DBL_MAX, _ymin = DBL_MAX, 
        _zmax = -DBL_MAX, _zmin = DBL_MAX;

        _total_mol_mass = 0;
    }

    bool ContainsProperty(const std::string property) const {
        return properties.find(property) != properties.end();
    }

    void SetGeometry(IGeometry *geometry) {
        _geometry = geometry;
    }

    void SetProperty(const std::string &name, const std::string &value) {
        properties[name] = value;
    }

    IGeometry *GetGeometry() {
        return _geometry;
    }

    std::vector<std::string> GetSymbols() {
        return _symbols;
    }

    void GetNeighbours(Atom *atom, std::vector<Atom*> &neighbours) {
        auto end = _atoms.end();
        double rcut = _geometry->GetRcut();

        for(auto i = _atoms.begin(); i != end; ++i) {
            if(_geometry->GetDistance(atom, *i) < rcut && atom != *i) {
                (*i)->SetOldEnergy();
                neighbours.push_back(*i);
            }
        }
    }

    void GetNeighbours(Atom *atom, 
                       std::vector<Atom*> &neighbours, 
                       std::vector<Atom*> &neighbours2) {
        auto end = _atoms.end();
        double rcut = _geometry->GetRcut();

        for(auto i = _atoms.begin(); i != end; ++i) {
            if(_geometry->GetDistance(atom, *i) < rcut && atom != *i) {
                (*i)->SetOldEnergy();
                neighbours.push_back(*i);
                neighbours2.push_back(*i);
            }
        }
    }

    size_t GetNeigboursAnotherTypes(Atom *atom, std::vector<Atom*> &neighbours) {
        auto end = _atoms.end();
        double rcut = _geometry->GetRcut();
        size_t num = 0;

        for(auto i = _atoms.begin(); i != end; ++i) {
            if(_geometry->GetDistance(atom, *i) < rcut && atom != *i && atom->symbol_index != (*i)->symbol_index) {
                (*i)->SetOldEnergy();
                neighbours.push_back(*i);
                ++num;
            }
        }

        return num;
    }

    size_t GetNeigboursAnotherTypes(Atom *atom, std::vector<Atom*> &neighbours, double dist) {
        auto end = _atoms.end();
        size_t num = 0;

        for(auto i = _atoms.begin(); i != end; ++i) {
            if(_geometry->GetDistance(atom, *i) < dist && atom != *i && atom->symbol_index != (*i)->symbol_index) {
                neighbours.push_back(*i);
                ++num;
            }
        }

        return num;
    }

    size_t GetNumOfAtoms() const {
        return _atoms.size();
    }

    size_t GetNumOfTypes() const {
        return _symbols.size();
    }
	
	size_t GetNumOfAtoms(const std::string &component) const {
        size_t num = 0;
        auto end = _atoms.end();
        for(auto i = _atoms.begin(); i !=end; ++i) {
            if((*i)->symbol == component) ++num;
        }
        return num;
    }

    Atom *GetAtom(size_t i) {
        return _atoms[i];
    }

    std::vector<Atom*> GetAtoms() {
        return _atoms;
    }

    std::vector<Atom*> GetNoFrozenAtoms() {
        std::vector<Atom*> result;

        for(auto &atom : _atoms) {
            if(!atom->frozen) {
                result.emplace_back(atom);
            }
        }

        return result;
    }

    void GetClone(Configuration &cloneconfig) const {
        auto end = _atoms.end();
        cloneconfig.Clear();

        for(auto i = _atoms.begin(); i != end; ++i) {
            cloneconfig.AddAtom(*i);
        }
    }

    void SaveState() {
        if(_state.empty()) {
            auto end = _atoms.end();
            for(auto i = _atoms.begin(); i != _atoms.end(); ++i) {
                Atom *a = new Atom;
                *a = **i;
                _state.push_back(a);
            }
            return;
        }

        size_t size = _atoms.size();

        if(_state.size() == size) {
            for(size_t i = 0; i < size; ++i) {
                *_state[i] = *_atoms[i];
            }
            return;
        }

        _xmax_state = _xmax;
        _xmin_state = _xmin;
        _ymax_state = _ymax;
        _ymin_state = _ymin;
        _zmax_state = _zmax;
        _zmin_state = _zmin;

        size_t state_size = _state.size();

        if(state_size > size) {
            size_t length = state_size - size;

            for(size_t i = 0; i < length; ++i) {
                Atom *a = _state.back();
                delete a;
                _state.pop_back();
            }

            for(size_t i = 0; i < size; ++i) {
                *_state[i] = *_atoms[i];
            }

            return;
        }

        size_t length = size - state_size;

        for(size_t i = 0; i < length; ++i) {
            Atom *a = new Atom;
            *a = *_atoms[state_size + i];
            _state.push_back(a);
        }
    }

    void BackToState() {
        if(_state.empty()) return;

        size_t size = _atoms.size();

        if(size == _state.size()) {
            for(size_t i = 0; i < size; ++i) {
                *_atoms[i] = *_state[i];
            }
            return;
        }

        _xmax = _xmax_state;
        _xmin = _xmin_state;
        _ymax = _ymax_state;
        _ymin = _ymin_state;
        _zmax = _zmax_state;
        _zmin = _zmin_state;

        size_t state_size = _state.size();

        if(state_size > size) {
            for(size_t i = 0; i < size; ++i) {
                *_atoms[i] = *_state[i];
            }

            size_t length = state_size - size;

            for(size_t i = 0; i < length; ++i) {
               Atom *a = new Atom;
               *a = *_state[size + i];
               _atoms.push_back(a);
            }

            return;
        }

        for(size_t i = 0; i < state_size; ++i) {
            *_atoms[i] = *_state[i];
        }

        size_t length = size - state_size;

        for(size_t i = 0; i < length; ++i) {
                Atom *a = _atoms.back();
                delete a;
                _atoms.pop_back();
        }
    }

    void LoadMolMassesCE(const std::string &filename) {
        CEParamsReader reader(filename);
        auto end = _symbols.end();
        for(auto i = _symbols.begin(); i != end; ++i) {
            _molmasses.push_back(reader.GetMolMass(*i));
        }
        _molmasses_loaded = true;
    }

    void SetMolMass(const std::string &component, double value) {
        auto end = _symbols.end();
        size_t k = 0;
        _molmasses.resize(_symbols.size());

        for(auto i = _symbols.begin(); i != end; ++i) {
            if((*i) == component) {
                _molmasses[k] = value;
                _molmasses_loaded = true;
                return;
            }
            ++k;
        }

    }

    void Merge(const Configuration &conf) {
        if(properties.find("merge_atoms") == properties.end()) {
            properties["merge_atoms"] = to_string(GetNumOfAtoms()) + ",";
        }
        
        size_t n = 0;
        for(const auto &a: conf._atoms) {
            Atom *b = new Atom();
            *b = *a;
            AddAtom(b);
            ++n;
        }

        properties["merge_atoms"] += to_string(n) + ",";
    }

    std::vector<double> GetMolMasses() {
        return _molmasses;
    }

    double GetFullEnergy() const {
        double sum = 0;
        auto end = _atoms.end();
        for(auto i = _atoms.begin(); i != end; ++i) {
            sum += (*i)->energy;
        }
        return sum;
    }

    double GetFullEnergy(double &Ekin, double &Epot) const {
        Ekin = 0; Epot = 0;
        auto end = _atoms.end();
        for(auto i = _atoms.begin(); i != end; ++i) {
            Epot += (*i)->energy;
            Ekin += 0.5 * _molmasses[(*i)->symbol_index] * g_per_mol_to_eV_ps2_A2 * blitz::dot((*i)->velocity, (*i)->velocity);
        }
        return Ekin + Epot;
    }

    double GetTemperature() const {
        if(!ContainsProperty("temperature")) return -1.0;
        return as<double>(properties.at("temperature"));
    }

    void FitTo(size_t num_of_atoms) {
        if(num_of_atoms >= _atoms.size()) return;

        Vec3D rc{0,0,0};

        auto end = _atoms.end();
        for(auto i = _atoms.begin(); i != end; ++i) {
            rc += (*i)->position;
        }

        rc *= 1.0/_atoms.size();
        
        std::vector<std::pair<Atom*,double>> atms_d;
        Vec3D rv;

        for(auto i = _atoms.begin(); i != end; ++i) {
            rv = (*i)->position - rc;
            atms_d.emplace_back(std::pair<Atom*,double>(*i, blitz::dot(rv,rv)));
        }

        std::sort(atms_d.begin(), atms_d.end(), [](std::pair<Atom*,double> p1, std::pair<Atom*,double> p2) {
            return p1.second > p2.second;
        });

        auto endp = atms_d.end();
        for(auto i = atms_d.begin(); i != endp; ++i) {
            if(_atoms.size() == num_of_atoms) break;
            RemoveAtom(i->first);
        }
    }

    std::string GetSystemName() const {
        auto end = _symbols.end();
        std::string result = "";

        for(auto i = _symbols.begin(); i != end; ++i) {
            result += (*i) + as_string(GetNumOfAtoms(*i)) + "@";
        }

        return result.substr(0, result.size() - 1);
    }

    double GetInterAtomicDist(size_t num_of_neighbours) {
        double result = 0.0;
        double aver;
        size_t num_of_atoms = _atoms.size();
        std::vector<double> cur_dists;
        std::vector<double> aver_dists;
        cur_dists.resize(num_of_atoms);
        aver_dists.resize(num_of_atoms);
        auto begin = cur_dists.begin();
        auto end = cur_dists.end();

        num_of_neighbours = num_of_neighbours < num_of_atoms ? num_of_atoms : num_of_neighbours;

        for(size_t i = 0; i < num_of_atoms; ++i) {
            for(size_t j = 0; j < num_of_atoms; ++j) {
                if(i == j) continue;
                cur_dists[i] = _geometry->GetDistance(_atoms[i],_atoms[j]);
            }
            std::sort(begin, end);
            aver = 0.0;
            for(size_t k = 0; k < num_of_neighbours; ++k) {
                aver += cur_dists[k];
            }
            aver_dists[i] = (aver/num_of_neighbours);
        }

        average_data(aver_dists, result);
        return result;
    }

    void PrintInfo() {
        std::cout << "System:                " << GetSystemName() << std::endl;
        std::cout << "Number of atoms:       " << GetNumOfAtoms() << std::endl;
        std::cout << "Number of components:  " << GetNumOfTypes() << std::endl;

        auto end = _symbols.end();
        size_t k = 0;
        for(auto i = _symbols.begin(); i != end; ++i) {
            std::cout << "   ---> " << (*i) << " num: " << GetNumOfAtoms(*i);
            if(_molmasses_loaded) {
                std::cout << ", molmass: " << _molmasses[k];
                ++k;
            }
            std::cout << std::endl;
        } 
    }

    std::vector<Group> GetMergedGroups() const {
        std::vector<Group> result;
        if(properties.find("merge_atoms") != properties.end()) {
            std::vector<std::string> nums;
            split(properties.at("merge_atoms"), nums, ',');

            size_t start = 0;
            size_t num_of_atoms;
            Group range;
            auto end = nums.end();

            for(auto i = nums.begin(); i != end; ++i) {
                num_of_atoms = as<size_t>(*i);
                range.start = start;
                range.finish = start + num_of_atoms;
                start += num_of_atoms;
                result.emplace_back(range);
            }
        }
        return result;
    }

    template <class Selector>
    std::vector<Atom*> SelectAtoms(Selector &sel) {
        std::vector<Atom*> sl_atoms;
        auto end = _atoms.end();

        for(auto i = _atoms.begin(); i != end; ++i) {
            if(sel.Stop()) break;
            if(sel.Check(*i)) {
                sl_atoms.emplace_back(*i);
            }
        }

        return sl_atoms;
    }

    void LoadFromFile(const std::string &filename, int frame = 0);
    void AppendFromFile(const std::string &filename, int frame = 0);
    void SaveToFile(const std::string &filename, bool append = false);
    void RefreshComment(bool appendEnergyColumn = false, bool appendFreezeColumn = false);
};
}
