#pragma once

#include <string>
#include <sstream>

namespace sbox {
    class Number {
        public:
        enum class NumberType {
            UNKNOWN,
            INTEGER,
            DOUBLE
        };

        private:
        double  _dbl_value{0};
        int32_t _int_value{0};
        NumberType _nmb_type{NumberType::UNKNOWN};

        public:
        bool Parse(const std::string &value) {
            size_t len = value.length();
            if(len == 0) return false;

            char c = value[0];
            if(!std::isdigit(c) && c != '-') {
                std::cout << "nondigit symbol '" << c << "'" << std::endl;
                return false;
            }
            

            size_t num_points{0};

            for(size_t i = 1; i < len; ++i) {
                c = value[i];

                if(c == '.' && i == 1 && value[0] == '-') return false;

                if(c == '.') {
                    ++num_points;
                    if(num_points > 1) return false;
                    continue;
                }

                if(!std::isdigit(c)) return false;
            }

            if(len == 1 && !std::isdigit(value[0])) return false;

            std::stringstream inp_ss(value);

            if(num_points == 0) {
                _nmb_type = NumberType::INTEGER;
                inp_ss >> _int_value;
                _dbl_value = _int_value;
                return true;
            } 

            _nmb_type = NumberType::DOUBLE;
            inp_ss >> _dbl_value;
            return true;
        }

        bool IsInteger() const noexcept {
            return _nmb_type == NumberType::INTEGER;
        }

        bool IsDouble() const noexcept {
            return _nmb_type == NumberType::DOUBLE;
        }

        NumberType GetNumberType() const noexcept {
            return _nmb_type;
        }

        double GetDoubleValue() const noexcept {
            return _dbl_value;
        }

        int32_t GetIntegerValue() const noexcept {
            return _int_value;
        }
    };

    class NumVector3D {
        private:
        Number _x, _y, _z;

        public:
        bool Parse(const std::string &value) {
            std::stringstream inp_ss(value);
            std::string nmb;

            inp_ss >> nmb;
            if(inp_ss.eof()) return false;
            if(!_x.Parse(nmb)) return false;

            inp_ss >> nmb;
            if(inp_ss.eof()) return false;
            if(!_y.Parse(nmb)) return false;

            inp_ss >> nmb;
            if(!_z.Parse(nmb)) return false;
            if(!inp_ss.eof()) return false;

            return true;
        }

        void GetComponents(Number &x, Number &y, Number &z) const noexcept {
            x = _x; 
            y = _y; 
            z = _z;
        }

        void GetComponents(double &x, double &y, double &z) const noexcept {
            x = _x.GetDoubleValue();
            y = _y.GetDoubleValue();
            z = _z.GetDoubleValue();
        }

        void GetComponents(int32_t &x, int32_t &y, int32_t &z) const noexcept {
            x = _x.GetIntegerValue();
            y = _y.GetIntegerValue();
            z = _z.GetIntegerValue();
        }

        bool IsDoubleVector() const noexcept {
            return _x.IsDouble() && _y.IsDouble() && _z.IsDouble();
        }

        bool IsIntegerVector() const noexcept {
            return _x.IsInteger() && _y.IsInteger() && _z.IsInteger();
        }
    };
}