#include "configuration.hpp"

#include <cstring>
#include <cstdlib>
#include <sstream>

namespace sbox {

Configuration::Configuration(const std::string &filename, int frame) {
        std::ifstream fs(filename);
        if(fs.is_open()) {
            XYZFormat xyz(frame);
            xyz.Read(fs, *this);
        }
        fs.close();
}

void Configuration::RefreshComment(bool appendEnergyColumn, bool appendFreezeColumn) {
        std::string str = "[";
        auto end = properties.end();
        auto end_clm = columns.end();

        for(auto i = properties.begin(); i != end; ++i) {
            str += i->first + ":" + i->second + " ";
        }

        str += "columns:";

        if(appendEnergyColumn) str += "Energy,";
        if(appendFreezeColumn) str += "Freeze,";

        for(auto i = columns.begin(); i != end_clm; ++i) {
            str += i->first +",";
        }

        str[str.length()-1] = ']';
        comment = str;

        /*size_t start = comment.find('[');
        size_t finish = comment.find(']');

         if(start != std::string::npos && finish != std::string::npos) {
            size_t count = finish - start + 1;
            std::cout.flush();
            comment.replace(start, count, str);
        } 
        else {
           comment += " " + str;  
        }*/
}

void Configuration::SaveToFile(const std::string &filename, bool append) {
        std::ofstream fs;
        if(append) fs.open(filename, std::ios_base::out | std::ios_base::app);
        else fs.open(filename, std::ios_base::out | std::ios_base::trunc);
        if(fs.is_open()) {
            std::string content;
            XYZFormat xyz;
            xyz.Write(*this, fs);
        }
        fs.close();
}

void Configuration::LoadFromFile(const std::string &filename, int frame) {
        Clear();
        std::ifstream content(filename);
        
        if(content.is_open()) {
           XYZFormat xyz(frame);
           xyz.Read(content, *this);
        }
}

void Configuration::AppendFromFile(const std::string &filename, int frame) {
    size_t num_of_atoms = GetNumOfAtoms();

    std::ifstream content(filename);
    if(properties.find("merge_atoms") == properties.end()) {
        properties["merge_atoms"] = to_string(num_of_atoms) + ",";
    }

    if(content.is_open()) {
        XYZFormat xyz(frame);
        xyz.Read(content, *this);
        properties["merge_atoms"] += to_string(GetNumOfAtoms() - num_of_atoms) + ",";
    }
}

}