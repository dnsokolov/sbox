#pragma once
#include <stddef.h>
#include <initializer_list>
#include <vector>
#include <string>
#include <stdexcept>
#include <fstream>

namespace sbox {
    class FuncTable {
        public:
        class Item {
            double *_values;
            size_t _size;

            public:
            Item(std::initializer_list<double> values) {
                _size = values.size();
                _values = new double[_size];
                auto end = values.end();
                size_t k = 0;

                for(auto i = values.begin(); i != end; ++i) {
                    _values[k] = *i;
                    ++k;
                }
            } 

            double& operator [] (size_t i) {
                if(i >= _size) {
                    throw std::invalid_argument("In 'Item::operator[] (size_t i)' i >= _size");
                }

                return _values[i];
            }

            ~Item() {
                delete [] _values;
            }
        };

        private:
        std::vector<Item*> _items;
        std::vector<std::string> _columns_name;
        size_t _num_of_columns = 0;

        public:
        FuncTable(std::initializer_list<const char*> columns_name) {
            _num_of_columns = columns_name.size();
            auto end = columns_name.end();

            for(auto i = columns_name.begin(); i != end; ++i) {
                _columns_name.push_back(*i);
            }
        }

        ~FuncTable() {
            Clear();
        }

        void Clear() {
            auto end = _items.end();
            for(auto i = _items.begin(); i != end; ++i) {
                delete (*i);
            }
            _items.clear();
            _num_of_columns = 0;
        }

        void InsertValues(std::initializer_list<double> values) {
            if(values.size() != _num_of_columns) {
                throw std::invalid_argument("In 'FuncTable::InsertValues(std::initializer_list<double> values)' values.size() != _num_of_columns");
            }

            Item *item = new Item(values);
            _items.push_back(item);
        }

        void SaveToFile(const std::string &filename) {

            if(std::filesystem::exists(filename)) {
                std::filesystem::remove(filename);
            }

            std::ofstream ostream(filename);
            auto end = _columns_name.end();

            for(auto i = _columns_name.begin(); i != end; ++i) {
                ostream << (*i) << " ";
            }

            ostream << std::endl;
            auto end_items = _items.end();
            size_t j = 0;

            for(auto i = _items.begin(); i != end_items; ++i) {
                for(;j<_num_of_columns; ++j) {
                    ostream << (*(*i))[j] << " ";
                }
                ostream << std::endl;
                j = 0;
            }

            ostream.close();
        }
    };
}