#ifndef _PROPERTYREADER_H_
#define _PROPERTYREADER_H_

#include "utils.hpp"
#include <exception>

namespace sbox {
class PropertyReader {
    private:
    toml::table data;
    bool error{false};

    public:
    PropertyReader(const std::string &propertyfile) {
        try {
           data = toml::parse(propertyfile);
        }
        catch(const std::exception &exc) {
            std::cerr << "PropertyReader exception:\n" << exc.what() << std::endl;
            error = true;
        }
    } 

    bool IsError() const {
        return error;
    }

    void GetPathes(std::vector<std::string> &pathes) {
        if(!data.count("pathes")) return;
        auto _pathes = toml::get<std::vector<std::string>>(data.at("pathes"));
        for(auto &p : _pathes) {
            pathes.push_back(p);
        }
    }

    std::string GetPotentialParams(
        const std::string &potname, 
        const std::string &comp1, 
        const std::string &comp2
    );

    template <typename T>
    void GetAtomProperty(
        const std::string &chemicalname, 
        const std::string &propertyname, 
        T &value
    ) {
        if(data.count(chemicalname) != 0) {
           auto table = toml::get<toml::Table>(data.at(chemicalname));
           if(table.count(propertyname) != 0)
              value = toml::get<T>(table.at(propertyname));
        }
    }
};
}

#endif
