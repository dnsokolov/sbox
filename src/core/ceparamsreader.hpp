#ifndef _CEPARAMSREADER_HPP_
#define _CEPARAMSREADER_HPP_

#include <string>
#include <sstream>
#include <fstream>
#include <vector>
#include <map>

#include "utils.hpp"

namespace sbox {

class CEParamsReader
{
	std::string _cefile;
	std::vector<std::string> components;
	std::map<std::string, double> ro;
	std::map<std::string, double> molmas;
public:
	CEParamsReader(const std::string &cefile);
	
	std::string ReadTBPotential( 
	            const std::string &comp1,
				const std::string &comp2) const;

	void GetComponents(std::vector<std::string> &comps) const;
	double GetMolMass(const std::string &comp) const;

};

}

#endif // _CEPARAMSREADER_HPP_
