#pragma once

#include <iostream>
#include <string>
#include <vector>

#include "utils.hpp"
#include "propertyreader.hpp"

#ifndef _WIN32
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#else
#include <Windows.h>
#endif

namespace sbox {
    class LightProcess {
        public:
        class Options {
            private:
            char **_argv = nullptr;
            public:
            std::string path;
            std::vector<std::string> args;
            bool wait_for_exit;
            bool create_new_console;
            std::string redirect_stdin{""};
            std::string redirect_stdout{""};

            Options() {
                path = "";
                wait_for_exit = false;
                create_new_console = true;
            }

            void AppendCmdArgument(const std::string &arg) {
                args.push_back(arg);
            }

            std::string GetCmdLine() {
                std::string result = "";
                
                if(path.find(' ') != std::string::npos) {
                    result += ("\"" + path + "\" ");
                }
                else {
                    result += (path + " ");
                }

                auto end = args.end();
                for(auto i = args.begin(); i != end; ++i) {
                    if(i->find(' ') != std::string::npos) {
                        result += ("\"" + (*i) + "\" ");
                    }
                    else {
                        result += ((*i) + " ");
                    }
                }

                return result.substr(0, result.length()-1);
            }

            void ParseCmdLine(const std::string &cmdline) {
                size_t length = cmdline.length();
                bool flag = false;
                bool redirect_stdin_flag = false;
                bool redirect_stdout_flag = false;
                std::string chunck = "";
                char c;

                for(size_t i = 0; i < length; ++i) {
                    c = cmdline[i];

                    if(c=='<') {
                        redirect_stdin_flag = true;
                        continue;
                    }

                    if(c=='>') {
                        redirect_stdout_flag = true;
                        continue;
                    }

                    if(c == '\"' || c == '\'') {
                        flag = !flag;
                        continue;
                    }

                    if(!flag && c == ' ') {
                        if(chunck == "") continue;

                        if(redirect_stdin_flag) {
                            redirect_stdin = chunck;
                            redirect_stdin_flag = false;
                        }
                        else if(redirect_stdout_flag) {
                            redirect_stdout = chunck;
                            redirect_stdout_flag = false;
                        }
                        else{
                            args.push_back(chunck);
                        }
                        
                        chunck = "";
                        continue;
                    }

                    chunck += c;
                }

                if(args.size() == 0) path = chunck;
                else {
                    if(redirect_stdin_flag) {
                        redirect_stdin = chunck;
                    }
                    else if(redirect_stdout_flag) {
                        redirect_stdout = chunck;
                    }
                    else {
                        args.push_back(chunck);
                    }
                    
                    path = args[0];
                    args.erase(args.begin());
                }

            }

            char **CreateArgv() {
                if(_argv != nullptr) return _argv;

                size_t argc = args.size() + 2;
                size_t k = 1;

                _argv = new char*[argc];

                auto end = args.end();
                _argv[0] = new char[path.length() + 1];
                strcpy(_argv[0], path.c_str());
                
                for(auto i = args.begin(); i != end; ++i) {
                    _argv[k] = new char[i->length() + 1];
                    strcpy(_argv[k], i->c_str());
                    ++k;
                }

                _argv[argc-1] = new char[1];
                _argv[argc-1] = NULL;

                return _argv;
            }

            void FreeArgv() {
                size_t argc = args.size() + 1;

                for(size_t i = 0; i < argc; ++i) {
                    delete [] _argv[i];
                }

                delete [] _argv;
                _argv = nullptr;
            }
        };

        Options options;

        std::string FindProgram(const std::string &filename, bool &find_flag) {
           if(std::filesystem::exists(filename)) {
              find_flag = true;
              return filename;
           }

           std::string file = filename;

           if(const char *env_p = std::getenv("PATH")) {
           std::string env_p_str = env_p;
           std::vector<std::string> pathes;
           #ifndef _WIN32
           split(env_p_str, pathes,':');
           #else
           if(!has_suffix(filename,".exe")) {
               file += ".exe";
           }
           split(env_p_str, pathes,';');
           #endif

           std::string apps_path = get_executable_directory() + "/apps";

           if(std::filesystem::exists(apps_path))
           for (const std::filesystem::path &entry : std::filesystem::directory_iterator(apps_path)) {
               if(std::filesystem::is_directory(entry)) {
                  pathes.push_back(entry.string());
               }
           }

           std::string config_file = get_executable_directory() + "/settings.toml";
           if(std::filesystem::exists(config_file)) {
               PropertyReader preader(config_file);
               preader.GetPathes(pathes);
           }

           config_file = get_executable_directory() + "/config.toml";
           if(std::filesystem::exists(config_file)) {
               PropertyReader preader(config_file);
               preader.GetPathes(pathes);
           }

           auto end = pathes.end();

           std::string home_dir = "";
           std::string cur_dir;
           #ifndef _WIN32
           home_dir = std::getenv("HOME");
           #endif

           for(auto i = pathes.begin(); i != end; ++i) {
               cur_dir = (*i);
               #ifndef _WIN32
               if(cur_dir[0] == '~') {
                   cur_dir = cur_dir.replace(0, 1, home_dir);
               }
               #endif
               if(std::filesystem::exists(cur_dir + "/" + file)) {
                  find_flag = true;
                  return (cur_dir + "/" + file);
                }
            }
            }

            find_flag = false;
            return filename;
        }

        void Execute() {
            if(options.path == "") {
                std::cerr << "You must to define path to program in 'options.path'" << std::endl;
                return;
            }

            #ifndef _WIN32
            bool find_flag = false;
            options.path = FindProgram(options.path, find_flag);

           // std::cout << "Launching '" << options.path << "'" << std::endl;

            if(!find_flag) {
                std::cerr << "Couldn't find program '" << options.path << "'" << std::endl;
                return;
            }

            char **argv = options.CreateArgv();
            pid_t pid = fork();

            if(pid == 0) {
                if(options.redirect_stdin != "") {
                    //std::cout << "stdin redirected to " << options.redirect_stdin << std::endl;
                    auto fd = open(options.redirect_stdin.c_str(), O_RDONLY);
                    if(fd >= 0) {
                        dup2(fd, STDIN_FILENO);
                        close(fd);
                    }
                }
                if(options.redirect_stdout != "") {
                    auto fd = open(options.redirect_stdout.c_str(), O_WRONLY);
                    if(fd >= 0) {
                        dup2(fd,STDOUT_FILENO);
                        close(fd);
                    }
                }
                signal(SIGHUP, SIG_IGN);
                execv(argv[0], argv);
                std::cerr << "Child process EXIT_FAILURE" << std::endl;
                _exit(EXIT_FAILURE);
            }
            else if(pid == -1) {
                std::cerr << "Couldn't create child process" << std::endl;
            }
            else {
                options.FreeArgv();

                if(options.wait_for_exit) {
                   int status;
                   waitpid(pid, &status, 0);
                }
            }
            #else

            STARTUPINFO startUpInfo = { 0 };
            PROCESS_INFORMATION procInfo = { 0 };
            startUpInfo.cb = sizeof(startUpInfo);
           // std::cout << options.GetCmdLine() << std::endl;
            std::string cmdline_str = options.GetCmdLine();
            LPSTR cmdline = const_cast<char *>(cmdline_str.c_str());
            DWORD create_console = options.create_new_console ? CREATE_NEW_CONSOLE : 0;

            if (!CreateProcess(NULL, cmdline, NULL, NULL, FALSE, CREATE_NEW_CONSOLE, NULL, NULL, &startUpInfo, &procInfo)) {
               std::cerr << "Couldn't create process '" << cmdline << "'" << std::endl;
               return;
            }

            if(options.wait_for_exit) {
                WaitForSingleObject(procInfo.hProcess, INFINITE );
            }

            CloseHandle(procInfo.hProcess);
            CloseHandle(procInfo.hThread);

            #endif
        }
    };
}