#pragma once

extern "C" 
{
    #include "Lua/lua.h"
    #include "Lua/lauxlib.h"
    #include "Lua/lualib.h"
}

#include "LuaBridge/LuaBridge.h"

#include "configuration.hpp"
#include "functable.hpp"
#include "stopwatch.hpp"

#include <variant>

namespace sbox {

    void atomsk_init(luabridge::LuaRef options);
    void atomsk_do(luabridge::LuaRef com);
    bool atomsk_dofile(const std::string &fname);
    void atomsk_def(luabridge::LuaRef def);
    bool atomsk_get_error_state();
    void atomsk_get_value_vr(const std::string &regname, luabridge::LuaRef vec);
    void atomsk_set_value_vr(const std::string &regname, luabridge::LuaRef vec);
    void atomsk_clear_def();
    void atomsk_quit();
    double to_number(const std::string& str);
    std::string to_string(luabridge::LuaRef val);
    void load_config(const std::string &filename, bool continuation);
    size_t get_num_of_atoms();
    std::string get_system_name();
    std::string get_system_name_from(const std::string &fname);
    std::string find_file(const std::string &extension); 
    void find_files(const std::string &extension, luabridge::LuaRef files);
    void get_caloric_dat(const std::string &xyzfile, const std::string &caloricfile);
    double get_config_energy();
    double get_config_energy_per_atom();
    double get_potential_energy();
    double get_potential_energy_per_atom();
    double get_inter_atomic_dist(luabridge::LuaRef num_of_neighbours);
    size_t get_num_of_atoms_component(const std::string &cmp);
    void get_config_components(luabridge::LuaRef components);
    void get_potential_params(const std::string &comp1, const std::string &comp2, luabridge::LuaRef params);
    void set_potential_params(const std::string &comp1, const std::string &comp2, luabridge::LuaRef params);
    void set_potential_line(const std::string &line);
    void set_molar_mass(const std::string &line, double value);
    bool get_md_flag(const std::string &sampler_name);
    void remove_atom(size_t num);
    void remove_max_energy_atoms(int count);
    void print_atom_info(size_t num);
    void print_config_info();
    void print_geometry_info();
    void print_randomizer_info();
    void print_potential_info();
    void print_sampler_info();
    void print_info();
    std::string fmt(luabridge::LuaRef str, luabridge::LuaRef separator);
    void directory_remove(const std::string &dirname);
    void directory_create(const std::string &dirname);
    void directory_change(const std::string &dirname);
    bool directory_exists(const std::string &dirname);
    void directory_lock();
    void directory_unlock();
    void directories_create(luabridge::LuaRef dirs);
    void file_copy(const std::string &source, const std::string &destination);
    bool file_exists(const std::string &fname);
    void files_copy(luabridge::LuaRef files, const std::string &dirname);
    void file_remove(const std::string &filename);
    void file_remove_if_exists(const std::string &filename);
    void file_rename(const std::string &from_fn, const std::string &to_fn);
    void list_directories(luabridge::LuaRef dirs, const std::string &dirname);
    void list_files(luabridge::LuaRef files, const std::string &dirname);
    std::string get_working_directory();
    std::string get_file_extension(const std::string &fname);
    std::string get_file_name(const std::string &fname);
    std::string get_file_name_without_extension(const std::string &fname);
    std::string get_directory_name(const std::string &dir);
    std::string get_parent_directory(const std::string &path);
    void process_start(luabridge::LuaRef options);
    void sbox_execute(luabridge::LuaRef options);
    void adj_execute(const std::string &args);
    size_t find_max_energy_atom(bool exclude_frozen);
    size_t find_min_energy_atom(bool exclude_frozen);
    void config_dump(const std::string &filename, bool append);
    void config_update(double temperature, luabridge::LuaRef n);
    void get_config_box(luabridge::LuaRef box, luabridge::LuaRef fname);
    void set_configs_to_cms(const std::string &filename);
    void simple_geometry_init(double rcut);
    void box_geometry_init(double rcut, luabridge::LuaRef table);
    void pbc_geometry_init(double rcut, luabridge::LuaRef table);
    void gupt_load(const std::string &filename, luabridge::LuaRef num_of_threads);
    void gupt_init(luabridge::LuaRef num_of_threads);
    void mol_masses_load(const std::string &filename);
    void simple_randomizer_init(double drmax, uint32_t seed);
    void maxwell_randomizer_init(double time, uint32_t seed);
    void integrator_start();
    void verlet_integrator_init(double dt);
    void verlet_velocity_integrator_init(double dt);
    void nose_hoover_integrator_init(double dt, double Q, double temperature);
    void leapfrog_integrator_init(double dt);
    void MH_sampler_init(luabridge::LuaRef table);
    void MD_sampler_init(luabridge::LuaRef table);
    void EXCH_sampler_init(luabridge::LuaRef table);
    void HMC_sampler_init(luabridge::LuaRef table);
    void sampler_init(const std::string &sampler_name, luabridge::LuaRef params);
    void set_sampler_params(luabridge::LuaRef table);
    void random_velocities_init(double temperature);
    void run_temperature(double temperature);
    void run(double start_temperature, double finish_temperature, double delta_temperature); 
    void select_configs(const std::string &filename, luabridge::LuaRef select_fn, luabridge::LuaRef configs);
    //void test_fn(std::variant<luabridge::LuaRef, int> params);
    //void umap_str_str_to_table(std::unordered_map<std::string, std::string> &data, luabridge::LuaRef table);
    void print_logo();
    void free_all();
    void free_global_randomizer();
    void free_global_config();
    void free_global_geometry();
    void free_global_potential();
    void free_global_sampler();
    void free_global_integrator();
    void free_global_atomsk();
    //void sleep(uint32_t milliseconds);
    void stopwatch_start();
    void stopwatch_stop(luabridge::LuaRef units);
    void benchmarking_run(luabridge::LuaRef params);
    void test(luabridge::LuaRef prm);

    class LuaEngine {
        private:
        lua_State *_lstate = nullptr;

        void Reg_Configuration_Class() {
            luabridge::getGlobalNamespace(_lstate)
               .beginClass<Configuration>("Configuration")
                  .addConstructor<void(*) (const std::string&, int)>()
                  .addFunction("GetNumOfTypes", &Configuration::GetNumOfTypes)
                  .addFunction("GetNumOfAtoms", (size_t (Configuration::*)() const) &Configuration::GetNumOfAtoms)
                  .addFunction("GetNumOfAtomsOfType", (size_t (Configuration::*)(const std::string &component) const) &Configuration::GetNumOfAtoms)
                  .addFunction("PrintInfo", &Configuration::PrintInfo)
                  .addFunction("Clear", &Configuration::Clear)
                  .addFunction("GetFullEnergy", (double (Configuration::*)() const) &Configuration::GetFullEnergy)
                  .addFunction("GetTemperature", &Configuration::GetTemperature)
                  .addFunction("SaveToFile", &Configuration::SaveToFile)
                  .addFunction("LoadFromFile", &Configuration::LoadFromFile)
                  .addFunction("RefreshComment", &Configuration::RefreshComment)
                  .addFunction("GetSystemName", &Configuration::GetSystemName)
                  .addFunction("SetProperty", &Configuration::SetProperty)
                  //.addFunction("GetFullEnergyEkinAndEpot", (double (Configuration::*)(double&, double&) const) &Configuration::GetFullEnergy)
               .endClass();
        }

        void Reg_UMapStrStr_Class() {
            luabridge::getGlobalNamespace(_lstate)
               .beginClass<std::unordered_map<std::string, std::string>>("UMapStrStr")
                   .addFunction("size", &std::unordered_map<std::string, std::string>::size)
                   .addFunction("at", (const std::string& (std::unordered_map<std::string, std::string>::*)( const std::string&) const) &std::unordered_map<std::string, std::string>::at)
               .endClass();
        }

        void Reg_Variant_Class() {
           // luabridge::getGlobalNamespace(_lstate)
               
        }

        public:
        LuaEngine() {
            _lstate = luaL_newstate();
            luaL_openlibs(_lstate);
            
            auto _namespace = luabridge::getGlobalNamespace(_lstate);
            _namespace.addFunction("atomsk_init", atomsk_init);
            _namespace.addFunction("atomsk_do", atomsk_do);
            _namespace.addFunction("atomsk_dofile", atomsk_dofile);
            _namespace.addFunction("atomsk_def", atomsk_def);
            _namespace.addFunction("atomsk_clear_def", atomsk_clear_def);
            _namespace.addFunction("atomsk_get_value_vr", atomsk_get_value_vr);
            _namespace.addFunction("atomsk_set_value_vr", atomsk_set_value_vr);
            _namespace.addFunction("atomsk_get_error_state", atomsk_get_error_state);
            _namespace.addFunction("atomsk_quit", atomsk_quit);
            _namespace.addFunction("to_number", to_number);
            _namespace.addFunction("to_string", to_string);
            _namespace.addFunction("config_load", load_config);
            _namespace.addFunction("config_update", config_update);
            _namespace.addFunction("get_config_box", get_config_box);
            _namespace.addFunction("get_num_of_atoms", get_num_of_atoms);
            _namespace.addFunction("get_config_energy", get_config_energy);
            _namespace.addFunction("get_config_energy_per_atom", get_config_energy_per_atom);
            _namespace.addFunction("get_potential_energy", get_potential_energy);
            _namespace.addFunction("get_potential_energy_per_atom", get_potential_energy_per_atom);
            _namespace.addFunction("get_caloric_dat", get_caloric_dat);
            _namespace.addFunction("get_potential_params", get_potential_params);
            _namespace.addFunction("set_potential_params", set_potential_params);
            _namespace.addFunction("set_potential_line", set_potential_line);
            _namespace.addFunction("set_molar_mass", set_molar_mass);
            _namespace.addFunction("get_num_of_components", get_num_of_atoms_component);
            _namespace.addFunction("get_config_components", get_config_components);
            _namespace.addFunction("get_inter_atomic_dist", get_inter_atomic_dist);
            _namespace.addFunction("find_file", find_file);
            _namespace.addFunction("find_files", find_files);
            _namespace.addFunction("print_atom_info", print_atom_info);
            _namespace.addFunction("print_info", print_info);
            _namespace.addFunction("print_config_info", print_config_info);
            _namespace.addFunction("print_geometry_info", print_geometry_info);
            _namespace.addFunction("print_randomizer_info", print_randomizer_info);
            _namespace.addFunction("print_potential_info", print_potential_info);
            _namespace.addFunction("print_sampler_info", print_sampler_info);
            _namespace.addFunction("directory_remove", directory_remove);
            _namespace.addFunction("directory_create", directory_create);
            _namespace.addFunction("directory_change", directory_change);
            _namespace.addFunction("directory_exists", directory_exists);
            _namespace.addFunction("directory_lock", directory_lock);
            _namespace.addFunction("directory_unlock", directory_unlock);
            _namespace.addFunction("directories_create", directories_create);
            _namespace.addFunction("list_directories", list_directories);
            _namespace.addFunction("list_files", list_files);
            _namespace.addFunction("get_working_directory", get_working_directory);
            _namespace.addFunction("get_file_name", get_file_name);
            _namespace.addFunction("get_file_name_without_extension", get_file_name_without_extension);
            _namespace.addFunction("get_file_extension", get_file_extension);
            _namespace.addFunction("get_directory_name", get_directory_name);
            _namespace.addFunction("get_parent_directory", get_parent_directory);
            _namespace.addFunction("get_sbox_directory", get_executable_directory);
            _namespace.addFunction("get_md_flag", get_md_flag);
            _namespace.addFunction("file_copy", file_copy);
            _namespace.addFunction("file_exists", file_exists);
            _namespace.addFunction("files_copy", files_copy);
            _namespace.addFunction("file_remove", file_remove);
            _namespace.addFunction("file_remove_if_exists", file_remove_if_exists);
            _namespace.addFunction("file_rename", file_rename);
            _namespace.addFunction("process_start", process_start);
            _namespace.addFunction("sbox_execute", sbox_execute);
            _namespace.addFunction("adj_execute", adj_execute);
            _namespace.addFunction("remove_atom", remove_atom);
            _namespace.addFunction("remove_max_energy_atoms", remove_max_energy_atoms);
            _namespace.addFunction("config_dump", config_dump);
            _namespace.addFunction("fmt", fmt);
            _namespace.addFunction("set_configs_to_cms", set_configs_to_cms);
            _namespace.addFunction("set_sampler_params", set_sampler_params);
            _namespace.addFunction("find_min_energy_atom", find_min_energy_atom);
            _namespace.addFunction("find_max_energy_atom", find_max_energy_atom);
            _namespace.addFunction("simple_geometry_init", simple_geometry_init);
            _namespace.addFunction("box_geometry_init", box_geometry_init);
            _namespace.addFunction("pbc_geometry_init", pbc_geometry_init);
            _namespace.addFunction("gupt_load", gupt_load);
            _namespace.addFunction("gupt_init", gupt_init);
            _namespace.addFunction("gupt_potential_load", gupt_load);
            _namespace.addFunction("gupt_potential_init", gupt_init);
            _namespace.addFunction("mol_masses_load", mol_masses_load);
            _namespace.addFunction("simple_randomizer_init", simple_randomizer_init);
            _namespace.addFunction("maxwell_randomizer_init", maxwell_randomizer_init);
            _namespace.addFunction("random_velocities_init", random_velocities_init);
            _namespace.addFunction("integrator_start", integrator_start);
            _namespace.addFunction("verlet_integrator_init", verlet_integrator_init);
            _namespace.addFunction("verlet_velocity_integrator_init", verlet_velocity_integrator_init);
            _namespace.addFunction("nose_hoover_integrator_init", nose_hoover_integrator_init);
            _namespace.addFunction("leapfrog_integrator_init", leapfrog_integrator_init);
            _namespace.addFunction("MH_sampler_init", MH_sampler_init);
            _namespace.addFunction("MD_sampler_init", MD_sampler_init);
            _namespace.addFunction("EXCH_sampler_init", EXCH_sampler_init);
            _namespace.addFunction("HMC_sampler_init", HMC_sampler_init);
            _namespace.addFunction("sampler_init", sampler_init);
            _namespace.addFunction("run_temperature", run_temperature);
            _namespace.addFunction("run", run);
            _namespace.addFunction("get_system_name", get_system_name);
            _namespace.addFunction("get_system_name_from", get_system_name_from);
            _namespace.addFunction("print_logo", print_logo);
            _namespace.addFunction("free_all", free_all);
            _namespace.addFunction("free_randomizer", free_global_randomizer);
            _namespace.addFunction("free_config", free_global_config);
            _namespace.addFunction("free_geometry", free_global_geometry);
            _namespace.addFunction("free_potential", free_global_potential);
            _namespace.addFunction("free_sampler", free_global_sampler);
            _namespace.addFunction("free_integrator", free_global_integrator);
            _namespace.addFunction("free_atomsk", free_global_atomsk);
            _namespace.addFunction("select_configs", select_configs);
            _namespace.addFunction("sleep", sleep);
            _namespace.addFunction("stopwatch_start", stopwatch_start);
            _namespace.addFunction("stopwatch_stop", stopwatch_stop);
            _namespace.addFunction("benchmarking_run", benchmarking_run);
            _namespace.addFunction("test", test);
           // _namespace.addFunction("test_fn", test_fn);
           // _namespace.addFunction("umap_str_str_to_table", umap_str_str_to_table);

            //Reg_UMapStrStr_Class();
            Reg_Configuration_Class();
            
            std::string executable_directory = get_executable_directory();

            luaL_dostring(_lstate, "ppath = package.path");
            auto v = luabridge::getGlobal(_lstate, "ppath");
            std::string ppath = v.cast<std::string>();
            std::stringstream execstream;
            std::string path1 = executable_directory + "/lua/?.lua";
            std::string path2 = executable_directory + "/lua/?.lx";

            execstream << "package.path = '" << ppath << "'..';" << path1 << "'..';" << path2 << "'";
           // std::cout << execstream.str() << std::endl;
            luaL_dostring(_lstate, execstream.str().c_str());
          //  luaL_dostring(_lstate, "_expr_result = 3");
        }

        int Execute(const std::string &filename) {
            int code = luaL_dofile(_lstate, filename.c_str());
            if(code) {
                printf("-- %s\n", lua_tostring(_lstate, -1));
                lua_pop(_lstate, 1);
                sleep(5000);
            }
            return code;
        }

        int ExecuteString(const std::string &string) {
            bool print_result_flag{false};
            std::string exec_str;

            if(string.rfind("=")==0) {
                exec_str = "_exec_result" + string;
                print_result_flag = true;
            }
            else {
                exec_str = string;
            }

            int code = luaL_dostring(_lstate, exec_str.c_str());

            if(code) {
                printf("-- %s\n", lua_tostring(_lstate, -1));
                lua_pop(_lstate, 1);
                print_result_flag = false;
            }

            if(print_result_flag) {
                luaL_dostring(_lstate,"print(_exec_result)");
            }

            return code;
        }
        
    };
}

