#pragma once

namespace sbox {
    template <typename T>
    struct Range {
        size_t id;
        T start;
        T finish;
    };

    void change_uint_distr_ranges(Range<size_t> *ranges, size_t num, size_t num_of_ranges) {
        size_t remainder = num % num_of_ranges;
        size_t part = num / num_of_ranges;

        size_t start = 0;
        size_t finish = 0;
        size_t offset = 0;

        for(size_t i = 0; i < num_of_ranges; ++i) {
            start = finish;

            if(remainder > 0) {
                --remainder;
                offset = 1;
            }
            else {
                offset = 0;
            }

            finish = start + part + offset;

            ranges[i].id = i;
            ranges[i].start = start;
            ranges[i].finish = finish;
        }
    }

    Range<size_t> *generate_uint_distr_ranges(size_t num, size_t num_of_ranges) {
        Range<size_t> *ranges = new Range<size_t>[num_of_ranges];
        change_uint_distr_ranges(ranges, num, num_of_ranges);
        return ranges;
    }
}