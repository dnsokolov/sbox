#pragma once

#include <blitz/array.h>
#include <blitz/tinyvec2.h>
#include <string>
#include <vector>

using namespace blitz;
typedef TinyVector<double, 3> Vec3D;

namespace sbox {

    struct CrystalLattice {
        Vec3D a;
        Vec3D b;
        Vec3D c;
    };

    struct Atom {
        bool         neighbour_flag = false;
        uint32_t     index;
        uint32_t     frozen;
        uint32_t     symbol_index;
        double       energy;
        double       old_energy;
        Vec3D        position;
        Vec3D        old_position; 
        Vec3D        velocity;
        Vec3D        old_velocity;
        Vec3D        force;
        std::string  symbol;
        std::vector<std::string> column_values;

        void SetOldConfig() {
            old_position = position;
            old_energy = energy;
        }

        void SetOldEnergy() {
            old_energy = energy;
        }

        void BackToOldConfig() {
            position = old_position;
            energy = old_energy;
        }

        void BackToOldEnergy() {
            energy = old_energy;
        }

        void Translate(const Vec3D &tr) {
            position += tr;
        }
    };

    inline double sqr_vec(const Vec3D& v) {
        return blitz::dot(v,v);
    }
    
}