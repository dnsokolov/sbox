#ifndef _UTILS_HPP_
#define _UTILS_HPP_

#include <algorithm>
#include <vector>
#include <fstream>
#include <string>
#include <cmath>
#include <filesystem>
#include <map>
#include <utility>
#include <thread>
#include "toml/toml.hpp"

#ifndef _WIN32
#include <unistd.h>
#define MAX_PATH 4096
#else
#include <Windows.h>
#endif

#define PROGRAM_NAME "Metropolis"
#define PROGRAM_VERSION "2.0b"

namespace sbox {

#define SBOX_AUTHORS "Denis Sokolov, Andrey Kolosov, Nikolay Sdobnyakov"
#define SBOX_VERSION "0.14"
#define SBOX_COPYRIGHT "Tver State University (C) 2019-2022"

const size_t n_pos = (size_t)(-1);
const double pi = 3.14159265358979323846;
const double two_pi = 6.283185307179586;
const double k_boltzmann = 8.6173303E-5; //eV/K
const double k_boltzmann_3_2 = k_boltzmann * 1.5;
const double r_k_boltzmann_2_3 = 2.0/(3.0 * k_boltzmann);
const double sqrt_R = 0.9118367517353773; //eV/(mol*K)
const double g_per_mol_to_eV_ps2_A2 = 1.0364269648E-4; //eV*ps^2/A^2

void sleep(uint32_t milliseconds);

#define ASSERT(cond) if( !(cond) )\
{ \
   unsigned int line = __LINE__; \
   printf( "Error ---> at file '%s', at line %d\n", __FILE__, line ); \
   printf( "Program will be terminated, info dumped at crash.txt...\n"); \
   FILE *f = fopen("crash.txt","w"); \
   fprintf(f, "Error ---> at file '%s', at line %d\n", __FILE__, line); \
   fclose(f); \
   sleep(10000); \
   exit(1); \
}

template <typename T>
size_t index_of(const std::vector<T>  &vecOfElements, const T &element)
{
    auto begin = vecOfElements.begin();
    auto end = vecOfElements.end();
	auto it = std::find(begin, end, element);
 
	if (it != end) return std::distance(begin, it);
	else return n_pos;
}

template <typename T>
T as(const std::string &str) {
	std::stringstream stream(str);
	T result;
	stream >> result;
	return result;
}

template <typename T>
std::string as_string(const T &value) {
	std::stringstream stream;
	stream << value;
	auto result = stream.str();
	auto len = result.length();

	for (size_t i = 0; i < len; ++i) {
		if (result[i] == ',') result[i] = '.';
	}

	return result;
}

template <typename T>
bool push_back_unique(std::vector<T> &v, const T &value) {
	auto end = v.end();

	if(std::find(v.begin(), end, value) == end) {
		v.push_back(value);
		return true;
	}
	
	return false;
}

inline double round_nplaces(double value, double places) {
    return round(value * places) / places;
}

inline bool is_zero_eps(double num, double eps = 0.00001) {
	return abs(num) < eps;
}

inline double energy_to_temperature(double energy, uint32_t num_of_atoms) {
	return r_k_boltzmann_2_3 * energy / num_of_atoms;
}

bool contains_substr(const std::string &str, const std::string &substr);
bool has_suffix(const std::string &str, const std::string &suffix);
void read_all(const std::string &filename, std::string &content);
void read_line(std::ifstream& stream, std::string& line);
void split(const std::string &str, std::vector<std::string> &tokens, char delim, size_t num_tokens = 0);
std::string get_executable_directory();
int get_time();
void average_data(const std::vector<double*> &data, size_t data_block_size, double &mean);
void average_data(const std::vector<double*> &data, size_t data_block_size, double &mean, double &sigma);
void average_data(const double *data, size_t data_size, double &mean, double &sigma);
void average_data(const double *data, size_t data_size, double &mean);
void average_data(const std::vector<double> &data, double &mean, double &sigma);
void average_data(const std::vector<double> &data, double &mean);
void save_data(const std::string &fname, const std::map<double,std::pair<double,double>> &functable);
void trim(std::string &str, unsigned char c = ' ');
}

#endif //_UTILS_HPP_
