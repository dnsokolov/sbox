#include "luaengine.hpp"
#include "geometries/simplegeometry.hpp"
#include "geometries/boxgeometry.hpp"
#include "geometries/pbcgeometry.hpp"
#include "potentials/gupt.hpp"
#include "randomizers/simplerandomizer.hpp"
#include "randomizers/maxwellrandomizer.hpp"
#include "samplers/mhsampler.hpp"
#include "samplers/mdsampler.hpp"
#include "samplers/exchsampler.hpp"
#include "samplers/hmcsampler.hpp"
#include "integrators/verlet.hpp"
#include "integrators/verletvelocity.hpp"
#include "integrators/nosehooverintegrator.hpp"
#include "integrators/leapfrog.hpp"
#include "core/lightprocess.hpp"
#include "core/atomskprocess.hpp"

#include <thread>
#include <chrono>

#ifdef _WIN32
#include <Windows.h>
#endif

namespace sbox {

#define FREE_GLOBAL_OBJECT(obj) \
   if((obj) != nullptr) { \
      delete (obj);\
      (obj) = nullptr;\
   }


Configuration *_global_config = nullptr;
IGeometry *_global_geometry = nullptr;
IPotential *_global_potential = nullptr;
IRandomizer *_global_randomizer = nullptr;
ISampler *_global_sampler = nullptr;
IIntegrator *_global_integrator = nullptr;
StopWatch *_global_stopwatch = nullptr;
AtomskProcess *_atomsk = nullptr;

bool _continuation = false;
bool _molmasses_loaded = false;
bool _md_flag = false;
size_t _num_of_threads = 1;

void load_config(const std::string &filename, bool continuation) {
    std::string cfile = filename;

    if(continuation) {
       continuation = std::filesystem::exists("_last_xyz");
       if(continuation) cfile = "_last_xyz";
    }

    if(!std::filesystem::exists(cfile)) {
        std::cerr << "File '" << cfile << "' is not exists!" << std::endl;
        return;
    }

    _continuation = continuation;
    _global_config = new Configuration(cfile);
}

void mol_masses_load(const std::string& filename) {
     ASSERT(_global_config != nullptr);
      
     if(!std::filesystem::exists(filename)) {
        std::cerr << "File '" << filename << "' is not exists!" << std::endl;
        return;
    }

    _global_config->LoadMolMassesCE(filename);
    _molmasses_loaded = true;
}

void set_molar_mass(const std::string &component, double value) {
    ASSERT(_global_config != nullptr);
    _global_config->SetMolMass(component, value);
}

size_t get_num_of_atoms() {  
    ASSERT(_global_config != nullptr);
    return _global_config->GetNumOfAtoms();
}

size_t get_num_of_atoms_component(const std::string &cmp) {
    ASSERT(_global_config != nullptr);
    return _global_config->GetNumOfAtoms(cmp);
}

void simple_geometry_init(double rcut) {
    ASSERT(_global_config != nullptr);
    _global_geometry = new SimpleGeometry(rcut);
    _global_config->SetGeometry(_global_geometry);
}

void box_geometry_init(double rcut, luabridge::LuaRef table) {
    ASSERT(_global_config != nullptr);
    double xmin, xmax, ymin, ymax, zmin, zmax;
    luabridge::LuaRef r_xmin = table["xmin"];
    luabridge::LuaRef r_ymin = table["ymin"];
    luabridge::LuaRef r_zmin = table["zmin"];
    luabridge::LuaRef r_xmax = table["xmax"];
    luabridge::LuaRef r_ymax = table["ymax"];
    luabridge::LuaRef r_zmax = table["zmax"];

    if(r_xmin.isNil()) {
        std::cerr << "xmin is not defined" << std::endl;
        exit(1);
    }

    if(r_ymin.isNil()) {
        std::cerr << "ymin is not defined" << std::endl;
        exit(1);
    }

    if(r_zmin.isNil()) {
        std::cerr << "zmin is not defined" << std::endl;
        exit(1);
    }

    if(r_xmax.isNil()) {
        std::cerr << "xmax is not defined" << std::endl;
        exit(1);
    }

    if(r_ymax.isNil()) {
        std::cerr << "ymax is not defined" << std::endl;
        exit(1);
    }

    if(r_zmax.isNil()) {
        std::cerr << "zmax is not defined" << std::endl;
        exit(1);
    }

    xmin = r_xmin.cast<double>();
    ymin = r_ymin.cast<double>();
    zmin = r_zmin.cast<double>();

    xmax = r_xmax.cast<double>();
    ymax = r_ymax.cast<double>();
    zmax = r_zmax.cast<double>();

    _global_geometry = new BoxGeometry(rcut, xmin, xmax, ymin, ymax, zmin, zmax);
    _global_config->SetGeometry(_global_geometry);
}

void pbc_geometry_init(double rcut, luabridge::LuaRef table) {
    ASSERT(_global_config != nullptr);
    double size_x = 0, size_y = 0, size_z = 0;
    luabridge::LuaRef r_size_x = table["size_x"];
    luabridge::LuaRef r_size_y = table["size_y"];
    luabridge::LuaRef r_size_z = table["size_z"];

    if(!r_size_x.isNil()) {
        size_x = r_size_x.cast<double>();
    }

    if(!r_size_y.isNil()) {
        size_y = r_size_y.cast<double>();
    }

    if(!r_size_z.isNil()) {
        size_z = r_size_z.cast<double>();
    }

    _global_geometry = new PBCGeometry(rcut, size_x, size_y, size_z);
    _global_config->SetGeometry(_global_geometry);
}

void gupt_load(const std::string &filename, luabridge::LuaRef num_of_threads) {
    ASSERT(_global_config != nullptr);

    if(!std::filesystem::exists(filename)) {
        std::cerr << "File '" << filename << "' is not exists!" << std::endl;
        return;
    }

    _num_of_threads = 1;
    if(!num_of_threads.isNil() && num_of_threads.isNumber()) {
        _num_of_threads = num_of_threads.cast<size_t>();
    }

    
    _global_potential = new Gupt(_global_config, _num_of_threads);
    Gupt *gupt = static_cast<Gupt*>(_global_potential);
    gupt->LoadParamsFromCEFile(filename);
    _global_config->RegAtomRemoveHandler(static_cast<Configuration::IRemoveAtomHandler*>(_global_potential));
}

void gupt_init(luabridge::LuaRef num_of_threads) {
    ASSERT(_global_config != nullptr);

    _num_of_threads = 1;
    if(!num_of_threads.isNil() && num_of_threads.isNumber()) {
        _num_of_threads = num_of_threads.cast<size_t>();
    }

    _global_potential = new Gupt(_global_config, _num_of_threads);
    _global_config->RegAtomRemoveHandler(static_cast<Configuration::IRemoveAtomHandler*>(_global_potential));
}

void simple_randomizer_init(double drmax, uint32_t seed) {
    ASSERT(_global_config != nullptr);
    SimpleRandomizer *rnd = new SimpleRandomizer(drmax, seed, _global_config);
    _global_randomizer = rnd;
    _global_config->RegAtomRemoveHandler(static_cast<Configuration::IRemoveAtomHandler*>(_global_randomizer));
}

void maxwell_randomizer_init(double time, uint32_t seed) {
    ASSERT(_global_config != nullptr);
    ASSERT(_molmasses_loaded);

    MaxwellRandomizer *rnd = new MaxwellRandomizer(time, seed, _global_config);
    _global_randomizer = rnd;
    _global_config->RegAtomRemoveHandler(static_cast<Configuration::IRemoveAtomHandler*>(_global_randomizer));
}

void integrator_start() {
    ASSERT(_global_integrator != nullptr);
    _global_integrator->Init();
}

void verlet_integrator_init(double dt) {
    ASSERT(_global_config != nullptr);
    ASSERT(_global_potential != nullptr);
    _global_integrator = new Verlet(_global_config, _global_potential, dt);
    _global_config->RegAtomRemoveHandler(static_cast<Configuration::IRemoveAtomHandler*>(_global_integrator));
}

void verlet_velocity_integrator_init(double dt) {
    ASSERT(_global_config != nullptr);
    ASSERT(_global_potential != nullptr);
    _global_integrator = new VerletVelocity(_global_config, _global_potential, dt);
    _global_config->RegAtomRemoveHandler(static_cast<Configuration::IRemoveAtomHandler*>(_global_integrator));
}

void nose_hoover_integrator_init(double dt, double Q, double temperature) {
    ASSERT(_global_config != nullptr);
    ASSERT(_global_potential != nullptr);
    _global_integrator = new NoseHooverIntegrator(_global_config, _global_potential, dt, temperature, Q);
    _global_config->RegAtomRemoveHandler(static_cast<Configuration::IRemoveAtomHandler*>(_global_integrator));
}

void leapfrog_integrator_init(double dt) {
    ASSERT(_global_config != nullptr);
    ASSERT(_global_potential != nullptr);
    _global_integrator = new LeapFrog(_global_config, _global_potential, dt);
    _global_config->RegAtomRemoveHandler(static_cast<Configuration::IRemoveAtomHandler*>(_global_integrator));
    /*std::cout << "Leapfrog integrator is initialized" << std::endl;
    std::string inp;
    std::getline(std::cin, inp);*/
}

std::string find_file(const std::string &extension) {
    std::filesystem::path path = std::filesystem::current_path();
    std::vector<std::string> files;

    for (const std::filesystem::path &entry : std::filesystem::directory_iterator(path)) {
        if(entry.extension() == extension) {
            files.push_back(entry.string());
        }
    }

    if(files.size() == 0) return "";
    if(files.size() == 1) {
       std::cout << "File '" << files[0] << "' was found" << std::endl;
       return files[0];
    }

    size_t size = files.size();
    std::cout << size << " files were found. Select the file you want:" << std::endl;

    for(size_t i = 0; i < size; ++i) {
        std::cout << (i+1) << ") " << files[i] << std::endl;
    }

    size_t number;
    std::cout << "Enter number: ";
    std::cin >> number;
    --number;
    number %= size;

    std::cout << "You select file '" << files[number] << "'" << std::endl;

    return files[number];
}

void sampler_init(const std::string &sampler_name, luabridge::LuaRef params) {
    ASSERT(_global_config != nullptr);
    bool ok_flag = false;

    if(sampler_name == "MH") {
        _global_sampler = new MHSampler(_global_config, _global_randomizer, _global_potential, _continuation);
        _md_flag = false;
        ok_flag = true;
    }

    if(sampler_name == "MD") {
        ASSERT(_global_integrator != nullptr);
        _md_flag = true;
        _global_sampler = new MDSampler(_global_config, _global_randomizer, _global_potential, _global_integrator, _continuation);
        ok_flag = true;
    }

    if(sampler_name == "EXCH") {
        _global_sampler = new ExchSampler(_global_config, _global_randomizer, _global_potential, _continuation);
        _md_flag = false;
        ok_flag = true;
    }

    if(sampler_name == "HMC") {
        ASSERT(_global_integrator != nullptr);
        _global_sampler = new HMCSampler(_global_config, _global_randomizer, _global_potential, _global_integrator, _continuation);
        _md_flag = true;
        ok_flag = true;
        std::cout << "HMC sampler is initialized" << std::endl;  
    }

    if(!ok_flag) {
        std::cerr << "Unknown name of sampler '" << sampler_name << "'" << std::endl;
        return;
    }

    set_sampler_params(params);
    _global_config->RegAtomRemoveHandler(static_cast<Configuration::IRemoveAtomHandler*>(_global_sampler));
}

void MH_sampler_init(luabridge::LuaRef params) {
     sampler_init("MH", params);
}

void MD_sampler_init(luabridge::LuaRef params) {
    sampler_init("MD", params);
}

void EXCH_sampler_init(luabridge::LuaRef params) {
   sampler_init("EXCH", params);
}

void HMC_sampler_init(luabridge::LuaRef params) {
    sampler_init("HMC", params);
}

void random_velocities_init(double temperature) {
    ASSERT(_global_randomizer != nullptr);

    auto mol_masses = _global_config->GetMolMasses();
    auto atoms = _global_config->GetAtoms();
    auto end = atoms.end();

    MaxwellRandomizer mxrnd(1, _global_randomizer->GetSeed(), _global_config);
    mxrnd.SetCurrentTemperature(temperature);

    Vec3D velocity;

    for(auto i = atoms.begin(); i != end; ++i) {
        mxrnd.SetCurrentAtom(*i);
        mxrnd.GetRandomTranslation(velocity);
        (*i)->velocity = velocity;
    }
}

void run_temperature(double temperature) {
    ASSERT(_global_sampler != nullptr);
    _global_sampler->Run(temperature);
}

void run(double start_temperature, double finish_temperature, double delta_temperature) {
    ASSERT(_global_sampler != nullptr);
    
    if(_continuation && _global_config->ContainsProperty("temperature")) {
        start_temperature = as<double>(_global_config->properties["temperature"]);
    }

    double current_temperature = start_temperature;
    delta_temperature = abs(delta_temperature);
    if(is_zero_eps(delta_temperature)) delta_temperature = 1;
    if(start_temperature > finish_temperature) delta_temperature = -delta_temperature;
    std::string sys_name = _global_config->GetSystemName();
  
    while (true) {
        std::cout << sys_name << "> " << "Run simulation for  T = " << current_temperature << " K" << std::endl;
        _global_sampler->Run(current_temperature);
        std::cout << std::endl;

        current_temperature += delta_temperature;
        if(start_temperature <= finish_temperature && 
           current_temperature > finish_temperature) break;
        if(start_temperature > finish_temperature &&
           current_temperature < finish_temperature) break;
    }
}

std::string get_system_name() {
    ASSERT(_global_config != nullptr);
    return _global_config->GetSystemName();
}

std::string get_system_name_from(const std::string &fname) {
    if(std::filesystem::exists(fname)) {
        Configuration config(fname);
        return config.GetSystemName();
    }

    return  "";
}

double get_config_energy() {
    ASSERT(_global_config != nullptr);
    return _global_config->GetFullEnergy();
}

double get_config_energy_per_atom() {
    return get_config_energy()/_global_config->GetNumOfAtoms();
}

void get_config_box(luabridge::LuaRef box, luabridge::LuaRef fname) {
    Vec3D min = {0, 0, 0}, max = {0, 0, 0};

    if(fname.isNil()) {
        ASSERT(_global_config != nullptr);      
        _global_config->GetBox(min,max);
    }
    else if (file_exists(fname.cast<std::string>())){
        Configuration config(fname.cast<std::string>());
        config.GetBox(min, max);
    }

    box["xmin"] = min[0];
    box["ymin"] = min[1];
    box["zmin"] = min[2];
    box["xmax"] = max[0];
    box["ymax"] = max[1];
    box["zmax"] = max[2];
}

void get_caloric_dat(const std::string &xyzfile, const std::string &datfile) {
    if(!std::filesystem::exists(xyzfile)) {
        std::cerr << "File '" << xyzfile << "' is not exists!" << std::endl;
        return;
    }

    Configuration current_config;
    ifstream xyzstream;
    xyzstream.open(xyzfile);
    std::vector<double> values;
    double temperature, last_temperature = -1.0;
    double mean, sigma;
    FuncTable table({"T,K", "U,eV/atom", "sigma"});

    while (XYZFormat::NextFrame(xyzstream, current_config)) {
        if(!current_config.ContainsProperty("temperature")) {
            std::cerr << "This config is not contain property 'temperature'" << std::endl;
            xyzstream.close();
            break;
        }

        temperature = as<double>(current_config.properties["temperature"]);
        if(last_temperature < 0) last_temperature = temperature;

        if(last_temperature != temperature) {
            average_data(values, mean, sigma);
            table.InsertValues({last_temperature, mean, sigma});
            last_temperature = temperature;
            values.clear();
        }

        values.push_back(current_config.GetFullEnergy()/current_config.GetNumOfAtoms());
        current_config.Clear();    
    }  

    average_data(values, mean, sigma);
    table.InsertValues({last_temperature, mean, sigma});
    table.SaveToFile(datfile);

    xyzstream.close();
}

double get_inter_atomic_dist(luabridge::LuaRef num_of_neighbours) {
    ASSERT(_global_config != nullptr);
    ASSERT(_global_geometry != nullptr);
    size_t nn = num_of_neighbours.isNil() ? 12 : num_of_neighbours.cast<size_t>();
    return _global_config->GetInterAtomicDist(nn);
}

bool get_md_flag(const std::string &sampler_name) {
    if(sampler_name == "MD") return true;
    if(sampler_name == "HMC") return true;
    return false;
}

void print_logo() {
    std::cout << PROGRAM_NAME<< "   "<< PROGRAM_VERSION << std::endl;
    std::cout << "Engine: sbox " << SBOX_VERSION << std::endl;
    std::cout << SBOX_COPYRIGHT << std::endl;
    std::cout << SBOX_AUTHORS << std::endl << std::endl;
    std::cout << "Powered by Lua" << std::endl; 
    std::cout << LUA_COPYRIGHT << std::endl << LUA_AUTHORS << std::endl << std::endl;

    #if defined(__INTEL_COMPILER)
    std::cout << "Compiled with Intel Compiler" << std::endl;
    #endif
    #if defined(PRIVATE_VER)
    std::cout << "--- Private version ---" << std::endl << std::endl;;
    #endif
}

std::string fmt(luabridge::LuaRef str, luabridge::LuaRef separator) {
    if(str.isTable()) {
        auto length = str.length();
        std::stringstream ss;

        for(auto i = 1; i <= length; ++i) {
            auto val = str[i].cast<std::string>();
            if (str[i].isNumber()) val = as_string(str[i].cast<double>());
           ss << val;
           if(!separator.isNil()) {
               ss << separator.cast<std::string>();
           }
        }

        return ss.str();
    }

    if(!str.isString()) return "";

    std::string buffer{str.cast<string>()};
    std::string result{""};
    std::string expr{""};
    std::string execstr;
    bool read_expr_flag{false};
    lua_State *state = str.state();
    std::vector<std::string> back_global_exec;

    //std::cout << buffer << std::endl;
   lua_Debug debug;
   lua_Debug *entry = &debug;
   int num_var = 1;

   lua_getstack(state, 1, entry);
   lua_getinfo(state, "Sln", entry);

   while(true) {
      auto var = lua_getlocal(state, entry, num_var);
      if(!var) break;

      auto stack_size = lua_gettop(state);
      auto type = luaL_typename(state, stack_size);
      std::string vars = var;
      std::string types = type;

      if(types == "function" && vars == "(*temporary)") {
          break;
      }

      if(!(types == "number" || types == "string" || types == "boolean")) {
          ++num_var;
          continue;
      }

      execstr = var;
      std::string save_global_exec = "_global_" + vars + " = " + vars;
      back_global_exec.emplace_back(vars + " = " + "_global_" + vars);
      luaL_dostring(state, save_global_exec.c_str());

      execstr += " = ";

      if(types != "string") {
          auto v = lua_tostring(state, stack_size);
          execstr += v;
          luaL_dostring(state, execstr.c_str());
      }

      if(types == "string") {
          auto v = lua_tostring(state, stack_size);
          execstr += "\"";
          execstr += v;
          execstr += "\"";
          luaL_dostring(state, execstr.c_str());
      }

      ++num_var;
   }

    for(const auto &c: buffer) {
        if(c == '[') {
            read_expr_flag = true;
            continue;
        }

        if(c == ']') {
            execstr = "_expr_result = " + expr;
            luaL_dostring(state, execstr.c_str());
            luaL_dostring(state, "_expr_result = tostring(_expr_result)");

            expr = "";
            read_expr_flag = false;
            luabridge::LuaRef expr_result = luabridge::getGlobal(state, "_expr_result");
            
            result += expr_result.cast<std::string>(); 
            continue;
        }

        if(read_expr_flag) {
            expr += c;
            continue;
        }

        result += c;
    }

    for(auto &s: back_global_exec) {
        luaL_dostring(state, s.c_str());
    }

    return result;
}

double get_potential_energy() {
    ASSERT(_global_potential != nullptr);
    return _global_potential->GetFullEnergy();
}

double get_potential_energy_per_atom() {
    ASSERT(_global_potential != nullptr);
    return _global_potential->GetFullEnergy()/_global_config->GetNumOfAtoms();
}

void remove_atom(size_t num) {
    ASSERT(_global_config != nullptr);
    ASSERT(num < _global_config->GetNumOfAtoms());
    _global_config->RemoveAtom(num);
}

void print_atom_info(size_t num) {
    ASSERT(_global_config != nullptr);
    ASSERT(num < _global_config->GetNumOfAtoms());
    Atom *a = _global_config->GetAtom(num);
    std::cout << a->symbol << " " << a->position << " " << a->velocity << " " << a->energy << std::endl;
}

size_t find_min_energy_atom(bool exclude_frozen) {
    ASSERT(_global_config != nullptr);
    Atom **a;
    return _global_config->FindMinEnergyAtom(a, exclude_frozen);
}

size_t find_max_energy_atom(bool exclude_frozen) {
    ASSERT(_global_config != nullptr);
    Atom **a;
    return _global_config->FindMaxEnergyAtom(a, exclude_frozen);
}

void remove_max_energy_atoms(int count) {
    ASSERT(_global_potential != nullptr);
    size_t atom;
    double energy;
    std::cout << "Energy = " << (_global_config->GetFullEnergy()/_global_config->GetNumOfAtoms()) << " eV/atom" << std::endl;

    while (count > 0) {
        atom = find_max_energy_atom(true);
        std::cout << atom << " ---> ";
        print_atom_info(atom);
        remove_atom(atom);
        std::cout << "Atom " << atom << " is deleted" << std::endl;
        energy = _global_potential->GetFullEnergy()/_global_config->GetNumOfAtoms();
        std::cout << "Energy = " << energy << " eV/atom" << std::endl << std::endl;
        --count;
    }
    
}

void config_dump(const std::string &filename, bool append) {
    ASSERT(_global_config != nullptr);
    _global_config->RefreshComment(true,true);
    _global_config->SaveToFile(filename, append);
}

void config_update(double temperature, luabridge::LuaRef n) {
    ASSERT(_global_sampler != nullptr);
    _global_sampler->InitRun(temperature);
    
    if(n.isNil()) { 
        if(temperature <= 0.0) _global_sampler->UpdateZero();
        else _global_sampler->Update();    
    }
    else {
        size_t num = n.cast<size_t>();
        std::function<void()> update;
        
        if (temperature <= 0.0) 
            update = ([]() {
            _global_sampler->UpdateZero();
        });
        else 
            update = ([]() {
            _global_sampler->Update();
        });    

        for(size_t i = 0; i < num; ++i) {
            update();
        }
    }
}

void set_configs_to_cms(const std::string &filename) {
    ASSERT(_global_config != nullptr);

    if(!std::filesystem::exists(filename)) {
        std::cerr << "File '" << filename << "' is not exists" << std::endl;
        return;
    }

    XYZFormat xyz;
    std::ifstream fs(filename);
    Configuration config;

    while (xyz.NextFrame(fs,config)) {
        config.SetMolMasses(_global_config->GetMolMasses()); //!!!Warning
        config.SetCMS();
        config.SaveToFile(filename +".tmp", true);
        config.Clear();
    }

    std::filesystem::remove(filename);
    std::filesystem::rename(filename + ".tmp", filename);
}

void print_config_info() {
    if(_global_config == nullptr) {
        std::cout << "Warning: _global_config is null!" << std::endl;
        return;
    }

    _global_config->PrintInfo();
}

void print_geometry_info() {
    if(_global_geometry == nullptr) {
        std::cout << "Warning: _global_geometry is null!" << std::endl;
        return;
    }

    _global_geometry->PrintInfo();
}

void print_randomizer_info() {
    if(_global_randomizer== nullptr) {
        std::cout << "Warning: _global_randomizer is null!" << std::endl;
        return;
    }

    _global_randomizer->PrintInfo();
}

void print_potential_info() {
    if(_global_potential== nullptr) {
        std::cout << "Warning: _global_potential is null!" << std::endl;
        return;
    }

    _global_potential->PrintInfo();
}

void print_sampler_info() {
    if(_global_randomizer== nullptr) {
        std::cout << "Warning: _global_sampler is null!" << std::endl;
        return;
    }

    _global_sampler->PrintInfo();
}

void print_info() {
    print_config_info();
    std::cout << std::endl;
    print_geometry_info();
    std::cout << std::endl;
    print_randomizer_info();
    std::cout << std::endl;
    print_potential_info();
    std::cout << std::endl;
    print_sampler_info();
    std::cout << std::endl;
}

void free_global_config() {
    FREE_GLOBAL_OBJECT(_global_config);
}

void free_global_geometry() {
    FREE_GLOBAL_OBJECT(_global_geometry);
}

void free_global_potential() {
    FREE_GLOBAL_OBJECT(_global_potential);
}

void free_global_randomizer() {
    FREE_GLOBAL_OBJECT(_global_randomizer);
}

void free_global_sampler() {
    FREE_GLOBAL_OBJECT(_global_sampler);
}

void free_global_integrator() {
    FREE_GLOBAL_OBJECT(_global_integrator);
}

void free_global_atomsk() {
    FREE_GLOBAL_OBJECT(_atomsk);
}

void free_all() {
    free_global_config();
    free_global_geometry();
    free_global_potential();
    free_global_randomizer();
    free_global_sampler();
    free_global_integrator();
    free_global_atomsk();
}

bool directory_exists(const std::string &dirname) {
    return std::filesystem::is_directory(dirname) && std::filesystem::exists(dirname);
}

void directory_remove(const std::string &dirname) {
    std::filesystem::remove_all(dirname);
}

void directory_create(const std::string &dirname) {
    if(!directory_exists(dirname))
       std::filesystem::create_directory(dirname);
}

void directory_change(const std::string &dirname) {
    std::filesystem::current_path(dirname);
}

void directory_lock() {
    if(std::filesystem::exists(".lock")) {
        std::cerr << "Error: Directory " << std::filesystem::current_path() << " is locked by another sbox process." << std::endl;
        std::cerr << "Do you want to ignore and continue (no recommended)? n/[y]" << std::endl;

        char c = getchar();
        if( c == 'y' || c == 'Y') return;

        std::cerr << "Program will be terminated..." << std::endl;
        sleep(3000);
        exit(1);
    }

    std::ofstream lockfile(".lock");
    lockfile.close();

    #ifdef _WIN32
    wchar_t* fileLPCWSTR = L".lock"; 
    int attr = GetFileAttributesW(fileLPCWSTR);
    if ((attr & FILE_ATTRIBUTE_HIDDEN) == 0) {
       SetFileAttributesW(fileLPCWSTR, attr | FILE_ATTRIBUTE_HIDDEN);
    }
    #endif
}

void directory_unlock() {
    if(std::filesystem::exists(".lock")) {
        std::filesystem::remove(".lock");
    }
}

void process_start(luabridge::LuaRef options) {
    LightProcess lproc;
    
    if(options.isString()) {
        std::string line = options.cast<std::string>();
        lproc.options.ParseCmdLine(line);
        lproc.Execute();
    }
    else if(options.isTable()) {
        if(!options["cmdline"].isNil()) {
            std::string line = options["cmdline"].cast<std::string>();
            lproc.options.ParseCmdLine(line);
        }

        if(!options["wait_for_exit"].isNil()) {
            lproc.options.wait_for_exit = options["wait_for_exit"].cast<bool>();
        }

        if(!options["create_new_console"].isNil()) {
            lproc.options.create_new_console = options["create_new_console"].cast<bool>();
        }

        lproc.Execute();
    }

}

void adj_execute(const std::string &args) {
     LightProcess lproc;
     bool findflag = false;
     std::string adj_path = lproc.FindProgram("adj", findflag);

     if(findflag) {
        std::string path = "'" + adj_path + "'";
        std::string line = path + " " + args;

        lproc.options.ParseCmdLine(line);
        lproc.options.create_new_console = false;
        lproc.options.wait_for_exit = true;

        std::string cmdline = lproc.options.GetCmdLine();
        lproc.Execute();
     }
}

void sbox_execute(luabridge::LuaRef options) {
    if(options.isString()) {
        LightProcess lproc;
        std::string sbox_dir = get_executable_directory();

        #ifndef _WIN32
        std::string path = sbox_dir + "/mtp";
        std::string line = "xterm -e '" + path + "' " + options.cast<std::string>();
        #else
        std::string path = "'" + sbox_dir + "/mtp.exe'";
        std::string line =  path + " " + options.cast<std::string>();
        #endif
        
        lproc.options.ParseCmdLine(line);
       // lproc.options.wait_for_exit = true;

       /* auto end = lproc.options.args.end();
        for(auto i = lproc.options.args.begin(); i != end; ++i) {
            std::cout << *i << std::endl;
        }*/

        lproc.Execute();
    }
    else if(options.isTable()) {
        if(options["script"].isNil()) {
            std::cerr << "You need to define name script in options['script']" << std::endl;
            return;
        }

        if(options["path"].isNil()) {
           std::string sbox_dir = get_executable_directory();
           #ifndef _WIN32
           options["path"] = sbox_dir + "/mtp";
           #else
           options["path"] = sbox_dir + "/mtp.exe";
           #endif

           std::string path = options["path"].cast<std::string>();
           if(path.find(' ') != std::string::npos) {
               options["path"] = "'" + path + "'";
           }
        }

        #ifndef _WIN32
        options["cmdline"] = "xterm -e " + options["path"].cast<std::string>() + " " + options["script"].cast<std::string>();
        #else
        options["cmdline"] = options["path"].cast<std::string>() + " " + options["script"].cast<std::string>();
        #endif
        process_start(options);
    }
}

void find_files(const std::string &extension, luabridge::LuaRef files) {
    std::filesystem::path path = std::filesystem::current_path();

    for (const std::filesystem::path &entry : std::filesystem::directory_iterator(path)) {
        if(entry.extension() == extension) {
            files.append(entry.string());
        }
    }
}

void file_copy(const std::string &source, const std::string &destination) {
    std::filesystem::copy_file(source, destination);
}

bool file_exists(const std::string &filename) {
    return std::filesystem::is_regular_file(filename) && std::filesystem::exists(filename);
}

void file_remove(const std::string &filename) {
    std::filesystem::remove(filename);
}

void file_remove_if_exists(const std::string &filename) {
    if(file_exists(filename)) {
        file_remove(filename);
    }
}

void file_rename(const std::string &from_fn, const std::string &to_fn) {
    if(std::filesystem::exists(from_fn)) {
        std::filesystem::rename(from_fn, to_fn);
    }
}

void files_copy(luabridge::LuaRef files, const::string &dirname) {
    if(!directory_exists(dirname)) {
        directory_create(dirname);
    }

    if(files.isTable()) {
        size_t size = files.length();
        std::string fname;
        std::string filepath;
        for(size_t i = 1; i <= size; ++i) {
            filepath = files[i].cast<std::string>();
            fname = std::filesystem::path(filepath).filename().string();
            file_copy(filepath, dirname + "/" + fname);
        }
    }
}

void list_directories(luabridge::LuaRef dirs, const std::string &dirname) {
    std::filesystem::path path;
    if(dirname == "") path = std::filesystem::current_path();
    else path = dirname;

    for (const std::filesystem::path &entry : std::filesystem::directory_iterator(path)) {
        if(std::filesystem::is_directory(entry)) {
            dirs.append(entry.string());
        }
    }
}

void list_files(luabridge::LuaRef files, const std::string &dirname) {
    std::filesystem::path path;
    if(dirname == "") path = std::filesystem::current_path();
    else path = dirname;

    std::string extension = "";
    if(files.isTable()) {
        if(!files["extension"].isNil()) {
            extension = files["extension"].cast<std::string>();
        }
    }

    std::string templ = "";
    if(files.isTable()) {
        if(!files["template"].isNil()) {
            templ = files["template"].cast<std::string>();
        }
    }

    for (const std::filesystem::path &entry : std::filesystem::directory_iterator(path)) {
        if(std::filesystem::is_regular_file(entry)) {
            if(extension == "" || extension == entry.extension().string())
            {
                if(templ == "" || (entry.string().find(templ) != std::string::npos))
                   files.append(entry.string());
            }
        }
    }
}

void get_potential_params(const std::string &comp1, const std::string &comp2, luabridge::LuaRef params) {
    ASSERT(_global_potential != nullptr);
    std::vector<double> v_params;
    std::string pot_name = _global_potential->GetName();
    std::string result = _global_potential->GetParams(comp1, comp2, v_params);

    params.append(pot_name);

    auto end = v_params.end();
    for(auto i = v_params.begin(); i != end; ++i) {
        params.append(*i);
    }
}

void set_potential_params(const std::string &comp1, const std::string &comp2, luabridge::LuaRef params) {
    ASSERT(_global_potential != nullptr);
    std::stringstream strparam;

    if(params.isTable()) {
        strparam << params[1].cast<std::string>() << " " << comp1 << " " << comp2 << " ";

        size_t size = params.length();
        for(size_t i = 2; i <= size; ++i) {
           strparam << params[i] << " ";
        }
    }
    else {
        return;
    }

    _global_potential->AddParams(strparam.str());
}

void set_potential_line(const std::string &line) {
    ASSERT(_global_config != nullptr);
    _global_potential->AddParams(line);
}

void get_config_components(luabridge::LuaRef components) {
    ASSERT(_global_config != nullptr);
    std::vector<std::string> v_components = _global_config->GetSymbols();

    auto end = v_components.end();
    for(auto i = v_components.begin(); i != end; ++i) {
        components.append(*i);
    }
}

void set_sampler_params(luabridge::LuaRef table) {
    ASSERT(_global_sampler != nullptr);

    for(auto&& pair : luabridge::pairs(table)) {
        _global_sampler->SetParam(pair.first.cast<std::string>(), pair.second.cast<std::string>());
    }
}

std::string get_working_directory() {
    return std::filesystem::current_path().string();
}

std::string get_parent_directory(const std::string &path) {
    std::filesystem::path dpath(path);
    return dpath.parent_path().string();
}

std::string get_file_extension(const std::string &fname) {
    return std::filesystem::path(fname).extension().string();
}

std::string get_file_name(const std::string &fname) {
    return std::filesystem::path(fname).filename().string();
}

std::string get_directory_name(const std::string &dir) {
    return std::filesystem::path(dir).filename().string();
}

std::string get_file_name_without_extension(const std::string &fname) {
    std::string fn = std::filesystem::path(fname).filename().string();
    size_t lindx = fn.find_last_of(".");
    if(lindx != n_pos) return fn.substr(0, lindx);
    else return fn;
}

void directories_create(luabridge::LuaRef dirs) {
    if(dirs.isTable()) {
        size_t size = dirs.length();
        for(size_t i = 1; i <= size; ++i) {
            directory_create(dirs[i].cast<std::string>());
        }
    }
}

void select_configs(const std::string &filename, luabridge::LuaRef select_fn, luabridge::LuaRef configs) {
    if(!std::filesystem::exists(filename)) {
        std::cerr << "File '" << filename << "' is not exists (in function select_configs)." << std::endl;
        return;
    }

    if(!select_fn.isFunction()) {
        std::cerr << "Second parametr in 'select_configs' (select_fn) must to be a bool function" << std::endl;
        return;
    }

    XYZFormat xyz;
    std::ifstream fs(filename);
    Configuration config;

    while(xyz.NextFrame(fs, config)) {
        config.delete_atoms = false;

        if(select_fn(config)) {
            configs.append(config);
            config.Clear();
            continue;
        }

        config.delete_atoms = true;
        config.Clear();
    }

    fs.close();
}

void test(luabridge::LuaRef prm) {
   // auto lua_state = prm.state();
   // luaL_dostring(lua_state, "print(_VERSION)");
}

void stopwatch_start() {
    if(_global_stopwatch == nullptr) _global_stopwatch = new StopWatch();
    _global_stopwatch->Start();
}

void stopwatch_stop(luabridge::LuaRef units) {
    ASSERT(_global_stopwatch != nullptr);
    std::string uns = "mcs";

    if(units.isString()) {
        uns = units.cast<std::string>();
    }

    int64_t res;
    std::string duration = _global_stopwatch->Stop(uns, res);
    std::cout << "Duration: " << duration << std::endl;
}

void benchmarking_run(luabridge::LuaRef params) {
    ASSERT(_global_sampler != nullptr);

    size_t num = 1000;

    if(params.isNumber()) {
        num = params.cast<size_t>();
    }

    StopWatch sw;
    std::string units = "mcs";

    std::cout << "Test of _global_potential->GetFullEnergy(Atom *a, ...) function" << std::endl;
    Atom *a = nullptr;
    std::vector<Atom*> neighbours, atoms;
    atoms = _global_config->GetAtoms();
    int64_t duration, sum = 0;
    int64_t result_1, result_2;
    IPotential::ICalculationStrategy *_strategy = _global_potential->GetStrategy();

    _strategy->GetFullEnergy();

    for(size_t i = 0; i < num; ++i) {
        neighbours.clear();
        _global_randomizer->GetRandomAtom(&a);
        a->SetOldEnergy();
        _global_potential->GetNeighbours(a, neighbours);

        sw.Start();
        _strategy->GetFullEnergy(a, neighbours);
        sw.Stop(units, duration);

        sum += duration;
    }

    result_1 = (sum/num);
    std::cout << "Result: " << result_1 << units << " (" << (1000000/result_1) << "Hz)" << std::endl;

    sum = 0;
    std::cout << "Test of _global_sampler->Update() function" << std::endl;
    _global_sampler->InitRun(300);

    for(size_t i = 0; i < num; ++i) {
        sw.Start();
        _global_sampler->Update();
        sw.Stop(units, duration);
        sum +=duration;
    }

    result_2 = (sum/num);
    std::cout << "Result: " << result_2 << units << " (" << (1000000/result_2) << "Hz)" << std::endl;
    std::cout << "Percentage of calculation of potential function: " << (result_1 * 100) / result_2 << "%" << std::endl;

    std::cout << "Test of _global_potential->DoEnergyJob() function" << std::endl;

    sum = 0;
    Range<size_t> range;
    range.start = 0;
    range.finish = get_num_of_atoms() / _num_of_threads;
    range.id = 0;

    for(size_t i = 0; i < num; ++i) {
        _global_randomizer->GetRandomAtom(&a);
        _global_potential->GetNeighbours(a, neighbours);

        sw.Start();
        _global_potential->DoEnergyJob(a, range);
        sw.Stop(units, duration);

        sum += duration;
    }

    result_2 = (sum/num);
    std::cout << "Result: " << result_2 << units << " (" << (1000000/result_2) << "Hz)" << std::endl;

    std::cout << "Test of _global_potential->GetAtomEnergy() function" << std::endl;
    sum = 0;

    for(size_t i = 0; i < num; ++i) {
        _global_randomizer->GetRandomAtom(&a);
        _global_potential->GetNeighbours(a, neighbours);

        sw.Start();
        _global_potential->GetAtomEnergy(a);
        sw.Stop(units, duration);

        sum += duration;
    }

    result_2 = (sum/num);
    std::cout << "Result: " << result_2 << units << " (" << (1000000/result_2) << "Hz)" << std::endl;
}

void atomsk_init(luabridge::LuaRef options) {
    std::string atomsk_path{""};
    if(options.isString()) atomsk_path = options.cast<std::string>();

    if(_atomsk) {
        _atomsk->Launch(atomsk_path);
        return;
    }

    _atomsk = new AtomskProcess(atomsk_path);
}

void atomsk_do(luabridge::LuaRef com) {
    ASSERT(_atomsk != nullptr);
    if(_atomsk->GetErrorState()) return;

    if(com.isString()) {
        _atomsk->DoString(com.cast<std::string>());
        return;
    } 

    if(com.isTable()) {
        int len = com.length();
        for(int i = 1; i <= len; ++i) {
            if(_atomsk->GetErrorState()) return;
            _atomsk->DoString(com[i].cast<std::string>());
        }
    }
}

bool atomsk_dofile(const std::string &fname) {
    ASSERT(_atomsk != nullptr);

    std::ifstream inpfile(fname);

    if(!inpfile.is_open()) {
        std::cerr << "Error: couldn't open file '" << fname << "'" << std::endl;
        return false;
    }

    std::string line;
    while(!inpfile.eof()) {
        std::getline(inpfile, line);
        if(_atomsk->GetErrorState()) return false;
        _atomsk->DoString(line);
    }

    return true;
}

std::unordered_map<std::string, luabridge::LuaRef> get_key_value_map(const luabridge::LuaRef& table)
{
    using namespace luabridge;
    std::unordered_map<std::string, LuaRef> result;
    if (table.isNil()) { return result; }
 
    auto L = table.state();
    push(L, table); // push table
 
    lua_pushnil(L);  // push nil, so lua_next removes it from stack and puts (k, v) on stack
    while (lua_next(L, -2) != 0) { // -2, because we have table at -1
        if (lua_isstring(L, -2)) { // only store stuff with string keys
            result.emplace(lua_tostring(L, -2), LuaRef::fromStack(L, -1));
        }
        lua_pop(L, 1); // remove value, keep key for lua_next
    }
 
    lua_pop(L, 1); // pop table
    return result;
}

void atomsk_def(luabridge::LuaRef def) {
    ASSERT(_atomsk != nullptr);
    if(_atomsk->GetErrorState()) return;

    if(def.isString()) {
        _atomsk->Define(def.cast<std::string>());
        return;
    } 

    if(def.isTable()) {
        int len = def.length();

        for(int i = 1; i <= len; ++i) {
            _atomsk->Define(def[i].cast<std::string>());
        }

        auto map = get_key_value_map(def);
        
        for(auto &pair: map) {
            std::string val = pair.second.cast<std::string>();
            if (pair.second.isNumber()) val = as_string(pair.second.cast<double>());
            _atomsk->Define(pair.first, val);
        }
    }
}

void atomsk_clear_def() {
    ASSERT(_atomsk != nullptr);
    _atomsk->ClearDefines();
}

void atomsk_quit() {
    ASSERT(_atomsk != nullptr);
    _atomsk->Quit();
}

bool atomsk_get_error_state() {
    ASSERT(_atomsk != nullptr);
    return _atomsk->GetErrorState();
}

void atomsk_get_value_vr(const std::string &regname, luabridge::LuaRef vec) {
    ASSERT(_atomsk != nullptr);
    if(vec.isTable()) {
        Vec3D val = _atomsk->GetValueVR(regname);
        vec.append(val[0]);
        vec.append(val[1]);
        vec.append(val[2]);
    } 
}

void atomsk_set_value_vr(const std::string &regname, luabridge::LuaRef vec) {
    ASSERT(_atomsk != nullptr);

    if(!vec.isNil() && vec.isTable() && vec.length() == 3) {
        Vec3D val;
        val[0] = vec[1].cast<double>();
        val[1] = vec[2].cast<double>();
        val[2] = vec[3].cast<double>();
        _atomsk->SetValueVR(regname, val);
        return;
    }

    if(!vec.isNil() && vec.isNumber()) {
        double nm = vec.cast<double>();
        Vec3D val(nm, nm, nm);
        _atomsk->SetValueVR(regname, val);
        return;
    }
}

double to_number(const std::string& str) {
    return as<double>(str);
}

std::string to_string(luabridge::LuaRef val) {
    auto result = val.cast<std::string>();
    if (val.isNumber()) {
        result = as_string(val.cast<double>());
    }
    return result;
}

}