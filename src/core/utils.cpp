#include "utils.hpp"
#include <sstream>

namespace sbox {

void read_all(const std::string &filename, std::string &content) {
    std::ifstream fs(filename);
    content.assign((std::istreambuf_iterator<char>(fs)),
                        (std::istreambuf_iterator<char>()));
    fs.close();
}

void read_line(std::ifstream& stream, std::string& line) {
	std::getline(stream, line);
	size_t length = line.length();
	if (line.length() <= 1) return;
	if (line[length - 1] == '\r') line = line.substr(0, length - 1);
}

void split(const std::string &str, std::vector<std::string> &tokens, char delim, size_t num_tokens) {

   tokens.clear();
   std::string token;
   std::istringstream tokenStream(str);

   while (std::getline(tokenStream, token, delim)) {
      tokens.push_back(token);
      if(num_tokens != 0 && tokens.size() == num_tokens) break;
   }
}

bool has_suffix(const std::string &str, const std::string &suffix) {
    return str.size() >= suffix.size() &&
           str.compare(str.size() - suffix.size(), suffix.size(), suffix) == 0;
}

std::string get_executable_directory() {
   char buffer[MAX_PATH];
   #ifndef _WIN32
   ssize_t result = readlink("/proc/self/exe", buffer, MAX_PATH - 1);
   buffer[result] = '\0';
   #else
   GetModuleFileNameA(NULL, buffer, MAX_PATH - 1);
   #endif
   std::filesystem::path exe(buffer);
   return exe.parent_path().string();
}


bool contains_substr(const std::string &str, const std::string &substr) {
	return str.find(substr) != std::string::npos;
}

int get_time() {
   return time(NULL);
}

void average_data(const std::vector<double*> &data, size_t data_block_size, double &mean, double &sigma) {
	mean = 0.0;
	sigma = 0.0;
	
	size_t data_size = data.size() * data_block_size;
	if(data_size == 0) return;
	
	auto end = data.end();
	
	double d;
	
	for(auto p = data.begin(); p != end; ++p) {
		for(size_t i = 0; i < data_block_size; ++i) {
			mean += (*p)[i];
		}
	}
	
	mean /= data_size;
	
	if(data_size <= 1) return;
	
	for(auto p = data.begin(); p != end; ++p) {
		for(size_t i = 0; i < data_block_size; ++i) {
			d = mean - (*p)[i];
			sigma += (d * d);
		}
	}
	
	sigma = sqrt(sigma/(data_size - 1));
}

void average_data(const std::vector<double*> &data, size_t data_block_size, double &mean) {
	mean = 0.0;
	
	size_t data_size = data.size() * data_block_size;
	if(data_size == 0) return;
	
	auto end = data.end();
	
	double d;
	
	for(auto p = data.begin(); p != end; ++p) {
		for(size_t i = 0; i < data_block_size; ++i) {
			mean += (*p)[i];
		}
	}
	
	mean /= data_size;
}

void average_data(const double *data, size_t data_size, double &mean, double &sigma) {
	mean = 0.0;
	sigma = 0.0;
	double d;
	
	if(data_size == 0) return;
	
	for(size_t i = 0; i < data_size; ++i) {
		mean += data[i];
	}
	
	mean /= data_size;
	if(data_size <= 1) return;
	
	for(size_t i = 0; i < data_size; ++i) {
		d = data[i] - mean;
		sigma += (d * d);
	}
	
	sigma = sqrt(sigma/(data_size - 1));
}

void average_data(const double *data, size_t data_size, double &mean) {
	mean = 0.0;
	double d;
	
	if(data_size == 0) return;
	
	for(size_t i = 0; i < data_size; ++i) {
		mean += data[i];
	}
	
	mean /= data_size;
}

void average_data(const std::vector<double> &data, double &mean, double &sigma) {
	mean = 0.0;
	sigma = 0.0;
	size_t data_size = data.size();
	double d;
	auto end = data.end();
	
	if(data_size == 0) return;

    for(auto i = data.begin(); i != end; ++i) {
		mean += (*i);
	}
	
	mean /= data_size;
	
	if(data_size <= 1) return;
	
	for(auto i = data.begin(); i != end; ++i) {
		d = mean - (*i);
		sigma += (d * d);
	}
	
	sigma = sqrt(sigma/(data_size - 1));
}

void average_data(const std::vector<double> &data, double &mean) {
	mean = 0.0;

	size_t data_size = data.size();
	auto end = data.end();
	
	if(data_size == 0) return;

    for(auto i = data.begin(); i != end; ++i) {
		mean += (*i);
	}
	
	mean /= data_size;
}

void save_data(const std::string &fname, const std::map<double,std::pair<double,double>> &functable) {
	std::ofstream out(fname);

    auto end = functable.end();
	for(auto i = functable.begin(); i != end; ++i) {
		out << i->first << " " << i->second.first << " " << i->second.second << std::endl;
	}

	out.close();
}

void sleep(uint32_t milliseconds) {
    std::this_thread::sleep_for(std::chrono::milliseconds(milliseconds));
}

void trim(std::string &str, unsigned char c) {
	if(c == ' ') {
		str.erase(str.begin(), std::find_if(str.begin(), str.end(), [](unsigned char ch) {
			return !std::isspace(ch);
		}));
		str.erase(std::find_if(str.rbegin(), str.rend(), [](unsigned char ch) {
			return !std::isspace(ch);
		}).base(), str.end());
	} else {
		str.erase(str.begin(), std::find_if(str.begin(), str.end(), [c](unsigned char ch) {
			return c != ch;
		}));
		str.erase(std::find_if(str.rbegin(), str.rend(), [c](unsigned char ch) {
			return c != ch;
		}).base(), str.end());
	}
}

}