#include "propertyreader.hpp"

namespace sbox {
std::string PropertyReader::GetPotentialParams(
        const std::string &potname, 
        const std::string &comp1, 
        const std::string &comp2
        ) {
        std::vector<std::string> potentials = toml::get<std::vector<std::string>>(data.at("potentials"));
        auto end = potentials.end();

		std::stringstream strs;
		std::string pot, c1, c2;

        for(auto i = potentials.begin(); i != end; ++i) {
			strs.clear();
			strs.str(*i);

			strs >> pot;
			strs >> c1;
			strs >> c2;

			if (potname == "") {
				if ((c1 == comp1 && c2 == comp2) || (c1 == comp2 && c2 == comp1)) return *i;
			}
			else {
				if ((potname == pot) && (c1 == comp1 && c2 == comp2) || (c1 == comp2 && c2 == comp1)) return *i;
			}
        }
 
        return "";
}
}
