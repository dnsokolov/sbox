#pragma once

#include "../../external/TinyProcess/process.hpp"
#include "lightprocess.hpp"
#include <unordered_map>
#include <sstream>
#include <array>
#include <mutex>
#include <atomic>
#include "core/configuration.hpp"
#include "number.hpp"
#include "utils.hpp"

namespace sbox {
    using Process = TinyProcessLib::Process;
  
    class AtomskProcess {
        private:
        std::string _atomsk_path;
        bool _exists_flag{false};
        bool _error_state{false};

        atomic<bool> _overwrite_flag{false};
        atomic<bool> _block_flag{false};
        std::recursive_mutex _mutex, _block;

        Process *_proc = nullptr;
        std::unordered_map<std::string, std::string> _deftable;
        //Configuration *_config{nullptr};
        std::string _last_str{""};

        Vec3D _vr1{0.0, 0.0, 0.0}, _vr2{0.0, 0.0, 0.0}, _vr3{0.0, 0.0, 0.0};
        Vec3D _vr1s{0.0, 0.0, 0.0}, _vr2s{0.0, 0.0, 0.0}, _vr3s{0.0, 0.0, 0.0};
        double _x1s{0}, _y1s{0}, _z1s{0}, _x2s{0}, _y2s{0}, _z2s{0}, _x3s{0}, _y3s{0}, _z3s{0}; 

        const static int _N_cmds = 20;
        const static int _N_regs = 12;

        enum class RegisterType {
            VR, DR, NUM, UNKNOWN
        };

        enum class TokenType {
            REG, MEM, UNKNOWN
        };

        const char _int_cmds[_N_cmds][10] = {
            "mov", "add", "sub", "show",
            "sleep", "exists", "ldbox", "dif", "wr", "rmfile",
            "mrg", "exec", "mul", "div", "sum", "dot", "push", "pop", "freeze",
            "fit"
        };

        const char _reg_names[_N_regs][5] = {
            "vr1", "vr2", "vr3",
            "drx1", "dry1", "drz1",
            "drx2", "dry2", "drz2",
            "drx3", "dry3", "drz3"
        };

        using VRRegistry = const unordered_map<std::string, Vec3D*>;
        using DRRegistry = const unordered_map<std::string, double*>;

        VRRegistry _vr = {
            {"vr1", &_vr1},
            {"vr2", &_vr2},
            {"vr3", &_vr3}
        };

        DRRegistry _dr = {
            {"drx1", &_vr1[0]},
            {"dry1", &_vr1[1]},
            {"drz1", &_vr1[2]},
            {"drx2", &_vr2[0]},
            {"dry2", &_vr2[1]},
            {"drz2", &_vr2[2]},
            {"drx3", &_vr3[0]},
            {"dry3", &_vr3[1]},
            {"drz3", &_vr3[2]}
        };

        std::string VecToString(const Vec3D &v) const noexcept {
            std::stringstream ss;
            ss << as_string(v[0]) << " " << as_string(v[1]) << " " << as_string(v[2]);
            return ss.str();
        }

        bool _IsIntCmd(const std::string &token) {
            for(int i = 0; i < _N_cmds; ++i) {
                if(token == _int_cmds[i]) return true;
            }
            return false;
        }

        bool _IsRegName(const std::string &name, RegisterType &regtype) const noexcept {
            regtype = RegisterType::UNKNOWN;

            for(int i = 0; i < _N_regs; ++i) {
                if(name == _reg_names[i]) {
                    if(name.find("vr") == 0) regtype = RegisterType::VR;
                    if(name.find("dr") == 0) regtype = RegisterType::DR;
                    return true;
                }
            }

            return false;
        }

        TokenType _GetTokenType(const std::string &token, RegisterType &regtype) {
            if(_IsRegName(token,regtype)) return TokenType::REG;

            regtype = RegisterType::UNKNOWN;
            size_t tk_len = token.length();
            if(tk_len > 2 && token[0] == '%' && token[tk_len-1] == '%') return TokenType::MEM;

            return TokenType::UNKNOWN;
        }

        public:
        AtomskProcess(const std::string &atomsk_path = "", bool launch = true) {
            if(launch) {
                Launch(atomsk_path);
            }
            Define("cb", "0.5*BOX");
            Define("CB", "0.5*BOX 0.5*BOX 0.5*BOX");
            Define("n", "10");
        }

        ~AtomskProcess() {
            Quit();
            //if(_config) delete _config;
        }

        void Quit() {
            if(_proc) {
                _proc->kill();
                delete _proc;
                _proc = nullptr;
                std::cout << "atomsk is closed" << std::endl;
            }
        }

        void Launch(const std::string &atomsk_path) {
            if(_proc) {
                std::cout << "Error: atomsk is launched already" << std::endl;
                return;
            }

            if(atomsk_path == "") {
                LightProcess proc;
                _atomsk_path = proc.FindProgram("atomsk", _exists_flag);
                if(_exists_flag) {
                    _exists_flag = std::filesystem::exists(_atomsk_path);
                }
            }

            if(_exists_flag) {
                _proc = new Process(_atomsk_path, Process::string_type(), [this](const char *bytes,size_t n) {this->_StdOutHandle(bytes,n);}, [this](const char *bytes,size_t n) {this->_StdErrHandle(bytes,n);}, true);
            }
            else {
                std::cout << "Error:" << std::endl;
                std::cout << "Path to atomsk is not found" << std::endl;
                std::cout << "Add path at 'setting.toml' to section [pathes]" << std::endl;
            }
        }

        bool PathExists() const noexcept {
            return _exists_flag;
        }

        bool GetErrorState() const noexcept {
            return _error_state;
        }

        std::string GetAtomskPath() const noexcept {
            return _atomsk_path;
        }

        void DoString(const std::string &line) {
             _block.lock();
             _block_flag.store(true);
            std::cout << "Input: " << line << std::endl;

            if(_error_state) {
                std::cout << "Error: couldn't to execute command: _error_state is true." << std::endl;
                std::cout << "Problem at: " << _last_str << std::endl;
                return;
            }

            if(_proc != nullptr) {
                _last_str = line;
                std::string str = DoPreprocessing(line);

                if(str == "") { 
                   _block.unlock();
                   _block_flag.store(false);
                   return;
                }  
                
                std::cout << "Execute: " << str << std::endl << std::endl;
                auto res = _proc->write(str + "\n");
                _error_state = !res;
                std::cout.flush();
            }
        }

        void Define(const std::string &key, const std::string &value) {
            _deftable[key] = value;
        }

        void Define(const std::string &line) {
            std::stringstream inp_ss(line);
            std::string key{""};
            std::string value{""};

            inp_ss >> key;
            value = line.substr(key.length());
            trim(value);

            _deftable[key] = value;
        }

        std::string GetValue(const std::string &key, bool return_key = false) const {
            std::string key_ = key;
            trim(key_,'%');

            if(_deftable.find(key_) != _deftable.end()) {
                return _deftable.at(key_);
            }
             
            if(key.length() > 1 && key[0] == '*') {
                trim(key_,'*');
                RegisterType reg_type;
                auto success = _IsRegName(key_, reg_type);
                if(success && reg_type == RegisterType::DR) {
                    auto value = _dr.at(key_);
                    return as_string(*value);
                }
                if(success && reg_type == RegisterType::VR) {
                    auto value = _vr.at(key_);
                    return VecToString(*value);
                }
            }

            if(return_key) return key;
            return "";
        }

        Vec3D GetVectorValue(const std::string &key, bool &succes) {
            RegisterType type;
            succes = false;

            if(_IsRegName(key, type)) {
                if(type == RegisterType::VR) {
                    succes = true;
                    return *_vr.at(key);
                }
                else {
                    succes = false;
                    return Vec3D(0.0,0.0,0.0);
                }
            }

            std::string value = GetValue(key);

            if(value == "") {
                succes = false;
                return Vec3D(0.0,0.0,0.0);
            }

            NumVector3D vec;
            if(vec.Parse(value)) {
                Vec3D result;
                vec.GetComponents(result[0], result[1], result[2]);
                succes = true;
                return result;
            }

            return Vec3D(0.0,0.0,0.0);
        }

        std::string DoPreprocessing(const std::string &line) {
            std::cout << "Preprocessing: " << line << std::endl;

            std::stringstream inp_ss(line), out_ss;
            std::string token, key, value;
            bool flag{false};
            size_t len;

            while(!inp_ss.eof()) {
                std::getline(inp_ss, token, ' ');

                len = token.length();
                if(len >= 1 && token[0] == '#') {
                    return "";
                }

                if(token == "def") {
                    std::string defline(line);
                    defline.erase(0,4);
                    trim(defline);
                    Define(defline);
                    return "";
                }

                if(token == "quit") {
                    Quit();
                    return "";
                }

                if(_IsIntCmd(token)) {
                    std::cout << "Execute internal command" << std::endl << std::endl;
                    _ExecIntCmd(line);
                    return "";
                }

                if(len>2) {
                    flag = token[0] == '%' && token[len-1] == '%';

                    if(flag) {
                        key = token.substr(1, len-2);

                        if(_deftable.find(key) != _deftable.end()) {
                            value = GetValue(key);
                        }
                        else {
                            std::cout << "Enter value of " << key << ": ";
                            std::getline(std::cin, value);
                            _deftable[key] = value;
                        }

                        out_ss << value << " ";
                        flag = false;
                        continue;
                    }
                }

                if(token[0] == '*') {
                        value = GetValue(token);
                        if(value != "") {
                            out_ss << value << " ";
                        }
                }

                out_ss << token << " ";
            }

            return out_ss.str();
        }

        void ClearDefines() {
            _deftable.clear();
        }

        Vec3D GetValueVR(const std::string &regname) const noexcept {
            if(regname == "vr1") return _vr1;
            if(regname == "vr2") return _vr2;
            if(regname == "vr3") return _vr3;
            return Vec3D(0.0, 0.0, 0.0);
        }

        void SetValueVR(const std::string &regname, const Vec3D &val) noexcept {
            if(regname == "vr1") {
                _vr1 = val;
                return;
            }

            if(regname == "vr2") {
                _vr2 = val;
                return;
            }

            if(regname == "vr3") {
                _vr3 = val;
                return;
            }
        }

        void _StdOutHandle(const char *bytes, size_t n) {
            _mutex.lock();
            std::string msg = std::string(bytes,n);
            std::cout << msg << std::endl;
            std::cout.flush();

            auto block_flag = _block_flag.load() && msg.find("@atomsk:") != std::string::npos;

            if(block_flag) {
                _block_flag.store(false);
                _block.unlock();
            }

            if(_overwrite_flag.load() && msg.find("<?> This file already exists:") != std::string::npos) {
                std::cout << "====== " << msg << " ======" << std::endl;
                 _overwrite_flag.store(false);
                std::cout << "preprocessor --> OK, overwriting..." << std::endl;
                auto res = _proc->write("y\n");
                _error_state = !res;
            }
            _mutex.unlock();
        }

        void _StdErrHandle(const char *bytes, size_t n) {
            _mutex.lock();
            if(_block_flag.load()) {
                _block_flag.store(false);
                _block.unlock();
            }
            std::string msg = std::string(bytes,n);
            std::cout << msg << std::endl;
            std::cout.flush();
            _mutex.unlock();
        }

        private:
        void _ExecIntCmd(const std::string &line) {
            std::stringstream inp_ss(line);
            std::string token, arg;
            std::vector<std::string> args;

            inp_ss >> token;

            while (!inp_ss.eof()) {
                inp_ss >> arg;
                args.emplace_back(arg);
            }

            if(token == "mov") {
                _movCmd(args);
                return;
            }

            if(token == "show") {
                _showCmd(args);
                return;
            }

            if(token == "add") {
                _arithmCmd("add", args);
                return;
            }

            if(token == "sub") {
                _arithmCmd("sub", args);
                return;
            }

            if(token == "mul") {
                _arithmCmd("mul", args);
                return;
            }

            if(token == "div") {
                _arithmCmd("div", args);
                return;
            }

            if(token == "sleep") {
                if(args.size() != 1) {
                    std::cout << "sleep expect 1 argument" << std::endl;
                    _error_state = true;
                    return;
                }

                Number n;
                auto value = GetValue(args[0], true);

                if(n.Parse(value)) {
                    if(n.IsInteger()) {
                        sleep(n.GetIntegerValue());
                        return;
                    }
                }

                std::cout << "Wrong usage sleep" << std::endl;
                _error_state = true;
                return;
            }

            if(token == "exists") {
                std::vector<std::string> tkns;
                split(line, tkns, ' ', 2);
                auto fname = GetValue(tkns[1], true);

                if(!std::filesystem::exists(fname)) {
                    std::cout << "File '" << fname << "' is not exists" << std::endl;
                    _error_state = true;
                    return;
                }
                return;
            }

            if(token == "ldbox") {
                std::vector<std::string> tkns;
                split(line, tkns, ' ', 2);

                if(tkns.size() != 2) {
                    std::cout << "Error: ldbox except 1 argument (f1le1.xyz)" << std::endl;
                    _error_state = true;
                    return;
                }

                auto fname = GetValue(tkns[1], true);
                
                if(!std::filesystem::exists(fname)) {
                    std::cout << "File '" << fname << "' is not exists" << std::endl;
                    _error_state = true;
                    return;
                }

                Configuration config;
                config.LoadFromFile(tkns[1]);
                config.GetBox(_vr1,_vr2);
                return;
            }

            if(token == "dif") {
                std::vector<std::string> tkns;
                split(line, tkns, ' ', 3);

                if(tkns.size() != 3) {
                    std::cout << "Error: dif except 2 arguments (f1le1.xyz file2.xyz)" << std::endl;
                    _error_state = true;
                    return;
                }

                auto fname1 = GetValue(tkns[1], true);
                auto fname2 = GetValue(tkns[2], true);

                if(!std::filesystem::exists(fname1)) {
                    std::cout << "File '" << fname1 << "' is not exists" << std::endl;
                    _error_state = true;
                    return;
                }

                if(!std::filesystem::exists(fname2)) {
                    std::cout << "File '" << fname2 << "' is not exists" << std::endl;
                    _error_state = true;
                    return;
                }

                Configuration config1;
                config1.LoadFromFile(fname1);
                Vec3D max1, min1;
                config1.GetBox(min1, max1);

                Configuration config2;
                config2.LoadFromFile(fname2);
                Vec3D max2, min2;
                config2.GetBox(min2, max2);

                _vr1 = max1 - min2;
                _vr2 = min1 - max2;

                return;
            }

            if(token == "wr") {
                std::vector<std::string> tkns;
                split(line, tkns, ' ', 2);

                if(tkns.size() != 2) {
                    std::cout << "Error: wrong usage: wr except 1 argument (f1le1.xyz)" << std::endl;
                    _error_state = true;
                    return;
                }

                auto fname = GetValue(tkns[1], true);
                if(std::filesystem::exists(fname) && std::filesystem::is_regular_file(fname)) {
                    std::cout << "Deleting '" << fname << "' ..." << std::endl;
                    std::filesystem::remove(fname);
                }

               // _overwrite_flag.store(true);

                std::string str = "write " + fname;
                std::cout << "Execute (from internal wr): " << str << std::endl;
                auto res = _proc->write(str + "\n");
                _error_state = !res;

                for(size_t i = 0; i < 10000000; ++i) {
                    if(std::filesystem::exists(fname)) break;
                }

                std::cout.flush();
                return;
            }

            if(token == "rmfile") {
                std::stringstream inp(line);
                std::string val;

                inp >> val;

                while(!inp.eof()) {
                    inp >> val;
                    val = GetValue(val, true);

                    if(std::filesystem::exists(val) && std::filesystem::is_regular_file(val)) {
                        std::filesystem::remove(val);
                    }
                }

                return;
            }

            if(token == "mrg") {
                std::stringstream inp(line);
                std::string val;

                inp >> val;
                if(inp.eof()) return;

                inp >> val;
                Configuration config;
                val = GetValue(val,true);
                config.LoadFromFile(val);

                std::vector<std::string> xyz_files;

                while(!inp.eof()) {
                    inp >> val;
                    val = GetValue(val,true);
                    xyz_files.emplace_back(val);
                }

                size_t size = xyz_files.size();

                if(size == 1) {
                    std::cout << "Error: wrong usage mrg: define output file at end of line (file.xyz)" << std::endl;
                    _error_state = true;
                    return;
                }

                std::string outputfile = xyz_files.at(size - 1);

                 if(size-1 > 0) {
                     for(size_t i = 0; i < size - 1; ++i) {
                         if(std::filesystem::exists(xyz_files[i])) {
                            config.AppendFromFile(xyz_files[i]);
                         }
                         else {
                             std::cout << "Error: in mrg Couldn't find file '" << xyz_files[i] << "'" << std::endl;
                             _error_state = true;
                             return;
                         }
                     }
                 }
                 
                 config.RefreshComment(true, true);
                 config.SaveToFile(outputfile);
                 return;
            }

            if(token == "exec") {
                std::string cmd = line;
                trim(cmd);
                cmd.erase(0, 5);
                trim(cmd);

                LightProcess lproc;
                lproc.options.ParseCmdLine(cmd);

                auto end = lproc.options.args.end();
                for(auto i = lproc.options.args.begin(); i != end; ++i) {
                    (*i) = GetValue(*i, true);
                }

                lproc.options.wait_for_exit = true;
                lproc.Execute();

                return;
            }

            if(token == "sum") {
                if(args.size() != 1) {
                    std::cout << "Error: sum except 1 argument (vr1 or vr2 or vr3)" << std::endl;
                    _error_state = true;
                    return;
                }

                RegisterType rg_type;
                if(_IsRegName(args[0], rg_type)) {
                    if(rg_type == RegisterType::VR) {
                        auto rgp = _vr.at(args[0]);
                        auto val = (*rgp)[0] + (*rgp)[1] + (*rgp)[2];
                        (*rgp)[0] = val;
                        (*rgp)[1] = val;
                        (*rgp)[2] = val;
                        return;
                    }
                }

                std::cout << "Error: couldn't execute command cmd " << args[0] << std::endl;
                _error_state = true;
            }

            if(token == "dot") {
                _dotCmd(args);
                return;
            }

            if(token == "push") {
                if(args.size() != 1) {
                    std::cout << "Error: wrong usage: push except 1 argument (REGISTER)" << std::endl;
                    _error_state = true;
                    return;
                }

                RegisterType rg_type;
                TokenType tk_type = _GetTokenType(args[0], rg_type);

                if(tk_type != TokenType::REG) {
                    std::cout << "Error: command push apply only to registers" << std::endl;
                    _error_state = true;
                    return;
                }

                if(args[0] == "vr1") {
                    _vr1s = _vr1;
                    return;
                }

                if(args[0] == "vr2") {
                    _vr2s = _vr2;
                    return;
                }

                if(args[0] == "vr3") {
                    _vr3s = _vr3;
                    return;
                }

                if(args[0] == "drx1") {
                    _x1s = _vr1[0];
                    return;
                }

                if(args[0] == "drx2") {
                    _x2s = _vr2[0];
                    return;
                }

                if(args[0] == "drx3") {
                    _x3s = _vr3[0];
                    return;
                }

                if(args[0] == "dry1") {
                    _y1s = _vr1[1];
                    return;
                }

                if(args[0] == "dry2") {
                    _y2s = _vr2[1];
                    return;
                }

                if(args[0] == "dry3") {
                    _y3s = _vr3[1];
                    return;
                }

                if(args[0] == "drz1") {
                    _z1s = _vr1[2];
                    return;
                }

                if(args[0] == "drz2") {
                    _z2s = _vr2[2];
                    return;
                }

                if(args[0] == "drz3") {
                    _z3s = _vr3[2];
                    return;
                }
            }

            if(token == "pop") {
                if(args.size() != 1) {
                    std::cout << "Error: wrong usage: pop except 1 argument (REGISTER)" << std::endl;
                    _error_state = true;
                    return;
                }

                RegisterType rg_type;
                TokenType tk_type = _GetTokenType(args[0], rg_type);

                if(tk_type != TokenType::REG) {
                    std::cout << "Error: command pop apply only to registers" << std::endl;
                    _error_state = true;
                    return;
                }

                if(args[0] == "vr1") {
                    _vr1 = _vr1s;
                    return;
                }

                if(args[0] == "vr2") {
                    _vr2 = _vr2s;
                    return;
                }

                if(args[0] == "vr3") {
                    _vr3 = _vr3s;
                    return;
                }

                if(args[0] == "drx1") {
                    _vr1[0] = _x1s;
                    return;
                }

                if(args[0] == "drx2") {
                    _vr2[0] = _x2s;
                    return;
                }

                if(args[0] == "drx3") {
                    _vr3[0] = _x3s;
                    return;
                }

                if(args[0] == "dry1") {
                    _vr1[1] = _y1s;
                    return;
                }

                if(args[0] == "dry2") {
                    _vr2[1] = _y2s;
                    return;
                }

                if(args[0] == "dry3") {
                    _vr3[1] = _y3s;
                    return;
                }

                if(args[0] == "drz1") {
                    _vr1[2] = _z1s;
                    return;
                }

                if(args[0] == "drz2") {
                    _vr2[2] = _z2s;
                    return;
                }

                if(args[0] == "drz3") {
                     _vr3[2] = _z3s;
                    return;
                }
            }

            if(token == "freeze") {
                _freezeCmd(args);
                return;
            }

            if(token == "fit") {
                if(args.size() != 2) {
                    std::cout << "Error: wrong usage: fit except 2 argument (file.xyz. number)" << std::endl;
                    _error_state = true;
                    return;
                }

                auto fname = GetValue(args[0], true);

                if(std::filesystem::exists(fname) && std::filesystem::is_regular_file(fname)) {
                    Number num;

                    if(num.Parse(args[1]) && num.IsInteger()) {
                        Configuration config(fname);
                        config.FitTo(num.GetIntegerValue());
                        config.SaveToFile(fname);
                    }
                }

                return;
            }
 
            std::cout << "Unknown token '" << token << "'" << std::endl; 
            _error_state = true;
        }

        template
        <class T1, class T2>
        void _doArithmOperation(const std::string &cmd, T1 *dest, T2 *source) {
            if(cmd == "add") { 
                (*dest) += (*source);
                return;
            }

            if(cmd == "sub") { 
                (*dest) -= (*source);
                return;
            }

            if(cmd == "mul") { 
                (*dest) *= (*source);
                return;
            }

            if(cmd == "div") { 
                (*dest) /= (*source);
                return;
            }
        }

        void _arithmCmd(const std::string &cmd, const std::vector<std::string> &args) {
            if(args.size() != 2) {
                std::cout <<"Error: " << cmd << " expect 2 arguments" << std::endl;
                _error_state = true;
                return;
            }

            RegisterType rg_type1 = RegisterType::UNKNOWN;
            RegisterType rg_type2 = RegisterType::UNKNOWN;

            TokenType tk_type1 = _GetTokenType(args[0], rg_type1);
            TokenType tk_type2 = _GetTokenType(args[1], rg_type2);

            if(tk_type1 == TokenType::REG && tk_type2 == TokenType::REG) {
                if(rg_type1 == RegisterType::DR && rg_type2 == RegisterType::DR) 
                {
                    auto dest = _dr.at(args[0]);
                    auto source = _dr.at(args[1]);
                    _doArithmOperation(cmd, dest, source);
                    return;
                }

                if(rg_type1 == RegisterType::VR && rg_type2 == RegisterType::VR) {
                    auto dest = _vr.at(args[0]);
                    auto source = _vr.at(args[1]);
                    _doArithmOperation(cmd, dest, source);
                    return;
                }
            }

            if(tk_type1 == TokenType::REG && (tk_type2 == TokenType::MEM || tk_type2 == TokenType::UNKNOWN)) {
                if(rg_type1 == RegisterType::VR) {
                    NumVector3D vec;
                    auto success = vec.Parse(GetValue(args[1], true));

                    if(success) {
                        auto dest = _vr.at(args[0]);
                        Vec3D val{0,0,0};
                        vec.GetComponents(val[0], val[1], val[2]);
                        _doArithmOperation(cmd, dest, &val);
                        return;
                    }
                } 

                if(rg_type1 == RegisterType::DR) {
                    Number num;
                    auto success = num.Parse(GetValue(args[1], true));

                    if(success) {
                        auto dest = _dr.at(args[0]);
                        auto val = num.GetDoubleValue();
                        _doArithmOperation(cmd, dest, &val);
                        return;
                    }
                }
            }

            if(rg_type1 == RegisterType::VR) {
                Number num;
                auto success = num.Parse(GetValue(args[1],true));

                if(success) {
                    auto dest = _vr.at(args[0]);
                    auto val = num.GetDoubleValue();
                    _doArithmOperation(cmd, dest, &val);
                    return;
                }
            }

            std::cout << "Error: couldn't to execute command "<< cmd << " " << args[0] << " " << args[1] << std::endl;
            _error_state = true;
        }

        template
        <class RT>
        void _movReg(RT &m, const std::string &dest_rg, const std::string &source_rg) {
            auto dest = m.at(dest_rg);
            auto source = m.at(source_rg);
            *dest = *source;
        }

        void _movCmd(const std::vector<std::string> &args) {
            if(args.size() != 2) {
                std::cout << "mov expect 2 arguments" << std::endl;
                _error_state = true;
                return;
            }

            RegisterType rg_type1 = RegisterType::UNKNOWN;
            RegisterType rg_type2 = RegisterType::UNKNOWN;

            TokenType tk_type1 = _GetTokenType(args[0], rg_type1);
            TokenType tk_type2 = _GetTokenType(args[1], rg_type2);

            if((tk_type1 == TokenType::REG && tk_type2 == TokenType::REG) &&
                                rg_type1 != rg_type2) {
                std::cout << "Error: Incompatible register types" << std::endl;
                _error_state = true;
                return;
            }

            if(tk_type1 == tk_type2 && tk_type1 == TokenType::REG) {
                if(rg_type1 == RegisterType::VR) {
                    _movReg(_vr, args[0], args[1]);
                    return;
                }

                if(rg_type1 == RegisterType::DR) {
                    _movReg(_dr, args[0], args[1]);
                    return;
                }
            }

            if(tk_type1 == TokenType::REG && tk_type2 == TokenType::MEM) {
                if(rg_type1 == RegisterType::VR) {
                    auto dest = _vr.at(args[0]);
                    bool success = false;
                    auto source = GetVectorValue(args[1], success);

                    if(success) {
                        *dest = source;
                        return;
                    }
                    else {
                        std::cout << "Couldn't read " << args[1] << std::endl;
                        _error_state = true;
                        return;
                    }
                }

                if(rg_type1 == RegisterType::DR) {
                    auto dest = _dr.at(args[0]);
                    auto value = GetValue(args[1]);

                    if(value == "") {
                        std::cout << "Couldn't read " << args[1] << std::endl;
                        _error_state = true;
                        return;
                    }
                    
                    Number num;
                    if(num.Parse(value)) {
                        *dest = num.GetDoubleValue();
                        return;
                    }

                    std::cout << "Couldn't read " << args[1] << std::endl;
                    _error_state = true;
                    return;
                }
            }

            if(tk_type1 == TokenType::MEM && tk_type2 == TokenType::REG) {
                std::string key = args[0];
                trim(key,'%');
                 
                if(rg_type2 == RegisterType::VR) {
                    auto value = VecToString(*_vr.at(args[1]));
                    Define(key, value);
                    return;
                }

                if(rg_type2 == RegisterType::DR) {
                    auto value = as_string(*_dr.at(args[1]));
                    Define(key, value);
                    return;
                }
            }

            if(tk_type1 == TokenType::REG && tk_type2 == TokenType::UNKNOWN) {
                Number num;
                if(num.Parse(args[1])) {
                    if(rg_type1 == RegisterType::DR) {
                        auto dest = _dr.at(args[0]);
                        *dest = num.GetDoubleValue();
                        return;
                    }
                    if(rg_type1 == RegisterType::VR) {
                        auto dest = _vr.at(args[0]);
                        auto val = num.GetDoubleValue();
                        (*dest)[0] = val;
                        (*dest)[1] = val;
                        (*dest)[2] = val;
                        return;
                    }
                }
            }

            std::cout << "Error: bad operation: mov " << args[0] << " " << args[1] << std::endl;
            _error_state = true;
        }

        void _dotCmd(const std::vector<std::string> &args) {
            if(args.size() != 2) {
                std::cout << "Error: wrong usage dot: except two arguments (VR and vector)" << std::endl;
                _error_state = true;
                return;
            }

            RegisterType rg_type1;
            TokenType tk_type1 = _GetTokenType(args[0], rg_type1);

            RegisterType rg_type2;
            TokenType tk_type2 = _GetTokenType(args[1], rg_type2);

            if(rg_type1 == RegisterType::VR) {
                NumVector3D vec;
                std::string arg = args[1];

                if(tk_type2 == TokenType::REG) arg = "*" + arg;
                if(tk_type2 == TokenType::MEM) arg = "%" + arg + "%";

                if(vec.Parse(GetValue(arg,true))) {
                    auto dest = _vr.at(args[0]);
                    Vec3D v{0,0,0};
                    vec.GetComponents(v[0], v[1], v[2]);
                    auto sc = blitz::dot(*dest, v);
                    (*dest)[0] = sc;
                    (*dest)[1] = sc;
                    (*dest)[2] = sc;
                    return;
                }
            }

            std::cout << "Error: couldn't execute command dot " << args[0] << " " << args[1] << std::endl;
            _error_state = true;
        }

        void _showCmd(const std::vector<std::string> &args) {
            std::string value = GetValue(args[0], true);

            if(value != "") {
                std::cout << value << std::endl;
                return;
            }

            if(args[0] == "*vr") {
                std::cout << "vr1: " << VecToString(_vr1) << std::endl;
                std::cout << "vr2: " << VecToString(_vr2) << std::endl;
                std::cout << "vr3: " << VecToString(_vr3) << std::endl;
                return;
            }

            if(args[0] == "*dr") {
                std::cout << "drx1: " << _vr1[0] << " dry1: " << _vr1[1] << " drz1 " << _vr1[2] << std::endl;
                std::cout << "drx2: " << _vr2[0] << " dry2: " << _vr2[1] << " drz2 " << _vr2[2] << std::endl;
                std::cout << "drx3: " << _vr1[0] << " dry3: " << _vr1[1] << " drz3 " << _vr1[2] << std::endl;
                return;
            }
        }

        void _freezeCmd(const std::vector<std::string> &args) {
            if(args.size() != 2) {
                std::cout << "Error: freeze except 2 arguments (file.xyz parametr[numer group|element name]" << std::endl;
                _error_state = true;
                return;
            }

            auto fname = args[0];
            fname = GetValue(fname,true);

            if(!std::filesystem::exists(fname)) {
                std::cout << "Error: freeze couldn't find file " << fname << "'" << std::endl;
                _error_state = true;
                return;
            }

            Number num;
            Configuration config(fname);
            
            if(num.Parse(args[1])) {
                if(num.IsInteger()) {
                    std::cout << "Freeze group " << num.GetIntegerValue() << std::endl;
                    Configuration::GroupSelector gselector(config);
                    gselector.Select(num.GetIntegerValue()-1, true);
                    config.RefreshComment(true, true);
                    config.SaveToFile(fname);
                    return;
                }
            }

            auto sp = GetValue(args[1],true);
            std::cout << "Freeze species " << sp << std::endl;
            Configuration::SpeciesSelector spselector(sp, true);
            config.SelectAtoms(spselector);
            config.RefreshComment(true,true);
            config.SaveToFile(fname);
        }
    };
}