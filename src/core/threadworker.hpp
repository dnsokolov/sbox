#pragma once

#include <thread>
#include <functional>
#include <vector>
#include <chrono>
#include <atomic>
#include <mutex>

namespace sbox {

    class ThreadWorker {
        private:
        std::atomic_bool _start{false}, _stop{false};
        size_t _job_id, _len = 0;
        size_t _t;
        std::vector<std::function<void(size_t)>> _jobs;
        std::thread *_thread = nullptr;
        std::atomic_bool *_shared_start = nullptr;
        std::atomic_int *_shared_counter = nullptr;
        std::atomic_int *_task_type = nullptr;

        public:
        ThreadWorker(const std::vector<std::function<void(size_t)>> &jobs, 
                      size_t job_id) {
            _jobs = jobs;
            _job_id = job_id;
        }

        ThreadWorker(const std::vector<std::function<void(size_t)>> &jobs, 
                      size_t job_id, std::atomic_bool *shared_start,
                      std::atomic_int *shared_counter, std::atomic_int *task_type, 
                      size_t num_of_threads) {
            _jobs = jobs;
            _job_id = job_id;

            _shared_start = shared_start;
            _task_type = task_type;
  
            _len = sizeof(std::atomic_bool) * num_of_threads;
            _shared_counter = shared_counter;
        }

        ThreadWorker(ThreadWorker &&worker) {
            _jobs = worker._jobs;
            _job_id = worker._job_id;
            _shared_start = worker._shared_start;
            _task_type = worker._task_type;
            _thread = std::move(worker._thread);
        }

        ThreadWorker(const ThreadWorker &worker) {
            _jobs = worker._jobs;
            _job_id = worker._job_id;
            _shared_start = worker._shared_start;
            _thread = worker._thread;
            _task_type = worker._task_type;
        }

        void Execute() {
            if(_thread) return;

            if(_shared_start == nullptr)
            {
                _thread = new std::thread ([&](){
                while (true) {
                    while (!_start.load());
                    if(_stop.load()) break;
                    _jobs[_t](_job_id);
                    _start.store(false);     
                } 
                });
            }
            else
            {
                _thread = new std::thread ([&](){
                while (true) {
                    while (!_shared_start->load());
                    if(_stop.load()) break;
                    _jobs[*_task_type](_job_id); 
                    _shared_start->store(false);
                    _shared_counter->fetch_add(1);    
                } 
                });
            }

            _thread->detach();
        }

        ~ThreadWorker() {
            _stop.store(true);
            _start.store(true);

            std::this_thread::sleep_for(std::chrono::microseconds(500));
            if(_thread) delete _thread;
        }

        void DoJob(size_t t) {
            _t = t;
            _start.store(true);
        }

        void Wait() {
            while (_start.load());
        }
    };
}