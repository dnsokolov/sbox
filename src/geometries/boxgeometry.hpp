#pragma once

#include "igeometry.hpp"

namespace sbox {
    class BoxGeometry : public IGeometry {
        private:
        double _xmin, _xmax, _ymin, _ymax, _zmin, _zmax;
        Vec3D pos, dr;

        public:
        BoxGeometry(double rcut, 
                    double xmin, double xmax,
                    double ymin, double ymax,
                    double zmin, double zmax
        ) : IGeometry(rcut), _xmin(xmin), _xmax(xmax),
                             _ymin(ymin), _ymax(ymax),
                             _zmin(zmin), _zmax(zmax) 
        {}

        double GetDistance(const Atom *a1, const Atom *a2) override {
            dr = a2->position - a1->position;
            return sqrt(blitz::dot(dr, dr));
        }

        double GetOldDistance(const Atom *a1, const Atom *a2) override {
            dr = a2->old_position - a1->old_position;
            return sqrt(blitz::dot(dr, dr));
        }

        double GetDistanceThreadSafe(const Atom *a1, const Atom *a2) override {
            Vec3D dr = a2->position - a1->position;
            return sqrt(blitz::dot(dr, dr));
        }

        double GetOldDistanceThreadSafe(const Atom *a1, const Atom *a2) override {
            Vec3D dr = a2->old_position - a1->old_position;
            return sqrt(blitz::dot(dr, dr));
        }

        double GetDistance(Vec3D &dr) override {
            return sqrt(blitz::dot(dr, dr));
        }

        void MoveAtom(Atom *a, Vec3D &tr) override {
            pos = a->position + tr;

            if(pos(0) > _xmax) pos(0) = _xmax;
            if(pos(1) > _ymax) pos(1) = _ymax;
            if(pos(2) > _zmax) pos(2) = _zmax;

            if(pos(0) < _xmin) pos(0) = _xmin;
            if(pos(1) < _ymin) pos(1) = _ymin;
            if(pos(2) < _zmin) pos(2) = _zmin;

            a->position = pos;
        }

        void Correction(Vec3D &pos) override {
            if(pos(0) > _xmax) pos(0) = _xmax;
            if(pos(1) > _ymax) pos(1) = _ymax;
            if(pos(2) > _zmax) pos(2) = _zmax;

            if(pos(0) < _xmin) pos(0) = _xmin;
            if(pos(1) < _ymin) pos(1) = _ymin;
            if(pos(2) < _zmin) pos(2) = _zmin;
        }

        void PrintInfo() override {
            std::cout << "Box Geometry" << std::endl;
            std::cout << "Cutting radius: " << _rcut << std::endl;
            std::cout << "Xmin:" << _xmin;
            std::cout << " Xmax:" << _xmax << std::endl;
            std::cout << "Ymin:" << _ymin;
            std::cout << " Ymax:" << _ymax << std::endl;
            std::cout << "Zmin:" << _zmin;
            std::cout << " Zmax:" << _zmax << std::endl;
        }
    };
}