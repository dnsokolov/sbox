#pragma once

#include "core/atom.hpp"

namespace sbox {
    class IGeometry {
        protected:
        double _rcut;

        public:
        IGeometry(double rcut) {
            _rcut = rcut;
        }

        double GetRcut() {
            return _rcut;
        }

        virtual double GetDistance(const Atom *a1, const Atom *a2) = 0;
        virtual double GetDistance(Vec3D &deltar) = 0;
        virtual double GetDistanceThreadSafe(const Atom *a1, const Atom *a2) = 0;
        virtual double GetOldDistance(const Atom *a1, const Atom *a2) = 0;
        virtual double GetOldDistanceThreadSafe(const Atom *a1, const Atom *a2) = 0;
        virtual void MoveAtom(Atom *a, Vec3D &tr) = 0;
        virtual void Correction(Vec3D &vec) = 0;
        virtual void PrintInfo() = 0;
    };
}