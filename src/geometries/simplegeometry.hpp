#pragma once

#include "igeometry.hpp"

namespace sbox {
    class SimpleGeometry : public IGeometry {
        private:
        Vec3D _dr;

        public:
        SimpleGeometry(double rcut) : IGeometry(rcut) {}

        double GetDistance(const Atom *a1, const Atom *a2) override {
            _dr = a2->position - a1->position;
            return sqrt(blitz::dot(_dr, _dr));
        }

        double GetOldDistance(const Atom *a1, const Atom *a2) override {
            _dr = a2->old_position - a1->old_position;
            return sqrt(blitz::dot(_dr, _dr));
        }

        double GetDistanceThreadSafe(const Atom *a1, const Atom *a2) override {
            Vec3D dr = a2->position - a1->position;
            return sqrt(blitz::dot(dr, dr));
        }

        double GetOldDistanceThreadSafe(const Atom *a1, const Atom *a2) override {
            Vec3D dr = a2->old_position - a1->old_position;
            return sqrt(blitz::dot(dr, dr));
        }

        double GetDistance(Vec3D &dr) override {
            return sqrt(blitz::dot(dr, dr));
        }

        void MoveAtom(Atom *a, Vec3D &tr) override {
            a->position += tr;
        } 

        void Correction(Vec3D &vec) override {}

        void PrintInfo() override {
            std::cout << "Simple Geometry" << std::endl;
            std::cout << "Cutting radius: " << _rcut << std::endl;
        }
    };
}