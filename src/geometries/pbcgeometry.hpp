#pragma once

#include "igeometry.hpp"

namespace sbox {
    class PBCGeometry : public IGeometry {
        private:
        double _size_x, _size_y, _size_z;
        double _size_x_half, _size_y_half, _size_z_half;
        double _minus_size_x_half, _minus_size_y_half, _minus_size_z_half;
        Vec3D pos, dr;
        bool _pbc_x, _pbc_y, _pbc_z;

        public:
        PBCGeometry(double rcut, double size_x, double size_y, double size_z) :
           IGeometry(rcut),
           _size_x(size_x), _size_y(size_y), _size_z(size_z),
           _size_x_half(size_x * 0.5), _size_y_half(size_y * 0.5), _size_z_half(size_z * 0.5),
           _minus_size_x_half(-size_x * 0.5), _minus_size_y_half(-size_y * 0.5), _minus_size_z_half(-size_z * 0.5), 
           _pbc_x(size_x > 0), _pbc_y(size_y > 0), _pbc_z(size_z > 0) {}

        double GetDistance(const Atom *a1, const Atom *a2) override {
            dr = a2->position - a1->position;

            if(_size_x > 0) {
                if(dr(0) > _size_x_half) dr(0) -= _size_x;
                if(dr(0) <= _minus_size_x_half) dr(0) += _size_x;
            }

            if(_size_y > 0) {
                if(dr(1) > _size_y_half) dr(1) -= _size_y;
                if(dr(1) <= _minus_size_y_half) dr(1) += _size_y;
            }

            if(_size_z > 0) {
                if(dr(2) > _size_z_half) dr(2) -= _size_z;
                if(dr(2) <= _minus_size_z_half) dr(2) += _size_z;
            }

            return sqrt(blitz::dot(dr, dr));
        }

        double GetOldDistance(const Atom *a1, const Atom *a2) override {
            dr = a2->old_position - a1->old_position;

            if(_size_x > 0) {
                if(dr(0) > _size_x_half) dr(0) -= _size_x;
                if(dr(0) <= _minus_size_x_half) dr(0) += _size_x;
            }

            if(_size_y > 0) {
                if(dr(1) > _size_y_half) dr(1) -= _size_y;
                if(dr(1) <= _minus_size_y_half) dr(1) += _size_y;
            }

            if(_size_z > 0) {
                if(dr(2) > _size_z_half) dr(2) -= _size_z;
                if(dr(2) <= _minus_size_z_half) dr(2) += _size_z;
            }

            return sqrt(blitz::dot(dr, dr));
        }

        double GetDistanceThreadSafe(const Atom *a1, const Atom *a2) override {
            Vec3D dr = a2->position - a1->position;

            if(_size_x > 0) {
                if(dr(0) > _size_x_half) dr(0) -= _size_x;
                if(dr(0) <= _minus_size_x_half) dr(0) += _size_x;
            }

            if(_size_y > 0) {
                if(dr(1) > _size_y_half) dr(1) -= _size_y;
                if(dr(1) <= _minus_size_y_half) dr(1) += _size_y;
            }

            if(_size_z > 0) {
                if(dr(2) > _size_z_half) dr(2) -= _size_z;
                if(dr(2) <= _minus_size_z_half) dr(2) += _size_z;
            }

            return sqrt(blitz::dot(dr, dr));
        }

        double GetOldDistanceThreadSafe(const Atom *a1, const Atom *a2) override {
            Vec3D dr = a2->old_position - a1->old_position;

            if(_size_x > 0) {
                if(dr(0) > _size_x_half) dr(0) -= _size_x;
                if(dr(0) <= _minus_size_x_half) dr(0) += _size_x;
            }

            if(_size_y > 0) {
                if(dr(1) > _size_y_half) dr(1) -= _size_y;
                if(dr(1) <= _minus_size_y_half) dr(1) += _size_y;
            }

            if(_size_z > 0) {
                if(dr(2) > _size_z_half) dr(2) -= _size_z;
                if(dr(2) <= _minus_size_z_half) dr(2) += _size_z;
            }

            return sqrt(blitz::dot(dr, dr));
        }

        double GetDistance(Vec3D &dr) override {
            if(_size_x > 0) {
                if(dr(0) > _size_x_half) dr(0) -= _size_x;
                if(dr(0) <= _minus_size_x_half) dr(0) += _size_x;
            }

            if(_size_y > 0) {
                if(dr(1) > _size_y_half) dr(1) -= _size_y;
                if(dr(1) <= _minus_size_y_half) dr(1) += _size_y;
            }

            if(_size_z > 0) {
                if(dr(2) > _size_z_half) dr(2) -= _size_z;
                if(dr(2) <= _minus_size_z_half) dr(2) += _size_z;
            }

            return sqrt(blitz::dot(dr, dr));
        }

        void MoveAtom(Atom *a, Vec3D &tr) override {
            pos = a->position + tr;

            if(_pbc_x) {
                if(pos(0) < _minus_size_x_half) pos(0) += _size_x;
                if(pos(0) >= _size_x_half) pos(0) -= _size_x;
            }

            if(_pbc_y) {
                if(pos(1) < _minus_size_y_half) pos(1) += _size_y;
                if(pos(1) >= _size_y_half) pos(1) -= _size_y;
            }

            if(_pbc_z) {
                if(pos(2) < _minus_size_z_half) pos(2) += _size_z;
                if(pos(2) >= _size_z_half) pos(2) -= _size_z;
            }

            a->position = pos;
        }

        void Correction(Vec3D &pos) override {
            if(_pbc_x) {
                if(pos(0) < _minus_size_x_half) pos(0) += _size_x;
                if(pos(0) >= _size_x_half) pos(0) -= _size_x;
            }

            if(_pbc_y) {
                if(pos(1) < _minus_size_y_half) pos(1) += _size_y;
                if(pos(1) >= _size_y_half) pos(1) -= _size_y;
            }

            if(_pbc_z) {
                if(pos(2) < _minus_size_z_half) pos(2) += _size_z;
                if(pos(2) >= _size_z_half) pos(2) -= _size_z;
            }
        }

         void PrintInfo() override {
            std::cout << "PBC Geometry" << std::endl;
            std::cout << "Cutting radius: " << _rcut << std::endl;
            
            if(_pbc_x) {
                std::cout << "pbc x is enabled" << std::endl;
                std::cout << "size_x:     " << _size_x << std::endl;
            }
            else {
                std::cout << "pbc x is disabled" << std::endl;
            }

            if(_pbc_y) {
                std::cout << "pbc y is enabled" << std::endl;
                std::cout << "size_y:     " << _size_y << std::endl;
            }
            else {
                std::cout << "pbc y is disabled" << std::endl;
            }

            if(_pbc_z) {
                std::cout << "pbc z is enabled" << std::endl;
                std::cout << "size_z:     " << _size_z << std::endl;
            }
            else {
                std::cout << "pbc z is disabled" << std::endl;
            }
        }
    };
}