#pragma once

#include "isampler.hpp"
#include "integrators/iintegrator.hpp"
#include "randomizers/maxwellrandomizer.hpp"

namespace sbox {
    class HMCSampler : public ISampler {
        double _E1, _E2, _p, _kT, _temperature;

        IPotential::ICalculationStrategy *_strategy = nullptr;
        IIntegrator *_integrator = nullptr;

        public:
        HMCSampler(Configuration *config, IRandomizer *randomizer, IPotential *potential, IIntegrator *integrator, bool continuation) :
           ISampler(config, randomizer, potential, continuation) {
               _strategy = potential->GetStrategy();
               _integrator = integrator;
           }   

        void InitRun(double temperature) override {
            _randomizer->SetCurrentTemperature(temperature);
            _integrator->temperature = temperature;
            _E1 = _strategy->GetFullEnergy();
            _kT = temperature * k_boltzmann;
            _temperature = temperature;
          //  _randomizer->SetRandomVelocities();
        }

        void Update() override {
             _config->SaveState();

            for(size_t i = 0; i < integrator_steps; ++i) {
                _integrator->Update();
            }

            _E2 = _strategy->GetFullEnergy();

            if(_E2 > _E1) {
                _p = exp((_E1 - _E2)/(_kT));
                if(genrand_close_open() > _p) {
                    _config->BackToState();
                    _E2 = _E1;     
                }
            }

            _E1 = _E2;

            _randomizer->SetRandomVelocities();
            _integrator->Renew();
        }

        void UpdateZero() override {
             _config->SaveState();

            for(size_t i = 0; i < integrator_steps; ++i) {
                _integrator->Update();
            }
            
            _E2 = _strategy->GetFullEnergy();
                
            if(_E2 > _E1) {
                _config->BackToState();
                _E2 = _E1; 
                 
            }

            _E1 = _E2;
            _randomizer->SetRandomVelocities();
            _integrator->Renew(); 
        }

        void PrintInfo() override {
            std::cout << "Hamiltonian Monte-Carlo Algorithm" << std::endl;
            std::cout << "Total steps:          " << total_steps << std::endl;
            std::cout << "Relaxing steps:       " << relaxing_steps << std::endl;
            std::cout << "Integrator steps:     " << integrator_steps << std::endl;
            std::cout << "Make dumping every:   " << dump_every << std::endl;
            std::cout << "Make averaging every: " << aver_every << std::endl;
            std::cout << "Dumping file:         " << dump_file << std::endl;
            std::cout << "Averaging file:       " << aver_file << std::endl;
            std::cout << "Working directory:    " << std::filesystem::current_path() << std::endl;
        }
    };
}