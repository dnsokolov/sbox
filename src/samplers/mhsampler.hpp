#pragma once

#include "samplers/isampler.hpp"

namespace sbox {

    class MHSampler : public ISampler {     
        double _E1, _E2, _p, _kT;
        Atom *_atom = nullptr;
        std::vector<Atom*> _neighbours;
        std::vector<Atom*>::iterator _neighbours_end;
        Vec3D _tr;
        IPotential::ICalculationStrategy *_strategy;

        public:
        MHSampler(Configuration *config, IRandomizer *randomizer, IPotential *potential, bool continuation) :
           ISampler(config, randomizer, potential, continuation) {
               _strategy = potential->GetStrategy();
           }   

        void InitRun(double temperature) override {
            _randomizer->SetCurrentTemperature(temperature);
            _E1 = _strategy->GetFullEnergy();
            _kT = temperature * k_boltzmann;
        }

        void Update() override {
            _neighbours.clear();
            _randomizer->GetRandomNoFrozenAtom(&_atom);
            _potential->GetNeighbours(_atom, _neighbours);
            _atom->SetOldConfig();
            _randomizer->GetRandomTranslation(_tr);
            _geometry->MoveAtom(_atom, _tr);
            _E2 = _strategy->GetFullEnergy(_atom, _neighbours);

            if(_E2 > _E1) {
                _p = exp((_E1 - _E2)/(_kT));
                if(genrand_close_open() > _p) {
                    _potential->RejectConfig(_atom, _neighbours);
                    _E2 = _E1;
                }
            }

            _E1 = _E2;
        }

        void UpdateZero() override {
            _neighbours.clear();
            _randomizer->GetRandomNoFrozenAtom(&_atom);
            _potential->GetNeighbours(_atom, _neighbours); 
            _atom->SetOldConfig();    
            _randomizer->GetRandomTranslation(_tr);
            _geometry->MoveAtom(_atom, _tr);
            
            _E2 = _strategy->GetFullEnergy(_atom, _neighbours);
                
            if(_E2 > _E1) {
                _potential->RejectConfig(_atom, _neighbours);
                _E2 = _E1;
                
            }

            _E1 = _E2;
        }

        void PrintInfo() override {
            std::cout << "Metropolis-Hastings Algorithm" << std::endl;
            std::cout << "Total steps:          " << total_steps << std::endl;
            std::cout << "Relaxing steps:       " << relaxing_steps << std::endl;
            std::cout << "Make dumping every:   " << dump_every << std::endl;
            std::cout << "Make averaging every: " << aver_every << std::endl;
            std::cout << "Dumping file:         " << dump_file << std::endl;
            std::cout << "Averaging file:       " << aver_file << std::endl;
            std::cout << "Working directory:    " << std::filesystem::current_path() << std::endl;
        }
    };
}