#pragma once

#include "isampler.hpp"
#include "integrators/iintegrator.hpp"
#include "randomizers/maxwellrandomizer.hpp"

namespace sbox {
    class MDSampler : public ISampler {
        private:
        IIntegrator *_integrator;
        size_t _current_step;
        double _time, _dt;
        double _Ekin, _Epot, _Efull;
        std::vector<double> _masses;
        uint32_t _num_of_atoms;

        public:
        MDSampler(Configuration *config, IRandomizer *randomizer, IPotential *potential, 
                  IIntegrator *integrator, bool continuation) :
           ISampler(config, randomizer, potential, continuation) {
               _integrator = integrator;   
               _dt = integrator->Get_dt();
               _time = _current_step * _dt;
               auto mol_masses = config->GetMolMasses();
               auto mol_masses_end = mol_masses.end();
               _num_of_atoms = config->GetNumOfAtoms();

               for(auto i = mol_masses.begin(); i != mol_masses_end; ++i) {
                   _masses.push_back((*i) * g_per_mol_to_eV_ps2_A2);
               }

               potential->GetForces();
        }

       /* void SetRandomVelocities(double temperature) {
            _randomizer->SetCurrentTemperature(temperature);
            auto end = _atoms.end();
            Vec3D velocity;
            Vec3D momentum = 0.0, rcms = 0.0;
            double mass = 0.0;
            size_t indx = 0;
            auto mol_masses = _config->GetMolMasses();
            MaxwellRandomizer *mxrnd = new MaxwellRandomizer(1,_randomizer->GetSeed(), mol_masses, _atoms);  

            for(auto i = _atoms.begin(); i != end; ++i) {
                mxrnd->SetCurrentAtom(*i);
                mxrnd->GetRandomTranslation(velocity);
                (*i)->velocity = velocity;
            }

            delete mxrnd;
        }*/

        /*void Run(double temperature) override {
            _integrator->temperature = temperature;

            if(std::filesystem::exists(dump_file)) {
                   std::filesystem::remove(dump_file);
               }

               if(std::filesystem::exists(aver_file)) {
                   std::filesystem::remove(aver_file);
               }
            
            std::cout << "Start relaxing for T = " << temperature << " K" << std::endl;
            for(;_current_step < relaxing_steps; ++_current_step) {
                if((_current_step + 1) % dump_every == 0) {
                    _Efull = _potential->GetFullEnergy(_Ekin, _Epot);
                    _config->properties["temperature"] = as_string(temperature);
                    _config->properties["Ekin_temperature"] = as_string(energy_to_temperature(_Ekin, _num_of_atoms));
                    _config->properties["step"] = as_string(_current_step);
                    _config->properties["time"] = as_string(_time);
                    _config->properties["Ekin"] = as_string(_Ekin);
                    _config->properties["Epot"] = as_string(_Epot);
                    _config->properties["Efull"] = as_string(_Efull);

                    _config->RefreshComment(true, true);
                    if(dump_file != "") _config->SaveToFile(dump_file, true);
                    _config->SaveToFile(last_xyz);
                }

                _integrator->Update();
                _time += _dt;
            }

            std::cout << "Start averaging for T = " << temperature << " K" << std::endl;
            for(;_current_step < total_steps; ++_current_step) {
                if((_current_step + 1) % dump_every == 0) {
                    _Efull = _potential->GetFullEnergy(_Ekin, _Epot);
                    _config->properties["temperature"] = as_string(temperature);
                    _config->properties["Ekin_temperature"] = as_string(energy_to_temperature(_Ekin, _num_of_atoms));
                    _config->properties["step"] = as_string(_current_step);
                    _config->properties["time"] = as_string(_time);
                    _config->properties["Ekin"] = as_string(_Ekin);
                    _config->properties["Epot"] = as_string(_Epot);
                    _config->properties["Efull"] = as_string(_Efull);
                    _config->RefreshComment(true, true);
                    if(dump_file != "") _config->SaveToFile(dump_file, true);
                    _config->SaveToFile(last_xyz);
                }

                if((_current_step + 1) % aver_every == 0 && aver_file != "") {
                    _Efull = _potential->GetFullEnergy(_Ekin, _Epot);
                    _config->properties["temperature"] = as_string(temperature);
                    _config->properties["Ekin_temperature"] = as_string(energy_to_temperature(_Ekin, _num_of_atoms));
                    _config->properties["mcstep"] = as_string(_current_step);
                    _config->properties["time"] = as_string(_time);
                    _config->properties["Ekin"] = as_string(_Ekin);
                    _config->properties["Epot"] = as_string(_Epot);
                    _config->properties["Efull"] = as_string(_Efull);
                    _config->RefreshComment(true, true);
                    _config->SaveToFile(aver_file, true);
                }
                
                _integrator->Update();
                _time += _dt;
            }

            _current_step = 0;
        }*/
        
        void InitRun(double temperature) override {

        }

        void Update() override {
            _integrator->Update();
            _time += _dt;
        }

        void UpdateZero() override {
            _integrator->Update();
            _time += _dt;
        }

        void PrintInfo() override {
            std::cout << "Molecular Dynamics Sampler" << std::endl;
        }

    };
}