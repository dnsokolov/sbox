#pragma once

#include "core/configuration.hpp"
#include "randomizers/irandomizer.hpp"
#include "potentials/ipotential.hpp"

namespace sbox {
    class ISampler : public Configuration::IRemoveAtomHandler {
        protected:
        Configuration *_config = nullptr;
        std::vector<Atom*> _atoms;
        size_t _num_of_atoms;
        IRandomizer *_randomizer = nullptr;
        IPotential *_potential = nullptr;
        IGeometry *_geometry = nullptr;
        uint32_t _current_step = 0;
        bool _continuation = false;

        public:
        ISampler(Configuration *config, IRandomizer *randomizer, IPotential *potential, bool continuation) {
            ASSERT((_config = config) != nullptr);
            ASSERT((_randomizer = randomizer) != nullptr);
            ASSERT((_geometry = config->GetGeometry()) != nullptr);
            ASSERT((_potential = potential) != nullptr);

            _potential = potential;

            _atoms = config->GetAtoms();
            _num_of_atoms = _atoms.size();
            
            _continuation = continuation;

            if(continuation) {
                   if(config->ContainsProperty("step")) {
                       _current_step = as<uint32_t>(config->properties["step"]) + 1;
                       return;
                    }

                   if(config->ContainsProperty("mcstep")) {
                       _current_step = as<uint32_t>(config->properties["mcstep"]) + 1;
                       return;
                    }
            }

        }

        void CallRemoveAtomHandler(Configuration &config, Atom *atom) override {
            _atoms = config.GetAtoms();
            _num_of_atoms = _atoms.size();
        }

        uint32_t total_steps = 60000;
        uint32_t relaxing_steps = 50000;
        uint32_t integrator_steps = 10;
        uint32_t dump_every = 1000;
        uint32_t aver_every = 1000;
        std::string dump_file = "";
        std::string aver_file = "";
        std::string last_xyz = "_last_xyz";
        double inter_atomic_dist = 0.0;

        void SetParam(const std::string &param, const std::string &value) {
            if(param == "total_steps") total_steps = as<uint32_t>(value);
            if(param == "relaxing_steps") relaxing_steps = as<uint32_t>(value);
            if(param == "integrator_steps") integrator_steps = as<uint32_t>(value);
            if(param == "dump_every") dump_every = as<uint32_t>(value);
            if(param == "aver_every") aver_every = as<uint32_t>(value);

            if(param == "dump_file") {
               dump_file = value;
               if(!_continuation && std::filesystem::exists(dump_file)) {
                   std::filesystem::remove(dump_file);
                   std::cout << "File '" << dump_file << "' is deleted" << std::endl;
               }
            }
            if(param == "aver_file") {
                aver_file = value;
                if(!_continuation && std::filesystem::exists(aver_file)) {
                    std::filesystem::remove(aver_file);
                    std::cout << "File '" << aver_file << "' is deleted" << std::endl;
                }
            }

            if(param == "last_xyz") last_xyz = value;
            if(param == "inter_atomic_dist") inter_atomic_dist = as<double>(value);
        }

        virtual void Update() = 0;
        virtual void UpdateZero() = 0;
        virtual void InitRun(double temperature) = 0;
        virtual void PrintInfo() = 0;

        void Run(double temperature) {
            InitRun(temperature);

            std::function<void()> update = (temperature <= 0.0) ? std::bind(&ISampler::UpdateZero, this) : std::bind(&ISampler::Update, this);

            std::cout << "Start relaxing for  (T = " << temperature << " K)" << std::endl;
            _config->properties["temperature"] = as_string(temperature);
            
            for(;_current_step < relaxing_steps; ++_current_step) {
                
                update();

                if((_current_step + 1) % dump_every == 0) {
                   // _config->properties["temperature"] = as_string(temperature);
                    _config->properties["step"] = as_string(_current_step);
                    _config->RefreshComment(true, true);
                    if(dump_file != "") _config->SaveToFile(dump_file, true);
                    _config->SaveToFile(last_xyz);
                }
            }

            std::cout << "Start averaging for (T = " << temperature << " K)" << std::endl;
            _config->properties["temperature"] = as_string(temperature);

            for(;_current_step < total_steps; ++_current_step) {
                
                update();

                if((_current_step + 1) % dump_every == 0) {
                   // _config->properties["temperature"] = as_string(temperature);
                    _config->properties["step"] = as_string(_current_step);
                    _config->RefreshComment(true, true);
                    if(dump_file != "") _config->SaveToFile(dump_file, true);
                    _config->SaveToFile(last_xyz);
                }

                if((_current_step + 1) % aver_every == 0 && aver_file != "") {
                    if(aver_file == "") continue;
                    //_config->properties["temperature"] = as_string(temperature);
                    _config->properties["step"] = as_string(_current_step);
                    _config->RefreshComment(true, true);
                    _config->SaveToFile(aver_file, true);
                }
            }

            _current_step = 0;     
        }
    };
}