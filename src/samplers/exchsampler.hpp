#pragma once

#include "isampler.hpp"

namespace sbox {
    class ExchSampler : public ISampler {
        private:
        uint32_t current_step = 0;
        double _E1, _E2, _p, _kT;
        Atom *_atom1 = nullptr, *_atom2 = nullptr;
        std::vector<Atom*> _neighbours1, _neighbours_table1;
        bool _recal_flag = false;

        public:
        ExchSampler(Configuration *config, IRandomizer *randomizer, IPotential *potential, bool continuation) :
           ISampler(config, randomizer, potential, continuation) {
               if(config->GetNumOfTypes() < 2) {
                   std::cerr << "This sampler works only for multicomponent systems" << std::endl;
                   exit(1);
               }

               if(continuation) {
                   if(config->ContainsProperty("mcstep")) {
                       current_step = as<uint32_t>(config->properties["mcstep"]) + 1;
                    }
               }
        }   
        
        void InitRun(double temperature) override {
            _randomizer->SetCurrentTemperature(temperature);
            _E1 = _potential->GetFullEnergy();
            _kT = temperature * k_boltzmann;
        }

        void Update() override {
            _randomizer->GetRandomAtom(&_atom1);
            _atom1->SetOldConfig();
            if(!_config->GetNeigboursAnotherTypes(_atom1, _neighbours1, inter_atomic_dist)) return;
            _atom2 = _randomizer->GetRandomAtom(_neighbours1);
            _atom2->SetOldConfig();

            _atom1->position = _atom2->position;
            _atom2->position = _atom1->old_position;

            _E2 = _potential->GetFullEnergy();
                
            if(_E2 > _E1) {
                _p = exp((_E1 - _E2)/(_kT));

                if(genrand_close_open() > _p) {
                    _atom1->BackToOldConfig();
                    _atom2->BackToOldConfig();
                    _E2 = _E1;
                    _recal_flag = true;
                }
                else {
                    _recal_flag = false;
                }
            }

            _E1 = _E2;
        }

        void UpdateZero() override {
            _randomizer->GetRandomAtom(&_atom1);
            _atom1->SetOldConfig();
            if(!_config->GetNeigboursAnotherTypes(_atom1, _neighbours1, inter_atomic_dist)) return;
            _atom2 = _randomizer->GetRandomAtom(_neighbours1);
            _atom2->SetOldConfig();

            _atom1->position = _atom2->position;
            _atom2->position = _atom1->old_position;

            _E2 = _potential->GetFullEnergy();
                
            if(_E2 > _E1) {
                _atom1->BackToOldConfig(); 
                _atom2->BackToOldConfig();
                _E2 = _E1;
                _recal_flag = true;
            }
            else {
                _recal_flag = false;
            }

            _E1 = _E2;
        }

        void PrintInfo() override {
            std::cout << "Exchange Atoms Metropolis-Hastings Algorithm" << std::endl;
            std::cout << "Total steps:          " << total_steps << std::endl;
            std::cout << "Relaxing steps:       " << relaxing_steps << std::endl;
            std::cout << "Make dumping every:   " << dump_every << std::endl;
            std::cout << "Make averaging every: " << aver_every << std::endl;
            std::cout << "Dumping file:         " << dump_file << std::endl;
            std::cout << "Averaging file:       " << aver_file << std::endl;
            std::cout << "Inter atomic dist:    " << inter_atomic_dist << std::endl;
            std::cout << "Working directory:    " << std::filesystem::current_path() << std::endl;
        }
    };
}