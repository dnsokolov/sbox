#ifndef _FORMAT_H_
#define _FORMAT_H_

#include <string>
#include "core/configuration.hpp"

namespace sbox {
class Configuration;
class IFormat {
    public:
    virtual void Read(std::istream &instream, Configuration &config) = 0;
    virtual void Write(Configuration &config, std::ostream &outstream) = 0; 
};
}

#endif