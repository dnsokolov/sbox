#include "xyzformat.hpp"
#include "core/utils.hpp"
#include "core/atom.hpp"
#include <sstream>

namespace sbox {

void XYZFormat::ParseComment(Configuration &config, std::vector<std::string> &columns) {
    std::string comment = config.comment;
    size_t pos_start = comment.find('[');
    size_t pos_finish = comment.find(']');

    if(pos_start != std::string::npos && pos_finish != std::string::npos){
        size_t count = pos_finish - pos_start - 1;
        comment = comment.substr(pos_start + 1, count);

        std::vector<std::string> tokens;
        split(comment, tokens, ' ');

        auto end = tokens.end();
        for(auto i = tokens.begin(); i != end; ++i) {
            std::vector<std::string> keyvalue;
            split(*i, keyvalue, ':', 2);
            if(keyvalue.size() == 2) {
                if(keyvalue[0] != "columns") {
                    config.properties[keyvalue[0]] = keyvalue[1];
                }
                else {
                    split(keyvalue[1], columns, ',');
                }
            }
        }
    }
}

void XYZFormat::ParseColumns(const Configuration &config, std::vector<std::string> &columns) {
    std::string comment = config.comment;
    size_t pos_start = comment.find('[');
    size_t pos_finish = comment.find(']');

    if(pos_start != std::string::npos && pos_finish != std::string::npos){
        size_t count = pos_finish - pos_start - 1;
        comment = comment.substr(pos_start + 1, count);

        std::vector<std::string> tokens;
        split(comment, tokens, ' ');

        auto end = tokens.end();
        for(auto i = tokens.begin(); i != end; ++i) {
            std::vector<std::string> keyvalue;
            split(*i, keyvalue, ':', 2);
            if(keyvalue.size() == 2) {
                if(keyvalue[0] == "columns") {
                    split(keyvalue[1], columns, ',');
                    return;
                }
            }
        }
    }
}

void XYZFormat::Read(std::istream &instream, Configuration &config) {
    std::stringstream framestream;
    std::string last_comment = "";

    size_t num_of_atoms;
    int current_frame = 0;
    std::string line;

    instream >> num_of_atoms;
    std::getline(instream, line);
    std::getline(instream, line);
    framestream << num_of_atoms << std::endl;
    framestream << line << std::endl;
    last_comment = line;
    
    while(true) {
        if(frame >= 0 && current_frame != frame) {
            if(instream.eof()) return;

            for(int i = 0; i < num_of_atoms; ++i) {
                std::getline(instream, line);
            }
            
            instream >> num_of_atoms;
            std::getline(instream, line);
            std::getline(instream, line);
            last_comment = line;

            ++current_frame;
            continue;
        }

        if(frame >= 0 && current_frame == frame) {
            config.comment = last_comment;
            std::vector<std::string> columns;
            XYZFormat::ParseComment(config, columns);
            std::string ename;
            double x, y, z;

            for(int i = 0; i < num_of_atoms; ++i) {
                std::getline(instream, line);
                std::stringstream line_stream(line);
                line_stream >> ename >> x >> y >> z;
                auto end = columns.end();

                double energy = 0.0;
                int freeze = 0;

                for(auto it = columns.begin(); it != end; ++it) {
                    if(*it == "Energy") {
                        line_stream >> energy;
                        continue;
                    }

                    if(*it == "Freeze") {
                        line_stream >> freeze;
                        continue;
                    }

                    std::string value;
                    line_stream >> value;
                    config.columns[*it].push_back(value);
                }

                config.AddAtom(x, y, z, energy, ename, freeze);
            }
            return;
        }

        for(int i = 0; i < num_of_atoms; ++i) {
            std::getline(instream, line);
            framestream << line << std::endl;
        }

        instream >> num_of_atoms;
        std::getline(instream, line);
        std::getline(instream, line);
        if(instream.eof() || line == "") break;

        framestream.str(std::string());
        framestream.clear();

        framestream << num_of_atoms << std::endl;
        framestream << line << std::endl; 
    }

    XYZFormat xyz(0);
    xyz.Read(framestream, config);
}

void XYZFormat::Write(Configuration &config, std::ostream &outstream) {
    Atom *atom;

    size_t num_of_atoms = config.GetNumOfAtoms();

    outstream << num_of_atoms << std::endl;
    outstream << config.comment << std::endl;

    std::vector<std::string> columns;
    XYZFormat::ParseColumns(config, columns);

    for(size_t i = 0; i < num_of_atoms; ++i) {
        atom = config.GetAtom(i);
        outstream << atom->symbol << " " << atom->position[0] << " " 
                                         << atom->position[1] << " " 
                                         << atom->position[2]; 
        size_t c = 0;

        if(columns.size() > 0) {
            for(auto const &column : columns) {
                if(column == "Energy") {
                    outstream << " " << atom->energy;
                    continue;
                }

                if(column == "Freeze") {
                    outstream << " " << atom->frozen;
                    continue;
                }
                 
                if(c >= atom->column_values.size()) continue;
                outstream << " " << atom->column_values[c];
                ++c;
            }
        }

        outstream << std::endl;
    }
}

size_t XYZFormat::GetNumFrames(std::istream &instream) {
    size_t num_of_atoms;
    size_t num_of_frames = 0;
    instream >> num_of_atoms;
    std::string line;
    std::getline(instream, line);
    std::getline(instream, line);

    while(!instream.eof()) {
        for(size_t i = 0; i < num_of_atoms; ++i) {
            std::getline(instream, line);
        }
        ++num_of_frames;
        instream >> num_of_atoms;
        std::getline(instream, line);
        std::getline(instream, line);
    }

    return num_of_frames;
}

size_t XYZFormat::GetNumFrames(const std::string &filename) {
    std::ifstream fin(filename);
    return XYZFormat::GetNumFrames(fin);
}

bool XYZFormat::NextFrame(std::istream &ins, Configuration &config) {
    size_t num_of_atoms;
    std::string line;

    ins >> num_of_atoms;
    std::getline(ins, line);
    std::getline(ins, line);

    if(ins.eof()) return false;
    config.comment = line;

    std::vector<std::string> columns;
    XYZFormat::ParseComment(config, columns);
    std::string ename;
    double x, y, z;

    for(int i = 0; i < num_of_atoms; ++i) {
        std::getline(ins, line);
        std::stringstream line_stream(line);
        line_stream >> ename >> x >> y >> z;
        auto end = columns.end();

        double energy = 0.0;
        int freeze = 0;

        for(auto it = columns.begin(); it != end; ++it) {
            if(*it == "Energy") {
                line_stream >> energy;
                continue;
            }

            if(*it == "Freeze") {
                line_stream >> freeze;
                continue;
            }

            std::string value;
            line_stream >> value;
            config.columns[*it].push_back(value);
        }

        config.AddAtom(x, y, z, energy, ename, freeze);
    }
    
    return true;
}

void XYZFormat::LoadFrames(std::istream &ins, std::vector<Configuration> &frames) {
    Configuration config;
    while(NextFrame(ins, config)) {
        frames.push_back(config);
        config.Clear();
    }
}

void XYZFormat::LoadFrames(const std::string &filename, std::vector<Configuration> &frames) {
    std::ifstream fs(filename);

    if(fs.is_open()) {
       LoadFrames(fs, frames);
    }
}

}