#ifndef _XYZFORMAT_H_
#define _XYZFORMAT_H_

#include "format.hpp"

namespace sbox {

class XYZFormat : public IFormat {
    int frame;

    public:
    XYZFormat(int frame = 0) {
        this->frame = frame;
    }

    void Read(std::istream &istream, Configuration &config) override;
    void Write(Configuration &config, std::ostream &outstream) override;
    static bool NextFrame(std::istream &ins, Configuration &config);
    static void LoadFrames(std::istream &ins, std::vector<Configuration> &frames);
    static void LoadFrames(const std::string &filename, std::vector<Configuration> &frames);

    static size_t GetNumFrames(std::istream &instream);
    static size_t GetNumFrames(const std::string &filename);
    
    static void ParseComment(Configuration &config, std::vector<std::string> &columns);
    static void ParseColumns(const Configuration &config, std::vector<std::string> &columns);
};
}

#endif