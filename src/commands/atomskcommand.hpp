#pragma once

#include "icommand.hpp"
#include "../core/atomskprocess.hpp"

namespace sbox {
   class AtomskCommand : public ICommand {
       public:
       void GetHelp() override {
            std::cout << "Launching atomsk REPL. This command is for testing atomsk in sbox." << std::endl;
            std::cout << "Enter quit for exit from REPL" << std::endl;
        }

        std::string GetName() override {
            return "--atomsk";
        }

        int Execute(std::vector<std::string> &args) override {
            AtomskProcess atomsk;
            if(atomsk.PathExists()) {
                std::string line;

                while (true) {
                    std::getline(std::cin, line);

                    if(line == "quit") {
                        break;
                    }

                    atomsk.DoString(line);
                }      
            }

            return 0;
        }
   };
}