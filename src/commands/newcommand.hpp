#pragma once

#include "icommand.hpp"
#include <iostream>
#include <filesystem>
#include <fstream>

namespace sbox {
    class NewCommand : public ICommand {
        private:
        std::vector<std::string> XYZfiles;
        std::string alloy_dat_file{""};
        bool alloy_flag{false};

        void LoadXYZFiles(bool ignore_atominfo = false) {
            std::filesystem::path path = std::filesystem::current_path();

            if(std::filesystem::exists("atominfo.toml") && !ignore_atominfo) {
                auto data = toml::parse("atominfo.toml");
                if(data.count("configs")) {
                     auto configs = toml::get<std::vector<std::string>>(data.at("configs"));
                     for(const auto &config: configs) {
                         auto nname = config;
                         auto ext = std::filesystem::path(config).extension().string();
                         if(ext == ".xyz" || ext == ".XYZ") {
                            if(ext == ".xyz") {
                                size_t last_index = nname.find_last_of(".");
                                nname = nname.substr(0, last_index) + ".XYZ";
                                std::filesystem::rename(config, nname);
                            }
                            XYZfiles.emplace_back(nname);
                         }
                     }
                     return;
                } 
            }

            for (const std::filesystem::path &entry : std::filesystem::directory_iterator(path)) {
                if(std::filesystem::is_regular_file(entry)) {
                    std::string ext = entry.extension().string();
                    if( ext == ".XYZ") {
                        XYZfiles.push_back(entry.filename().string());
                    }
                }
            }
        }

        void LoadAlloyDat() {
            if(std::filesystem::exists("alloy3_system.dat")) {
                alloy_dat_file = "alloy3_system.dat";
                return;
            }

            std::filesystem::path path = std::filesystem::current_path();
            for (const std::filesystem::path &entry : std::filesystem::directory_iterator(path)) {
                if(std::filesystem::is_regular_file(entry)) {
                    std::string ext = entry.extension().string();
                    if( ext == ".dat") {
                        std::string filename = entry.filename().string();
                        if(filename.find("alloy") != std::string::npos) {
                            alloy_dat_file = filename;
                            break;
                        }
                    }
                }
            }
        }

        void GenerateStarter() {
            std::ofstream starter_lua("starter.lua");
            starter_lua << "local serie = require \"serie\"" << std::endl << std::endl;
            starter_lua << "options = {}" << std::endl << std::endl;
            starter_lua << "--Files for copying" << std::endl;
            starter_lua << "options.files = {" << std::endl;

            if(alloy_dat_file != "") {
                std::string line = "   \"";
                line += (alloy_dat_file + "\",");
                starter_lua << line << std::endl;
            }

            if(std::filesystem::exists("atominfo.toml")) {
                starter_lua << "   \"atominfo.toml\"," << std::endl;
            }
            
            starter_lua << "   \"simexec.lua\"," << std::endl;
            starter_lua << "}" << std::endl << std::endl; 
            starter_lua << "--Starting configs must to have an extension '.XYZ'" << std::endl;
            starter_lua << "options.configs = {" << std::endl;
            for(auto &xyzf: XYZfiles) {
                starter_lua << "   \"" << xyzf << "\"," << std::endl; 
            }
            starter_lua << "}" << std::endl << std::endl;
            starter_lua << "options.start_with = 1 --try to create serie folder starting with defined number" << std::endl;
            starter_lua << "options.start_point = \"simexec.lua\" --script for starting simulation" << std::endl;
            starter_lua << "options.num_of_parallel_series = 1 --num of series running simultaneously" << std::endl;
            starter_lua << "options.num_of_total_series = 1 --num of total series" << std::endl << std::endl;
            starter_lua << "directory_lock()" << std::endl;
            starter_lua << "serie.execute(options)" << std::endl;
            starter_lua << "directory_unlock()" << std::endl;
            starter_lua.close();
        }

        void GenerateRestarter() {
           /* std::ofstream restarter_lua("restarter.lua");
            restarter_lua << "local serie = require \"serie\"" << std::endl << std::endl;
            restarter_lua << "serie.restart(\"simexec.lua\")" << std::endl;
            restarter_lua.close();*/
        }

        void GenerateSimExec() {
            std::ofstream simexec_lua("simexec.lua");
            simexec_lua << "--@redirect starter.lua" << std::endl << std::endl;
            simexec_lua << "local sim = require \"simulation\"" << std::endl << std::endl;
            simexec_lua << "params = {" << std::endl;
            simexec_lua << "   config_file = find_file('.XYZ')," << std::endl;

            if(alloy_dat_file != "") {
                std::string line{"   CE_dat_file = '"};
                line += (alloy_dat_file + "',");
                simexec_lua << line << std::endl << std::endl;
            }
            
            simexec_lua << "   geometry = {" << std::endl;
            simexec_lua << "      name = 'simple'," << std::endl;
            simexec_lua << "      rcut = 7.55," << std::endl;
            simexec_lua << "   }," << std::endl << std::endl;
            simexec_lua << "   randomizer = {" << std::endl;
            simexec_lua << "      name  = 'simple'," << std::endl;
            simexec_lua << "      drmax = 0.3," << std::endl;
            simexec_lua << "      seed  = -1," << std::endl;
            simexec_lua << "   }," << std::endl << std::endl;
            simexec_lua << "   integrator = {" << std::endl;
            simexec_lua << "      name = 'leapfrog'," << std::endl;
            simexec_lua << "      dt = 0.01," << std::endl;
           // simexec_lua << "      init_temperature = 300," << std::endl;
            simexec_lua << "   }," << std::endl << std::endl;
            simexec_lua << "   total_steps      = 100000," << std::endl;
            simexec_lua << "   relaxing_steps   = 90000," << std::endl;
            simexec_lua << "   integrator_steps = 10," << std::endl;
            simexec_lua << "   dump_every       = 1000,"  << std::endl;
            simexec_lua << "   aver_every       = 1000,"  << std::endl << std::endl;
            simexec_lua << "   continuation     = true," << std::endl;
            simexec_lua << "   num_of_threads   = 1," << std::endl;
            simexec_lua << "   sampler = 'MH'," << std::endl;
            simexec_lua << "}" << std::endl << std::endl;
            simexec_lua << "directory_lock()" << std::endl;
            simexec_lua << "sim.init(params)" << std::endl;
            simexec_lua << "print_info()" << std::endl << std::endl;
            simexec_lua << "--Enter your code here. For example:" << std::endl;
            simexec_lua << "run(300,1500,10)" << std::endl;
            simexec_lua << "--get_caloric_dat(params.aver_file, params.sys_name..'_caloric.dat') ---< If you want to get a caloric curve" << std::endl;
            simexec_lua << "directory_unlock()" << std::endl;
            simexec_lua << "free_all()" << std::endl<< std::endl; 
        }

        void GenerateBootOnExistingXYZ() {
            LoadXYZFiles(true);
            std::ofstream boot_lua("boot.lua");
            boot_lua << "local bootstrap = require \"bootstrap\"" << std::endl;
            boot_lua << "local chemdata = require \"chemdata\"" << std::endl << std::endl;
            boot_lua << "chemdata.init()" << std::endl;
            boot_lua << "bootstrap.init(chemdata)" << std::endl << std::endl;
            boot_lua << "local configs = {" << std::endl;

            for(const auto& f: XYZfiles) {
                boot_lua << "   \"" << f << "\"," << std::endl;
            }

            boot_lua << "}" << std::endl << std::endl;
            boot_lua << "bootstrap.generate_atominfo({" << std::endl;
            boot_lua << "   configs = configs," << std::endl;
            boot_lua << "   rcut = 6.5," << std::endl;
            boot_lua << "})" << std::endl << std::endl;
            boot_lua << "chemdata.free()" << std::endl; 
            boot_lua << "free_all()" << std::endl;
        }

        void GenerateBootTemplate() {
            std::ofstream boot_lua("boot.lua");
            boot_lua << "local bootstrap = require \"bootstrap\"" << std::endl;
            boot_lua << "local chemdata = require \"chemdata\"" << std::endl << std::endl;
            boot_lua << "atomsk_init()" << std::endl;
            boot_lua << "chemdata.init()" << std::endl;
            boot_lua << "bootstrap.init(chemdata)" << std::endl << std::endl;
            boot_lua << "local params = {" << std::endl;
            boot_lua << "   ename1 = \"Au\"," <<std::endl;
            boot_lua << "   target =\"Au.xyz\"," << std::endl;
            boot_lua << "   R = 6.0," << std::endl;
            boot_lua << "}" << std::endl << std::endl;
            boot_lua << "chemdata.fill_table(params)" << std::endl << std::endl;
            boot_lua << "atomsk_def(params)" << std::endl;
            boot_lua << "atomsk_do({" << std::endl;
            boot_lua << "   \"create \%lattype1\% \%a1\% \%ename1\%\"," << std::endl;
            boot_lua << "   \"duplicate \%n\% \%n\% \%n\%\"," << std::endl;
            boot_lua << "   \"select out sphere \%CB\% \%R\%\"," << std::endl;
            boot_lua << "   \"rmatom select\"," << std::endl;
            boot_lua << "   \"wr \%target\%\"," << std::endl;
            boot_lua << "   \"sleep 1000\"," << std::endl;
            boot_lua << "   \"exists \%target\%\"," << std::endl;
            boot_lua << "   \"exec ovito \%target\%\"," << std::endl;
            boot_lua << "})" << std::endl << std::endl;
            boot_lua << "bootstrap.generate_atominfo({" << std::endl;
            boot_lua << "   configs = {params.target}," << std::endl;
            boot_lua << "   rcut = 6.5," << std::endl;
            boot_lua << "})" << std::endl << std::endl;
            boot_lua << "chemdata.free()" << std::endl; 
            boot_lua << "free_all()" << std::endl;
        }
 
        void GenerateBoot(const std::vector<std::string> args) {
            std::string choice;

            if(args.size() > 1) choice = args[1];
            else {
                choice = "";
            }

            if(choice == "1" || choice == "xyz") {
                GenerateBootOnExistingXYZ();
                if(std::filesystem::exists("boot.lua")) {
                    std::cout << "boot.lua is generated" << std::endl;
                }
                return;
            }

            if(choice == "2" || choice == "") {
                GenerateBootTemplate();
                if(std::filesystem::exists("boot.lua")) {
                    std::cout << "boot.lua is generated" << std::endl;
                }
                return;
            }

            std::cout << "Error: unknown choice '" << choice << "'" << std::endl;

        }

        public:
        std::string GetName() override {
            return "--new";
        }

        void GetHelp() override {
            std::cout << "Generating a calculation project" << std::endl;
        }

        int Execute(std::vector<std::string> &args) override {
            if(args.size() >= 1) {
                if(args[0] == "boot") {
                    GenerateBoot(args);
                    return 0;
                }
            }

            bool flag = true;

            if(std::filesystem::exists("starter.lua")) {
                std::cout << "File 'starter.lua' exists already. Do you want to rewrite project (y/n)? [y]" << std::endl;
                char c = getchar();
                if(c == 'n' || c == 'N') flag = false;
            }

            if(!flag) {
                std::cout << "Process is cancled..." << std::endl;
                return 0;
            }

            LoadXYZFiles();
            LoadAlloyDat();

            GenerateStarter();
            GenerateRestarter();
            GenerateSimExec();

            std::cout << "Project is generated" << std::endl;

            return 0;
        }
    };
}