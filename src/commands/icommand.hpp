#pragma once

#include <string>
#include <vector>

namespace sbox {
    class ICommand {
        public:
        virtual int Execute(std::vector<std::string> &args) = 0;
        virtual void GetHelp() = 0;
        virtual std::string GetName() = 0;
    };
}