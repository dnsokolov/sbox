#pragma once

#include "core/luaengine.hpp"
#include "icommand.hpp"

namespace sbox {
    class LuaCommand: public ICommand {
        private:
        LuaEngine *_lua = nullptr;

        public:
        void SetLuaEngine(LuaEngine *lua) {
            _lua = lua;
        }

        void GetHelp() override {
            std::cout << "Launching lua REPL" << std::endl;
            std::cout << "Enter --exit for exit from REPL" << std::endl;
        }

        std::string GetName() override {
            return "--lua";
        }

        int Execute(std::vector<std::string> &args) override {
            if(args.size() >= 1) {
                std::string fname = args[0];
                std::cout << "Launch script '" << fname << "'" << std::endl;
                if(std::filesystem::exists(fname)) {
                    _lua->Execute(fname);
                }
                else {
                    std::cout << "Could not find file '" << fname << "'" << std::endl;
                }
            }

            std::string line;
            std::cout << "Lua REPL (enter --exit for exit)" << std::endl;

            while(true) {
                std::cout << "> ";
                std::getline(std::cin, line);
                if(line == "") continue;
                if(line == "--exit") return 0;

                _lua->ExecuteString(line);
            }

            return 0;
        }
    };
}