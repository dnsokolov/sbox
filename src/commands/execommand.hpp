#pragma once

#include "icommand.hpp"
#include "core/lightprocess.hpp"
#include <iostream>

namespace sbox {
    class ExeCommand: public ICommand {
        public:

        std::string GetName() override {
            return "--exe";
        }

        void GetHelp() override {
            std::cout << "Execute external program" << std::endl;
            std::cout << "Example1: --exe <progname> [params] - execute program <progname> with arguments [params]. Program will detached from sbox." << std::endl;
            std::cout << "Example2: --exe :<progname> [params] - execute program <progname> with arguments params. Program will attached to sbox." << std::endl;
        }

        int Execute(std::vector<std::string> &args) override {
            if(args.size() == 0) {
                std::cerr << "Wrong usage" << std::endl;
                std::cout << "Example1: --exe <progname> [params] - execute program <progname> with arguments [params]. Program will detached from sbox." << std::endl;
                std::cout << "Example2: --exe :<progname> [params] - execute program <progname> with arguments params. Program will attached to sbox." << std::endl;
                return 1;
            }

            LightProcess proc;
            std::string procname = args[0];
            bool attached = (procname[0] == ':');

            if(attached) procname.erase(0, 1);
            proc.options.wait_for_exit = attached;

            if(!attached) {
                #ifndef _WIN32
                proc.options.path = "xterm";
                proc.options.args.push_back("-e");         

                bool fnd = false;
                proc.options.args.push_back(proc.FindProgram(procname, fnd));
                #else
                proc.options.path = procname;
                proc.options.create_new_console = true;
                #endif
            }
            else {
                proc.options.path = procname;
            }

            bool flag = false;

            for(auto &arg: args) {
                if(flag) {
                   proc.options.args.push_back(arg);
                }

                flag = true;
            }

            proc.Execute();
            return 0;
        }
    };
}