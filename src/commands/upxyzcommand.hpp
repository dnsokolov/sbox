#pragma once

#include "icommand.hpp"
#include <filesystem>

namespace sbox {
	class UpxyzCommand : public ICommand {
		private:
		void Upxyz(const std::filesystem::path &xyzpath) {
			std::string path_to_xyz = xyzpath.filename().string();
			size_t last_index = path_to_xyz.find_last_of(".");
			std::filesystem::path XYZpath(path_to_xyz.substr(0, last_index)+".XYZ");
			std::filesystem::rename(xyzpath, XYZpath);
		}
		
        public:
        void GetHelp() override {
			std::cout << "Rename extension of '.xyz'-files to '.XYZ'-files" << std::endl;
		}	

        std::string GetName() override {
			return "--upxyz";
		}	

        int Execute(std::vector<std::string> &args) override {
			std::filesystem::path path = std::filesystem::current_path();
            for (const std::filesystem::path &entry : std::filesystem::directory_iterator(path)) {
                if(std::filesystem::is_regular_file(entry)) {
                    std::string ext = entry.extension().string();
                    if( ext == ".xyz") {
                        Upxyz(entry);
                    }
                }
            }
			
			std::cout << "Command --upxyz is finished" << std::endl;
			return 0;
		}			
	};
}