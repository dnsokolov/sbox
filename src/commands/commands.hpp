#pragma once

#include "luacommand.hpp"
#include "execommand.hpp"
#include "newcommand.hpp"
#include "upxyzcommand.hpp"
#include "atomskcommand.hpp"

namespace sbox {
    class Commands {
        private:
        std::vector<ICommand *> _commands;
        LuaCommand _lua_cmd;
        ExeCommand _exe_cmd;
        NewCommand _new_cmd;
		UpxyzCommand _upxyz_cmd;
        AtomskCommand _atomsk_cmd;

        public:
        Commands(LuaEngine *lua) {
            _lua_cmd.SetLuaEngine(lua);
            _commands.push_back(&_lua_cmd);
            _commands.push_back(&_exe_cmd);
            _commands.push_back(&_new_cmd);
			_commands.push_back(&_upxyz_cmd);
            _commands.push_back(&_atomsk_cmd);
        }

        void GetHelp() {
            std::cout << "Information about commadns:" << std::endl << std::endl;
            for(auto &cmd : _commands) {
                std::cout << "'" << cmd->GetName() << "': ";
                cmd->GetHelp();
                std::cout << std::endl;
            }
        }

        int Execute(const std::string &cmdname, std::vector<std::string> &args) {
            for(auto &cmd : _commands) {
                if(cmd->GetName() == cmdname) {
                    return cmd->Execute(args);
                }
            }

            return -1;
        }
    };
}